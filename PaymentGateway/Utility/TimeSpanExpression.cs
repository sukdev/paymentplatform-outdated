﻿using System;
using System.Text.RegularExpressions;


namespace StreamAMG.OTTPlatform.Utility
{
    public class TimeSpanExpression
    {
        public static int stringToMonths(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return 0;
            var re = new Regex("(?<value>[0-9]+)months");
            var m = re.Match(value);

            if (m.Success)
                return Convert.ToInt32(m.Groups["value"].Value);

            return 0;
        }

        public static int stringToDays(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return 0;
            var re = new Regex("(?<value>[0-9]+)days");
            var m = re.Match(value);

            if (m.Success)
                return Convert.ToInt32(m.Groups["value"].Value);

            return 0;
        }

        public static int stringToHours(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return 0;
            var re = new Regex("(?<value>[0-9]+)hours");
            var m = re.Match(value);

            if (m.Success)
                return Convert.ToInt32(m.Groups["value"].Value);

            return 0;
        }

        public static int stringToMinutes(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return 0;
            var re = new Regex("(?<value>[0-9]+)minutes");
            var m = re.Match(value);

            if (m.Success)
                return Convert.ToInt32(m.Groups["value"].Value);

            return 0;
        }


        public static string ToString(string value, string emptyString = "")
        {
            var result = "";

            if (stringToMonths(value) > 0)
                result = result + (result == "" ? "" : ",") + stringToMonths(value) + " month" + (stringToMonths(value) == 1 ? "" : "s");

            if (stringToDays(value) > 0)
                result = result + (result == "" ? "" : ",") + stringToDays(value) + " day" + (stringToDays(value) == 1 ? "" : "s");

            if (stringToHours(value) > 0)
                result = result + (result == "" ? "" : ",") + stringToHours(value) + " hour" + (stringToHours(value) == 1 ? "" : "s");

            if (stringToMinutes(value) > 0)
                result = result + (result == "" ? "" : ",") + stringToMinutes(value) + " minute" + (stringToMinutes(value) == 1 ? "" : "s");

            if (result == "")
                return emptyString;

            return result;
        }

        public static DateTime Add(string timeSpanExpression, DateTime value)
        {
            return value.AddMonths(stringToMonths(timeSpanExpression)).AddDays(stringToDays(timeSpanExpression)).AddHours(stringToHours(timeSpanExpression)).AddMinutes(stringToMinutes(timeSpanExpression));
        }
    }
}