﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace StreamAMG.OTTPlatform.Core.Utilities
{
    public class LazyMapper
    {
        public static IList Hydrate<T>(DataRow[] rows) where T : new()
        {
            var result = new List<T>();
            foreach (DataRow row in rows)
                result.Add(Hydrate<T>(row));
            return result;
        }

        public static IList Hydrate<T>(DataTable table) where T : new()
        {
            var result = new List<T>();
            foreach (DataRow row in table.Rows)
                result.Add(Hydrate<T>(row));
            return result;
        }

        public static T Hydrate<T>(DataRow Row) where T : new()
        {
            
            var result = new T();


            if (result.GetType().ToString() == "System.Guid")
            {
                return (T)(object)(new Guid(Row[0].ToString()));
            }


            Hydrate(result, Row);
            return result;
        }

        public static void HydrateTransaltion(object o, DataRow Row)
        {
            Dictionary<string, PropertyInfo> props = new Dictionary<string, PropertyInfo>();
            foreach (PropertyInfo p in o.GetType().GetProperties())
            {
                if (Row.Table.Columns.Contains(p.Name.ToLowerInvariant()))
                {
                    var columnValue = Row[p.Name.ToLowerInvariant()];
                    var safeType = Nullable.GetUnderlyingType(p.PropertyType) ?? p.PropertyType;

                    if (safeType.ToString() == "System.Guid")
                    {
                        var safeValue = (columnValue == null || columnValue == DBNull.Value) ? null : columnValue.ToString();
                        if (safeValue != null && !string.IsNullOrWhiteSpace(safeValue.ToString()))
                            p.SetValue(o, new Guid(safeValue), null);
                    }
                    else
                    {
                        var safeValue = (columnValue == null || columnValue == DBNull.Value) ? null : Convert.ChangeType(columnValue, safeType);
                        if (safeValue != null && !string.IsNullOrWhiteSpace(safeValue.ToString()))
                            p.SetValue(o, safeValue, null);
                    }


                }
            }
        }

        public static void Hydrate(object o, DataRow Row)
        {
           

            Dictionary<string, PropertyInfo> props = new Dictionary<string, PropertyInfo>();
            foreach (PropertyInfo p in o.GetType().GetProperties())
            {
                if (Row.Table.Columns.Contains(p.Name.ToLowerInvariant()))
                {
                    var columnValue = Row[p.Name.ToLowerInvariant()];
                    var safeType = Nullable.GetUnderlyingType(p.PropertyType) ?? p.PropertyType;
                    if (safeType.ToString() == "System.Guid")
                    {
                        p.SetValue(o, new Guid(columnValue.ToString()), null);
                    }
                    else
                    {
                        var safeValue = (columnValue == null || columnValue == DBNull.Value) ? null : Convert.ChangeType(columnValue, safeType);
                        p.SetValue(o, safeValue, null);
                    }
                }
            }
        }

        public static List<MySqlParameter> Evaporate(object o)
        {
            var result = new List<MySqlParameter>();
            foreach (PropertyInfo p in o.GetType().GetProperties())
            {
                result.Add(new MySqlParameter(p.Name, p.GetValue(o)));
            }
            return result;
        }

        public static T ExecuteObject<T>(string sql, params MySqlParameter[] sqlParameters) where T : new()
        {
            var sourceRow = MySqlHelper.ExecuteDataRow(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                sql,
                sqlParameters);

            if (sourceRow == null)
                return default(T);

            var result = new T();
            Hydrate(result, sourceRow);
            return result;
        }

        private static string GetTableName(string className)
        {
            var result = className.ToLowerInvariant();
            if (result.EndsWith("model"))
                return (result + "@").Replace("model@", "");

            return result;
        }

        public static T GetObjectById<T>(object id) where T : new()
        {
            if (id == null || id.ToString() == System.Guid.Empty.ToString())
                return default(T);

            var sourceRow = MySqlHelper.ExecuteDataRow(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "SELECT * FROM " + GetTableName(typeof(T).Name) + " WHERE id = ?id",
                new MySqlParameter("id", id));

            if (sourceRow == null)
                throw new Exception("Unable to load " + typeof(T).Name.ToLowerInvariant() + " with id '" + id.ToString() + "',");

            var result = new T();
            Hydrate(result, sourceRow);
            return result;
        }

        public static T GetObjectById<T>(object id, string language) where T : new()
        {
            if (id == null || id.ToString() == System.Guid.Empty.ToString())
                return default(T);

            var sourceRow = MySqlHelper.ExecuteDataRow(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "SELECT * FROM " + GetTableName(typeof(T).Name) + " WHERE id = ?id",
                new MySqlParameter("id", id));

            if (sourceRow == null)
                throw new Exception("Unable to load " + typeof(T).Name.ToLowerInvariant() + " with id '" + id.ToString() + "',");

            var result = new T();
            Hydrate(result, sourceRow);

            try
            {
                var transaltionRow = MySqlHelper.ExecuteDataRow(
                    System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                    "SELECT * FROM " + GetTableName(typeof(T).Name) + "translation WHERE id = ?id AND language = ?language",
                    new MySqlParameter("id", id),
                    new MySqlParameter("language", language)
                );

                if (transaltionRow != null)
                    LazyMapper.HydrateTransaltion(result, transaltionRow);
            }
            catch(Exception ex)
            {

            }

            return result;
        }

        public static T ExecuteScalar<T>(string sql, params MySqlParameter[] sqlParameters) where T : new()
        {
            var source = MySqlHelper.ExecuteScalar(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
              sql,
               sqlParameters);
            if (source == DBNull.Value)
                return new T();

            T result = new T();
            if (result.GetType().ToString() == "System.Guid")
                return (T)(object)new Guid((string)source);
            

            

            return (T)source;
        }

        public static string TrimToMaxLen(string value, int maxLength)
        {
            if (string.IsNullOrWhiteSpace(value) || value.Length <= maxLength)
                return value;

            return value.Substring(0, maxLength);
        }
    }
}
