﻿using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace StreamAMG.OTTPlatform.Core.Utilities
{
    public class DataBase
    {
        public static DateTime LocalToUtc(DateTime value)
        {
            return TimeZoneInfo.ConvertTimeToUtc(value, TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time"));
        }

        public static DateTime UtcToLocal(DateTime value)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(value, TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time"));
        }

        public static string ConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"];
            }
        }

        public static DataRow GetDataRow(string commandText, params MySqlParameter[] parms)
        {
            return MySqlHelper.ExecuteDataRow(ConnectionString, commandText, parms);
        }

        public static DataSet GetDataSet(string commandText, params MySqlParameter[] parms)
        {
            return MySqlHelper.ExecuteDataset(ConnectionString, commandText, parms);
        }

        public static DataTable GetDataTable(string commandText, params MySqlParameter[] parms)
        {
            return MySqlHelper.ExecuteDataset(ConnectionString, commandText, parms).Tables[0];
        }

        public static int ExecuteQuery(string commandText, params MySqlParameter[] parms)
        {
            return MySqlHelper.ExecuteNonQuery(ConnectionString, commandText, parms);
        }

        public static object GetObject(string commandText, params MySqlParameter[] parms)
        {
            return MySqlHelper.ExecuteScalar(ConnectionString, commandText, parms);
        }

        public static int GetInt(string commandText, params MySqlParameter[] parms)
        {
            return Convert.ToInt32(MySqlHelper.ExecuteScalar(ConnectionString, commandText, parms));
        }

        public static string GetString(string commandText, params MySqlParameter[] parms)
        {
            return Convert.ToString(MySqlHelper.ExecuteScalar(ConnectionString, commandText, parms));
        }
    }
}
