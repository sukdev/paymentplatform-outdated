﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Utility
{
    public class Currency
    {
        public static string GetSymbol(string code)
        {
            System.Globalization.RegionInfo regionInfo = (from culture in System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.InstalledWin32Cultures)
                                                          where culture.Name.Length > 0 && !culture.IsNeutralCulture
                                                          let region = new System.Globalization.RegionInfo(culture.LCID)
                                                          where String.Equals(region.ISOCurrencySymbol, code, StringComparison.InvariantCultureIgnoreCase)
                                                          select region).First();

            return regionInfo.CurrencySymbol;
        }

        public static string FormatAmount(string code, Decimal amount)
        {
            if (code.ToLowerInvariant() == "eur")
                return amount.ToString("#.00") + " " + GetSymbol(code);
            else
                return GetSymbol(code) + amount.ToString("#.00");
        }
    }
}