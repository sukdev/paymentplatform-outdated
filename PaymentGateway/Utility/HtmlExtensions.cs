﻿using StreamAMG.OTTPlatform.Utility;
using System.Collections.Generic;

using System.Linq.Expressions;
using System.Text;
using System.Web.Routing;


namespace System.Web.Mvc.Html
{
    public static class HtmlExtensions
    {

        public static MvcHtmlString AutoCompleteFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> valueExpression, string tableName, string idFieldName, string valueFieldName, Object htmlAttributes = null)
        {
            Func<TModel, TValue> valueMethod = valueExpression.Compile();            

            var isReadOnly = false;
            var valueString = "";
            var labelString = "";

            if (html.ViewData.Model != null)
            {
                try
                {
                    valueString = valueMethod(html.ViewData.Model).ToString();
                }
                catch
                {
                }               
            }

            var valueName = html.NameFor(valueExpression).ToString();
            var valueId = html.IdFor(valueExpression).ToString();

            // Determine if read only 
            var htmlAttributesDictionary = new RouteValueDictionary(htmlAttributes);
            if (htmlAttributesDictionary.ContainsKey("readonly") && (htmlAttributesDictionary["readonly"].ToString().ToLowerInvariant() == "true" || htmlAttributesDictionary["readonly"].ToString().ToLowerInvariant() == "readonly"))
                isReadOnly = true;




            var innerHtml = new StringBuilder();
            innerHtml.AppendLine("<input id=\"" + valueId + "\" name= \"" + valueName + "\" type=\"hidden\" value=\"" + valueString + "\" onchange=\"alert('changed')\" />");            
            innerHtml.AppendLine("<input id=\"" + valueId + "_typeahead\" type=\"text\" class=\"form-control\" placeholder=\"Search for...\" value=\"" + labelString + "\" >");
            innerHtml.AppendLine("<span class=\"input-group-btn\">");
            innerHtml.AppendLine("<button class=\"btn btn-default typeahead-clear\" type=\"button\" onclick=\"$('#" + valueId + "_typeahead').val('');$('#" + valueId + "').val('');\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>");
            innerHtml.AppendLine("</span>");
            innerHtml.AppendLine("<script>");
            innerHtml.AppendLine("$('#" + valueId + "_typeahead').typeahead(null, {");
            innerHtml.AppendLine("    name: 'best-pictures',");
            innerHtml.AppendLine("    display: 'Name',");
            innerHtml.AppendLine("    source: new Bloodhound({");
            innerHtml.AppendLine("        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),");
            innerHtml.AppendLine("        queryTokenizer: Bloodhound.tokenizers.whitespace,");
            innerHtml.AppendLine("        remote: {");
            innerHtml.AppendLine("            url: '/controlpanel/autocomplete/?t=" + tableName + "&i="+ idFieldName + "&v=" + valueFieldName + "&q=%QUERY',");
            innerHtml.AppendLine("            wildcard: '%QUERY'");
            innerHtml.AppendLine("        }");
            innerHtml.AppendLine("    })");
            innerHtml.AppendLine("}).bind('typeahead:select', function (ev, suggestion) {");
            innerHtml.AppendLine("    if($('#" + valueId + "').val() != suggestion.Id)");
            innerHtml.AppendLine("    {");
            innerHtml.AppendLine("        $('#" + valueId + "').val(suggestion.Id);");
            innerHtml.AppendLine("        $('#" + valueId + "').trigger('change');");
            innerHtml.AppendLine("    }");
            innerHtml.AppendLine("});");
            innerHtml.AppendLine("</script>");

            TagBuilder SelectTagBuilder = new TagBuilder("div")
            {
                InnerHtml = innerHtml.ToString()
            };


            SelectTagBuilder.Attributes.Add("id", valueId + "_container");
            SelectTagBuilder.Attributes.Add("class", "input-group");
            SelectTagBuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return MvcHtmlString.Create(SelectTagBuilder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString TimeSpanFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, Object htmlAttributes = null)
        {
            var method = expression.Compile();

            var valueString = "";
            try
            {
                valueString = method(html.ViewData.Model).ToString();
            }
            catch
            {

            }

            var valueName = html.NameFor(expression).ToString();
            var valueId = html.IdFor(expression).ToString();

            var hiddenInput = new TagBuilder("input")
            {                
                InnerHtml = ""
            };
            hiddenInput.Attributes.Add("type", "hidden");
            hiddenInput.Attributes.Add("value", valueString);
            hiddenInput.Attributes.Add("name", valueName);
            hiddenInput.Attributes.Add("id", valueId);

            var monthInput = new TagBuilder("select")
            {
                InnerHtml = "<option value=\"\"></option>"
            };

            monthInput.Attributes.Add("onchange", "$('#" + valueId + "').val($('#" + valueId + "_0').val()+$('#" + valueId + "_1').val())");
            monthInput.Attributes.Add("id", valueId +"_0");
            for (var i = 1; i <= 12; i++)            
                monthInput.InnerHtml += "<option value=\"" + i + "months\" "  + ( TimeSpanExpression.stringToMonths(valueString) == i ? "selected=\"seleted\"" : "" ) + " >" + i + ( i == 1 ? " month" : " months")+ "</option>";

            
            monthInput.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            var dayInput = new TagBuilder("select")
            {
                InnerHtml = "<option value=\"\"></option>"
            };
            
            dayInput.Attributes.Add("onchange", "$('#" + valueId + "').val($('#" + valueId + "_0').val()+$('#" + valueId + "_1').val())");
            dayInput.Attributes.Add("id", valueId + "_1");

            for (var i = 1; i <= 31; i++)
                dayInput.InnerHtml += "<option value=\"" + i + "days\" " + (TimeSpanExpression.stringToDays(valueString) == i ? "selected=\"seleted\"" : "") + " >" + i + (i == 1 ? " day" : " days") + "</option>";

            dayInput.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return MvcHtmlString.Create("<div class=\"row\">" + hiddenInput.ToString() +"<div class=\"col-md-6\">" +  monthInput.ToString() + "</DIV><div class=\"col-md-6\">" + dayInput.ToString() + "</div></div>");
        }

        private static string PagerQueryString(int pageIndex, int pageSize)
        {
            var items = new List<string>();
            foreach (var key in HttpContext.Current.Request.QueryString.AllKeys)
            {
                if (key.ToLowerInvariant() == "pageindex")
                    continue;

                if (key.ToLowerInvariant() == "pagesize")
                    continue;

                items.Add(string.Concat(key, "=", System.Web.HttpUtility.UrlEncode(HttpContext.Current.Request.QueryString[key])));
            }

            items.Add(string.Concat("pageindex", "=", pageIndex.ToString()));
            items.Add(string.Concat("pagesize", "=", pageSize.ToString()));

            return string.Join("&", items.ToArray());
        }
        public static MvcHtmlString Pager(int pageIndex, int pageSize, int itemCount)
        {
            var pageCount = Convert.ToInt32(Math.Ceiling((double)itemCount / pageSize));
            var pageFrom = pageIndex - 3;
            if (pageFrom < 0)
                pageFrom = 0;

            var pageTo = pageFrom + 7;
            if (pageTo > pageCount)
                pageTo = pageCount;

            var innerHtml = new StringBuilder();
            innerHtml.AppendLine("<div class=\"btn-group\">");
            innerHtml.AppendLine("<ul class=\"pagination\" style=\"margin: 0;\">");
            innerHtml.AppendLine("<li><a href=\"#\">«</a></li>");
            for (var i = pageFrom; i < pageTo; i++)
            {
                innerHtml.AppendLine("<li" + (i == pageIndex ? " class=\"active\"" : "") + "><a href=\"" + HttpContext.Current.Request.Url.AbsolutePath + "?" + PagerQueryString(i, pageSize) + "\" >" + (i + 1) + "</a></li>");
            }
            innerHtml.AppendLine("<li><a href=\"#\">»</a></li>");

            innerHtml.AppendLine("</ul>");
            innerHtml.AppendLine("</div>");
            return MvcHtmlString.Create(innerHtml.ToString());
        }       
    }
}
