﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace StreamAMG.OTTPlatform.Utility
{
    public class SystemLanguages    
    {
        static List<string> _languageCodes;
        static Dictionary<string, string> _languageNames;
        static Dictionary<string, string> _languageNativeNames;

        static SystemLanguages()
        {
            _languageCodes = new List<string>();
            _languageNames = new Dictionary<string, string>();
            _languageNativeNames = new Dictionary<string, string>();

            foreach (DataRow dr in MySqlHelper.ExecuteDataset(System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"], @"SELECT code AS id, name, name AS nativename FROM language ORDER BY defaultlanguage DESC, name;").Tables[0].Rows)
            {
                _languageCodes.Add(Convert.ToString(dr["id"]).ToLowerInvariant());
                _languageNames.Add(Convert.ToString(dr["id"]).ToLowerInvariant(), Convert.ToString(dr["name"]));
                _languageNativeNames.Add(Convert.ToString(dr["id"]).ToLowerInvariant(), Convert.ToString(dr["nativename"]));
            }
        }

        public static List<string> LanguageCodes
        {
            get
            {
                return _languageCodes;
            }
        }

        public static bool IsValidLanguage(string id)
        {
            return _languageCodes.Contains(id.ToLowerInvariant());
        }

        public static bool IsDefaultLanguage(string id)
        {
            return _languageCodes[0] == id.ToLowerInvariant();
        }

        public static string LanguageName(string id)
        {
            if (!_languageNames.ContainsKey(id.ToLowerInvariant()))
                throw new Exception("Language code '" + id + "' is not valid");

            return _languageNames[id.ToLowerInvariant()];
        }

        public static string LanguageNativeName(string id)
        {
            if (!_languageNativeNames.ContainsKey(id.ToLowerInvariant()))
                throw new Exception("Language code '" + id + "' is not valid");

            return _languageNativeNames[id.ToLowerInvariant()];
        }
    }
}