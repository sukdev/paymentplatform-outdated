﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using StreamAMG.OTTPlatform.Core.Services;
using MySql.Data.MySqlClient;

namespace StreamAMG.OTTPlatform.Utility
{
    public class Helpers
    {
        public static string CalculateMD5Hash(string input)

        {

            // step 1, calculate MD5 hash from input

            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)

            {

                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();

        }

        public static string RandomString(int length, string characters)
        {
            var result = new StringBuilder();
            using (var rng = new RNGCryptoServiceProvider())
            {
                byte[] uintBuffer = new byte[sizeof(uint)];

                while (length-- > 0)
                {
                    rng.GetBytes(uintBuffer);
                    var num = BitConverter.ToUInt32(uintBuffer, 0);
                    result.Append(characters[(int)(num % (uint)characters.Length)]);
                }
            }

            return result.ToString();
        }

        public static bool TryGetCurrencySymbol(string ISOCurrencySymbol, out string symbol)
        {
            symbol = CultureInfo
                .GetCultures(CultureTypes.AllCultures)
                .Where(c => !c.IsNeutralCulture)
                .Select(culture =>
                {
                    try
                    {
                        return new RegionInfo(culture.LCID);
                    }
                    catch
                    {
                        return null;
                    }
                })
                .Where(ri => ri != null && ri.ISOCurrencySymbol == ISOCurrencySymbol)
                .Select(ri => ri.CurrencySymbol)
                .FirstOrDefault();

            return symbol != null;
        }

        public static string GetCurrencySymbol(string currencyCode)
        {
            string currSymbol;

            TryGetCurrencySymbol(currencyCode, out currSymbol);

            return currSymbol;
        }

        public static string CreateCSVAndAppendCustomerFieldsFromGenericList<T>(List<T> list, string customerIdPropertyName)
        {

            var customFieldSource = new Dictionary<string, Dictionary<Guid, string>>();

            var getCustomFieldListResponse = CustomFieldService.GetCustomFieldList(new CustomFieldService.GetCustomFieldListRequest
            {
                PageIndex = 0,
                PageSize = int.MaxValue
            });

            foreach (var customField in getCustomFieldListResponse.Items)
            {
                customFieldSource.Add(customField.Id.ToString(),new Dictionary<Guid, string>());
                var customDataSet =
                    MySqlHelper.ExecuteDataset(
                        System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                        "SELECT customer.id, reportingvalue FROM customer INNER JOIN customercustomfielddata ON customercustomfielddata.customerid = customer.id AND customercustomfielddata.customfieldid = '" + customField.Id + "' INNER JOIN customfieldoption ON customfieldoption.id = customercustomfielddata.value");

                foreach (DataRow dr in customDataSet.Tables[0].Rows)
                    customFieldSource[customField.Id.ToString()].Add(new Guid(dr[0].ToString()), dr[1].ToString());
            }


            if (list == null || list.Count == 0) return string.Empty;

            //get type from 0th member
            var t = list[0].GetType();


            var sb = new StringBuilder();

            var o = Activator.CreateInstance(t);
            var props = o.GetType().GetProperties();


            sb.Append(string.Join(",", props.Select(d => d.Name).ToArray()));

            foreach (var customField in getCustomFieldListResponse.Items)
                sb.Append("," + customField.Name);

            sb.Append(Environment.NewLine);

            foreach (T item in list)
            {
                var values = props.Select(d => item.GetType().GetProperty(d.Name).GetValue(item, null)).ToArray();

                var row = "";
                foreach (var value in values)
                {
                    if (value == null)
                    {
                        row += (row == "" ? "" : ",") + "\"\"";
                    }
                    else
                    {
                        row += (row == "" ? "" : ",") + "\"" + value.ToString() + "\"";
                    }
                }

                foreach (var customField in getCustomFieldListResponse.Items)
                    if(customFieldSource[customField.Id.ToString()].ContainsKey(new Guid(item.GetType().GetProperty(customerIdPropertyName).GetValue(item, null).ToString())))
                        row += (row == "" ? "" : ",") + "\"" + customFieldSource[customField.Id.ToString()][new Guid(item.GetType().GetProperty(customerIdPropertyName).GetValue(item, null).ToString())] + "\"";
                    else
                        row += (row == "" ? "" : ",") + "\"\"";

                sb.AppendLine(row);
            }

            return sb.ToString();
        }

        public static string CreateCSVFromGenericList<T>(List<T> list)
        {
            if (list == null || list.Count == 0) return string.Empty;

            //get type from 0th member
            var t = list[0].GetType();


            var sb = new StringBuilder();

            var o = Activator.CreateInstance(t);
            var props = o.GetType().GetProperties();


            sb.AppendLine(string.Join(",", props.Select(d => d.Name).ToArray()));


            foreach (T item in list)
            {
                var values = props.Select(d => item.GetType().GetProperty(d.Name).GetValue(item, null)).ToArray();

                var row = "";
                foreach (var value in values)
                {
                    if (value == null)
                    {
                        row += (row == "" ? "" : ",") + "\"\"";
                    }
                    else
                    {
                        row += (row == "" ? "" : ",") + "\"" + value.ToString() + "\"";
                    }
                }
                sb.AppendLine(row);
            }

            return sb.ToString();
        }
    }
}