﻿using System;
using System.Configuration;
using System.Threading;
using System.Web;
using System.Web.Routing;
using NLog;
using StreamAMG.OTTPlatform.Core.Services;

namespace StreamAMG.OTTPlatform
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var mailGunLogProcessorThread = new Thread(MailGunLogProcessor);
            mailGunLogProcessorThread.Start();

            var offerVoucherCodeGeneratorProcessorThread = new Thread(OfferVoucherCodeGeneratorProcessor);
            offerVoucherCodeGeneratorProcessorThread.Start();

            var invoiceProcessorThread = new Thread(InvoiceProcessor);
            invoiceProcessorThread.Start();
        }


        private static void MailGunLogProcessor()
        {
            while (true)
            {
                try
                {
                    MailGunLogService.ProcessLogs();
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Error("Scheduled MailGunLogProcessor exited " + ex.Message, ex);
                }
                Thread.Sleep(new TimeSpan(0, 7, 30));
            }
        }


        private static void InvoiceProcessor()
        {
            while (true)
            {
                try
                {
                    if (ConfigurationManager.AppSettings["BatchProcessingEnabled"].ToLowerInvariant() == "true")
                        InvoicingService.RunInvoiceBatch();
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger()
                        .Error("Scheduled InvoiceProcessor exited " + ex.Message, ex);
                }
                Thread.Sleep(new TimeSpan(0, 15, 00));
            }
        }

        private static void OfferVoucherCodeGeneratorProcessor()
        {
            while (true)
            {
                try
                {
                    OfferVoucherService.PopulateVoucherCodes();
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger()
                        .Error("Scheduled OfferVoucherCodeGeneratorProcessor exited " + ex.Message, ex);
                }
                Thread.Sleep(new TimeSpan(0, 2, 30));
            }
        }
    }
}