﻿using StreamAMG.OTTPlatform.Core.Utilities;
using StreamAMG.OTTPlatform.Utility;
using MySql.Data.MySqlClient;
using Stripe;
using System;
using System.Collections.Generic;
using System.Text;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class CustomerTransactionRefundService
    {

        public class TransationRefundSearchResult
        {
            public class Item
            {
                public Guid Id { get; set; }
                public DateTime CreatedAt { get; set; }
                public DateTime UpdatedAt { get; set; }
                public DateTime? SubmittedAt { get; set; }
                public DateTime? WarnAt { get; set; }
                public Guid CustomerId { get; set; }
                public string CustomerEmailAddress { get; set; }
                public string CustomerFirstName { get; set; }
                public string CustomerLastName { get; set; }

                public string SubscriptionPlanName { get; set; }

                public int Status { get; set; }
                public Decimal Amount { get; set; }
                public string Currency { get; set; }
                public string Reference { get; set; }
                public string Provider { get; set; }
                public string ProviderId { get; set; }
                public string ProviderMessage { get; set; }
                public string Description { get; set; }
                public string Reason { get; set; }
                public Guid CustomerTransactionId { get; set; }
            }

            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }

            public TransationRefundSearchRequest Request { get; set; }
        }

        public class TransationRefundSearchRequest
        {
            public enum TransactionStatuses
            {
                Processing = -2,
                Warning = -3,
                Failure = -1,
                Success = 1,
                Prepared = 0
            }

            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public Guid CustomerId { get; set; }
            public Guid CustomerTransactionId { get; set; }
            public TransactionStatuses? TransactionStatus { get; set; }
            public Guid SubscriptionPlanId { get; set; }
            public DateTime? DateFrom { get; set; }
            public DateTime? DateTo { get; set; }
        }


        public static TransationRefundSearchResult TransactionRefundSearch(TransationRefundSearchRequest request)
        {

            var sqlWhere = "1 = 1";

            if (request.TransactionStatus.HasValue)
            {
                switch (request.TransactionStatus)
                {
                    case TransationRefundSearchRequest.TransactionStatuses.Failure:
                        sqlWhere += " AND status = -1 AND submittedat IS NOT NULL";
                        break;

                    case TransationRefundSearchRequest.TransactionStatuses.Success:
                        sqlWhere += " AND status = 1 AND submittedat IS NOT NULL";
                        break;

                    case TransationRefundSearchRequest.TransactionStatuses.Warning:
                        sqlWhere += " AND status = 0 AND submittedat IS NOT NULL AND warnat <= ?now";
                        break;

                    case TransationRefundSearchRequest.TransactionStatuses.Processing:
                        sqlWhere += " AND status = 0 AND submittedat IS NOT NULL AND warnat > ?now";
                        break;

                    case TransationRefundSearchRequest.TransactionStatuses.Prepared:
                        sqlWhere += " AND submittedat IS NULL";
                        break;
                }

            }

            if(request.CustomerId!= Guid.Empty)
                sqlWhere += " AND customerid = ?customerid";

            if (request.CustomerTransactionId != Guid.Empty)
                sqlWhere += " AND customertransactionid = ?customertransactionid";

            if (request.SubscriptionPlanId != Guid.Empty)
                sqlWhere += " AND customertransactionrefund.subscriptionplanid = ?subscriptionplanid";

            if (request.DateFrom.HasValue)
                sqlWhere += " AND customertransactionrefund.createdat >= ?datefrom";

            if (request.DateTo.HasValue)
                sqlWhere += " AND customertransactionrefund.createdat <= ?dateto";

            var sql = new StringBuilder();            
            sql.AppendLine("SELECT COUNT(customertransactionrefund.id) FROM customertransactionrefund INNER JOIN customer ON customer.id = customertransactionrefund.customerid INNER JOIN subscriptionplan ON subscriptionplan.id = customertransactionrefund.subscriptionplanid WHERE " + sqlWhere + ";");
            sql.AppendLine("SELECT customertransactionrefund.id, customertransactionrefund.createdat, customertransactionrefund.updatedat, customertransactionrefund.submittedat, customertransactionrefund.warnat, customer.id AS customerid, customer.emailaddress AS customeremailaddress, customer.firstname AS customerfirstname, customer.lastname AS customerlastname, customertransactionrefund.status, customertransactionrefund.amount, customertransactionrefund.currency, customertransactionrefund.reference, customertransactionrefund.provider, customertransactionrefund.providerid, customertransactionrefund.providermessage, customertransactionrefund.description, subscriptionplan.name AS subscriptionplanname, customertransactionrefund.reason, customertransactionrefund.customertransactionid FROM customertransactionrefund INNER JOIN customer ON customer.id = customertransactionrefund.customerid INNER JOIN subscriptionplan ON subscriptionplan.id = customertransactionrefund.subscriptionplanid WHERE " + sqlWhere + " ORDER BY customertransactionrefund.updatedat DESC LIMIT ?pageOffset, ?pageLimit;");

            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize),
                new MySqlParameter("customerid", request.CustomerId),
                new MySqlParameter("subscriptionplanid", request.SubscriptionPlanId),
                new MySqlParameter("customertransactionid", request.CustomerTransactionId),                
                new MySqlParameter("datefrom", request.DateFrom),
                new MySqlParameter("dateto", request.DateTo)
           );

            var result = new TransationRefundSearchResult();
            result.Request = request;
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<TransationRefundSearchResult.Item>)LazyMapper.Hydrate<TransationRefundSearchResult.Item>(sourceDataSet.Tables[1]);
            return result;
        }
        
        public class TransactionRefund
        {
            public Guid Id { get; set; }
            public DateTime CreatedAt { get; set; }
            public DateTime UpdatedAt { get; set; }
            public Guid CustomerId { get; set; }
            public Guid SubscriptionPlanId { get; set; }
            public Guid CustomerSubscriptionId { get; set; }            
            public Guid CustomerBillingProfileId { get; set; }
            public string CustomerBillingProfileParameters { get; set; }
            public int Status { get; set; }
            public Decimal Amount { get; set; }
            public string Currency { get; set; }
            public string Description { get; set; }            
            public string Provider { get; set; }
            public string ProviderId { get; set; }
            public string ProviderMessage { get; set; }
            public string Country { get; set; }
            public string Reference { get; set; }
            public DateTime RenewalFrom { get; set; }
            public DateTime RenewalTo { get; set; }
            public string StatementDescription { get; set; }
            public DateTime? CompletedAt { get; set; }
            public DateTime? SubmittedAt { get; set; }
            public DateTime? WarnAt { get; set; }
            public Guid CustomerTransactionId { get; set; }
            public string Reason { get; set; }
        }

        public class IssueTransactionRefundRequest
        {
            public Guid CustomerTransactionId { get; set; }
            public decimal Amount { get; set; }
            public string Reason { get; set; }

        }

        public class IssueTransactionRefundResult
        {
            public TransactionRefund TransactionRefund { get; set; }
        }

        public static IssueTransactionRefundResult IssueTransactionRefund(IssueTransactionRefundRequest request)
        {

            var customerTransactionGetResponse = CustomerTransactionService.Get(new CustomerTransactionService.GetTransactionRequest { TransactionId = request.CustomerTransactionId });


            if (customerTransactionGetResponse == null)
                throw new Exception("CustomerTransaction with id '" + request.CustomerTransactionId + "' could not be found");

            if (customerTransactionGetResponse.Transaction.Status != 1)
                throw new Exception("The transaction with id '" + request.CustomerTransactionId + "' cannot be refunded because it was not a successful transaction.");

            if (customerTransactionGetResponse.Transaction.AmountRefunded + request.Amount > customerTransactionGetResponse.Transaction.Amount)
                throw new Exception("Refunding '" + request.Amount.ToString("0.00") + "' to the transaction with id '" + request.CustomerTransactionId + "' would total more than the total amount invoiced '" + customerTransactionGetResponse.Transaction.Amount.ToString("0.00")  +"'.");

            // Create a new transactionrefund in the database
            var customerTransactionRefund = LazyMapper.ExecuteObject<TransactionRefund>(
                "INSERT INTO customertransactionrefund ( id, createdat, updatedat, customerid, subscriptionplanid, customersubscriptionid, customerbillingprofileid, status, amount, currency, description, provider, country, reference, renewalfrom, renewalto, customerbillingprofileparameters, statementdescription, customertransactionid, reason )VALUES( ?id, ?now, ?now, ?customerid, ?subscriptionplanid, ?customersubscriptionid, ?customerbillingprofileid, 0, ?amount, ?currency, ?description, ?provider, ?country, ?reference, ?renewalfrom, ?renewalto, ?customerbillingprofileparameters, ?statementdescription, ?customertransactionid, ?reason );SELECT * FROM customertransactionrefund WHERE id = ?id;",
                new MySqlParameter("id", Guid.NewGuid()),
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("customerid", customerTransactionGetResponse.Transaction.CustomerId),
                new MySqlParameter("subscriptionplanid", customerTransactionGetResponse.Transaction.SubscriptionPlanId),
                new MySqlParameter("customersubscriptionid", customerTransactionGetResponse.Transaction.CustomerSubscriptionId),
                new MySqlParameter("customerbillingprofileid", customerTransactionGetResponse.Transaction.CustomerBillingProfileId),
                new MySqlParameter("currency", customerTransactionGetResponse.Transaction.Currency),
                new MySqlParameter("description", customerTransactionGetResponse.Transaction.Description),
                new MySqlParameter("provider", customerTransactionGetResponse.Transaction.Provider),
                new MySqlParameter("country", customerTransactionGetResponse.Transaction.Country),
                new MySqlParameter("reference", customerTransactionGetResponse.Transaction.Reference),
                new MySqlParameter("renewalfrom", customerTransactionGetResponse.Transaction.RenewalFrom),
                new MySqlParameter("renewalto", customerTransactionGetResponse.Transaction.RenewalTo),
                new MySqlParameter("customerbillingprofileparameters", customerTransactionGetResponse.Transaction.CustomerBillingProfileParameters),
                new MySqlParameter("statementdescription", customerTransactionGetResponse.Transaction.StatementDescription),
                new MySqlParameter("customertransactionid", request.CustomerTransactionId),
                new MySqlParameter("reason", request.Reason),
                new MySqlParameter("amount", request.Amount)
            );

            if (customerTransactionRefund == null)
                throw new Exception("Unable to create a new customer transaction refund.");


            var success = false;
            var providermessage = "";
            var providerId = (string)null;

            if (customerTransactionRefund.Provider == "StripeCard")
            {
                var metaData = new Dictionary<string, string>();
                metaData.Add("customerId", customerTransactionRefund.CustomerId.ToString());
                metaData.Add("customerTransactionId", customerTransactionRefund.CustomerTransactionId.ToString());
                metaData.Add("customerTransactionRefundId", customerTransactionRefund.Id.ToString());

                // Mark submittedat so we can be flagged of any issues
                customerTransactionRefund = LazyMapper.ExecuteObject<TransactionRefund>(
                    "UPDATE customertransactionrefund SET submittedat = ?now, updatedat = ?now, warnat = DATE_ADD( ?now, INTERVAL 5 MINUTE ) WHERE submittedat IS NULL AND id = ?id;SELECT * FROM customertransactionrefund WHERE id = ?id AND submittedat - ?now;",
                    new MySqlParameter("now", DateTime.UtcNow),
                    new MySqlParameter("id", customerTransactionRefund.Id)
                );

                if (customerTransactionRefund == null)
                    throw new Exception("CustomerTransactionRefund '" + customerTransactionRefund.Id + "' could not be after setting submittedat, the transaction has already been submitted.");

                try
                {
                    // Make call to stripe!
                    var refundService = new StripeRefundService();

                    var cardServiceResponse = refundService.Create(
                        customerTransactionGetResponse.Transaction.ProviderId,
                        new StripeRefundCreateOptions()
                        {
                            Amount = Convert.ToInt32(customerTransactionRefund.Amount * 100),                             
                            Metadata = metaData
                        },
                        new StripeRequestOptions
                        {
                            ApiKey = System.Configuration.ConfigurationManager.AppSettings["StripePrivateKey"]
                        }
                    );

                    success = true;
                    providerId = cardServiceResponse.Id;
                }
                catch (StripeException ex)
                {
                    providerId = ex.StripeError.ChargeId;
                    providermessage = ex.StripeError.Message;
                }

            }
            else if (customerTransactionRefund.Provider == "PayPal")
            {
                throw new Exception("Paypal refunds not supported");
                /*
                var sdkConfig = new Dictionary<string, string>();
                sdkConfig.Add("mode", "sandbox");
                sdkConfig.Add("account1.apiUsername", "isobelle.benge_api1.streamamg.com");
                sdkConfig.Add("account1.apiPassword", "UTXERWDVZ9DV4FL4");
                sdkConfig.Add("account1.apiSignature", "AfTmX-m.uF92ypDT.3npLnfLtoqyAO8rJjyCp2nkAzC2HDgtpG..6IIB");

                var service = new PayPalAPIInterfaceServiceService(sdkConfig);

                var doReferenceTransactionReqResponse = service.RefundTransaction(new RefundTransactionReq
                {
                    RefundTransactionRequest = new RefundTransactionRequestType
                    {
                        TransactionID = customerTransactionGetResponse.Transaction.ProviderId,
                        RefundType = RefundType.PARTIAL,
                        Amount = new BasicAmountType((CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), currency), amount.ToString("0.00")),
                    }
                });

                if (doReferenceTransactionReqResponse.Ack == AckCodeType.SUCCESS)
                {
                    success = true;
                    providerId = doReferenceTransactionReqResponse.RefundTransactionID;
                }
                else
                {
                    foreach (var errorMessage in doReferenceTransactionReqResponse.Errors)
                        providermessage += (providermessage != "" ? "" : System.Environment.NewLine) + errorMessage.ShortMessage;
                }*/
            }

            customerTransactionRefund = LazyMapper.ExecuteObject<TransactionRefund>(
               "UPDATE customertransactionrefund SET providerid = ?providerid, status = ?status, updatedat = ?now, completedat = ?now, providermessage = ?providermessage WHERE id = ?id;SELECT * FROM customertransactionrefund WHERE id = ?id;",
               new MySqlParameter("id", customerTransactionRefund.Id),
               new MySqlParameter("now", DateTime.UtcNow),
               new MySqlParameter("providerid", providerId),
               new MySqlParameter("providermessage", providermessage),
               new MySqlParameter("status", success ? 1 : -1)               
           );

            if (success)
            {
                try
                {
                    var replacements = new Dictionary<string, string>();

                    replacements.Add("refundamount", Currency.FormatAmount(customerTransactionRefund.Currency,customerTransactionRefund.Amount));
                    replacements.Add("refundreference", customerTransactionRefund.Reference);
                    replacements.Add("refunddescription", customerTransactionRefund.Description);
                    replacements.Add("refundreason", customerTransactionRefund.Reason);

                    if (customerTransactionRefund.Provider == "StripeCard")
                        MailService.SendMail("RefundSuccessStripeCard", customerTransactionRefund.CustomerId, replacements);
                    else if(customerTransactionRefund.Provider == "PayPal")
                        MailService.SendMail("RefundSuccessPayPal", customerTransactionRefund.CustomerId, replacements);
                    else
                        throw new Exception(customerTransactionRefund.Provider   + " refund emails not supported");
                }
                catch (Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Warn("Unable to send email for the transaction refund of '" + customerTransactionRefund.Id + "'. Excpetion : " + ex.Message, ex);
                }
            }


            var result = new IssueTransactionRefundResult
            {
                TransactionRefund = customerTransactionRefund
            };

            return result;
        }


        public class GetTransactionRefundRequest
        {
            public Guid TransactionId { get; set; }
        }

        public class GetTransactionRefundResponse
        {
            public TransactionRefund TransactionRefund { get; set; }
        }

        public static GetTransactionRefundResponse Get(GetTransactionRefundRequest request)
        {
            var transactionRefund = LazyMapper.ExecuteObject<TransactionRefund>(
                "SELECT * FROM customertransactionrefund WHERE id = ?id",
                new MySqlParameter("id", request.TransactionId)
            );

            if (transactionRefund == null)
                throw new Exception("Customer transaction refund with id '" + request.TransactionId + "' not found.");

            return new GetTransactionRefundResponse
            {
                TransactionRefund = transactionRefund
            };
        }

    }
}
