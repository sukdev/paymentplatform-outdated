﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class MailGunLogService
    {

        public static void ProcessLogs()
        {
            NLog.LogManager.GetCurrentClassLogger().Info("MailGunLogService.ProcessLogs started");
            var lockKey = BatchLockService.GetLock("emailingest", new TimeSpan(0, 5, 0));
            if (lockKey == Guid.Empty)
            {
                NLog.LogManager.GetCurrentClassLogger().Info("Process already running, unable to get lock");
                return;
            }
            else
            {
                var pageIndex = 0;
                var lastPageUrl = string.Empty;
                var r = GetMailGunEvents(lastPageUrl);
                while (r["paging"]["next"].ToString() != lastPageUrl)
                {
                    NLog.LogManager.GetCurrentClassLogger().Info("MailGunLogService.ProcessLogs processing page " + (pageIndex + 1) + " started");
                    if (BatchLockService.KeepLock("emailingest", new TimeSpan(0, 5, 0), lockKey) == Guid.Empty)
                        throw new Exception("Batch lock was broken.");
                    lastPageUrl = r["paging"]["next"].ToString();
                    r = GetMailGunEvents(lastPageUrl);
                    ProcessEvents(r);
                    NLog.LogManager.GetCurrentClassLogger().Info("MailGunLogService.ProcessLogs processing page " + (pageIndex + 1) + " complete");
                    pageIndex++;
                }
            }
            NLog.LogManager.GetCurrentClassLogger().Info("MailGunLogService.ProcessLogs complete");
        }

        private static void ProcessEvents(JObject mailGunResult)
        {
            foreach (var item in mailGunResult["items"])
                ProcessEvent(item);
        }

        private static void ProcessEvent(JToken mailGunEvent)
        {

            var timestamp = new DateTime(1970, 1, 1).AddSeconds(Convert.ToSingle(mailGunEvent["timestamp"].ToString()));
            
            var mailLog = LazyMapper.ExecuteObject<MailLogModel>(
                "SELECT * FROM maillog WHERE referenceid = ?referenceid",
                new MySql.Data.MySqlClient.MySqlParameter("referenceid", "<" + mailGunEvent["message"]["headers"]["message-id"].ToString() + ">")
            );
            if (mailLog == null)
                return;
            switch (mailGunEvent["event"].ToString().ToLowerInvariant())
            {
                case "failed":
                    mailLog = LazyMapper.ExecuteObject<MailLogModel>(
                        "UPDATE maillog SET status = -1 WHERE id = ?id AND status <= 1 AND id = ?id;SELECT * FROM maillog WHERE id = ?id",
                        new MySql.Data.MySqlClient.MySqlParameter("id", mailLog.Id),
                        new MySql.Data.MySqlClient.MySqlParameter("timestamp", timestamp)
                    );
                    break;
                case "rejected":
                    mailLog = LazyMapper.ExecuteObject<MailLogModel>(
                        "UPDATE maillog SET status = -2 WHERE id = ?id AND status <= 1 AND id = ?id;UPDATE maillog SET rejectedat = ?timestamp WHERE id = ?id AND ( rejectedat IS NULL OR rejectedat > ?timestamp);SELECT * FROM maillog WHERE id = ?id",
                        new MySql.Data.MySqlClient.MySqlParameter("id", mailLog.Id),
                        new MySql.Data.MySqlClient.MySqlParameter("timestamp", timestamp)
                    );
                    break;
                case "delivered":
                    mailLog = LazyMapper.ExecuteObject<MailLogModel>(
                       "UPDATE maillog SET status = 2 WHERE id = ?id AND status <= 1 AND id = ?id;UPDATE maillog SET deliveredat = ?timestamp WHERE id = ?id AND ( deliveredat IS NULL OR deliveredat > ?timestamp);SELECT * FROM maillog WHERE id = ?id",
                        new MySql.Data.MySqlClient.MySqlParameter("id", mailLog.Id),
                        new MySql.Data.MySqlClient.MySqlParameter("timestamp", timestamp)
                    );
                    break;
                case "opened":
                    mailLog = LazyMapper.ExecuteObject<MailLogModel>(
                        "UPDATE maillog SET status = 3 WHERE id = ?id AND status <= 2 AND id = ?id;UPDATE maillog SET openedat = ?timestamp WHERE id = ?id AND ( openedat IS NULL OR openedat > ?timestamp);SELECT * FROM maillog WHERE id = ?id",
                        new MySql.Data.MySqlClient.MySqlParameter("id", mailLog.Id),
                        new MySql.Data.MySqlClient.MySqlParameter("timestamp", timestamp)
                    );
                    break;
                case "clicked":
                    mailLog = LazyMapper.ExecuteObject<MailLogModel>(
                        "UPDATE maillog SET status = 4 WHERE id = ?id AND status <= 3 AND id = ?id;UPDATE maillog SET interactedat = ?timestamp WHERE id = ?id AND ( interactedat IS NULL OR interactedat > ?timestamp);SELECT * FROM maillog WHERE id = ?id",
                        new MySql.Data.MySqlClient.MySqlParameter("id", mailLog.Id),
                        new MySql.Data.MySqlClient.MySqlParameter("timestamp", timestamp)
                    );
                    break;
                case "complained":
                    mailLog = LazyMapper.ExecuteObject<MailLogModel>(
                        "UPDATE maillog SET status = 2 WHERE id = ?id AND status <= 1 AND id = ?id;UPDATE maillog SET reportedat = ?timestamp WHERE id = ?id AND ( reportedat IS NULL OR reportedat > ?timestamp);SELECT * FROM maillog WHERE id = ?id",
                        new MySql.Data.MySqlClient.MySqlParameter("id", mailLog.Id),
                        new MySql.Data.MySqlClient.MySqlParameter("timestamp", timestamp)
                    );
                    break;
            }

            mailLog = LazyMapper.ExecuteObject<MailLogModel>(
                    "UPDATE maillog SET lasteventrecievedat = ?timestamp WHERE id = ?id AND (lasteventrecievedat IS NULL OR lasteventrecievedat < ?timestamp);SELECT * FROM maillog WHERE id = ?id",
                    new MySql.Data.MySqlClient.MySqlParameter("id", mailLog.Id),
                    new MySql.Data.MySqlClient.MySqlParameter("timestamp", timestamp)
                );
        }

        private static JObject GetMailGunEvents(string pageUrl)
        {
            if (string.IsNullOrWhiteSpace(pageUrl))
            {
                var lastDate = LazyMapper.ExecuteScalar<DateTime>("SELECT CAST(IFNULL(MAX(lasteventrecievedat),?defaultmaxcreatedat) AS DATETIME) FROM maillog", new MySql.Data.MySqlClient.MySqlParameter("defaultmaxcreatedat", DateTime.UtcNow.AddDays(-28)));


                lastDate = lastDate.AddMinutes(-60);



                var client = new RestClient();
                client.BaseUrl = new Uri("https://api.mailgun.net/v3");

                client.Authenticator = new HttpBasicAuthenticator("api", System.Configuration.ConfigurationManager.AppSettings["MailGunApiKey"]);
                var request = new RestRequest();
                request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MailGunDomain"], ParameterType.UrlSegment);
                request.Resource = System.Configuration.ConfigurationManager.AppSettings["MailGunDomain"] + "/events";
                request.AddParameter("begin", lastDate.ToString("ddd, d MMM yyyy HH:mm:ss") + " GMT");
                request.AddParameter("ascending", "yes");
                request.AddParameter("limit", "100");

                request.Method = Method.GET;
                var mailGunResult = client.Execute(request);

                if (mailGunResult.StatusCode != System.Net.HttpStatusCode.OK)
                    throw new Exception(mailGunResult.Content);


                return JObject.Parse(mailGunResult.Content);
            }
            else
            {


                var client = new RestClient();
                client.BaseUrl = new Uri(pageUrl);

                client.Authenticator = new HttpBasicAuthenticator("api", System.Configuration.ConfigurationManager.AppSettings["MailGunApiKey"]);
                var request = new RestRequest();
                request.Method = Method.GET;
                var mailGunResult = client.Execute(request);
                if (mailGunResult.StatusCode != System.Net.HttpStatusCode.OK)
                    throw new Exception(mailGunResult.Content);


                return JObject.Parse(mailGunResult.Content);
            }
        }
    }
}