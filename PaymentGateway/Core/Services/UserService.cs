﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using MySql.Data.MySqlClient;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class UserService
    {
        #region List 

        public class GetUserListResponse
        {
            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }

            public class Item
            {
                public Guid Id { get; set; }
                public string Name { get; set; }
                public string Surname { get; set; }
                public string EmailAddress { get; set; }
                public string Username { get; set; }
            }
        }

        public class GetUserListRequest
        {
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
        }

        public static GetUserListResponse GetUserList(GetUserListRequest request)
        {
            var sqlWhere = "1 = 1";

            var sql = new StringBuilder();

            sql.AppendLine("SELECT COUNT(*) FROM user WHERE " + sqlWhere + ";");
            sql.AppendLine("SELECT * FROM user WHERE " + sqlWhere +
                           " ORDER BY name, surname ASC LIMIT ?pageOffset, ?pageLimit;");


            var sourceDataSet = MySqlHelper.ExecuteDataset(
                ConfigurationManager.AppSettings["MySqlConnection"],
                sql.ToString(),
                new MySqlParameter("pageOffset", request.PageIndex*request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize)
            );

            var result = new GetUserListResponse();
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items =
                (List<GetUserListResponse.Item>) LazyMapper.Hydrate<GetUserListResponse.Item>(sourceDataSet.Tables[1]);
            return result;
        }

        #endregion

        #region Set

        public class SetRequest
        {
            public Guid UserId { get; set; }
            public UserModel User { get; set; }
        }

        public class SetResponse
        {
            public Guid UserId { get; set; }
            public UserModel User { get; set; }
        }

        public static SetResponse Set(SetRequest request)
        {
            var sql = new StringBuilder();

            var sqlParams = new List<MySqlParameter>();

            if (request.UserId == Guid.Empty)
            {
                sql.AppendLine(
                    "INSERT INTO user ( id, deleted, name, surname, username, emailaddress, mobilephone ) VALUES ( ?id, ?deleted, ?name, ?surname, ?username, ?emailaddress, ?mobilephone );");
                sqlParams.Add(new MySqlParameter("id", Guid.NewGuid()));
            }
            else
            {
                sql.AppendLine(
                    "UPDATE user SET name = ?name, surname = ?surname, username = ?username, emailaddress = ?emailaddress, mobilephone = ?mobilephone, deleted = ?deleted WHERE id = ?id;");
                sqlParams.Add(new MySqlParameter("id", request.UserId));
            }

            sql.AppendLine("DELETE FROM usersystemrole WHERE userid = ?id;");

            for (var i = 0; i < request.User.SystemRoleIds.Count; i++)
            {
                sql.AppendLine("INSERT INTO usersystemrole ( userid, systemroleid ) VALUES( ?id, ?systemroleid_" + i +
                               ");");
                sqlParams.Add(new MySqlParameter("systemroleid_" + i, request.User.SystemRoleIds[i]));
            }

            sql.AppendLine("SELECT * FROM user WHERE id = ?id;");
            sql.AppendLine("SELECT systemroleid FROM usersystemrole WHERE userid = ?id;");
            sqlParams.Add(new MySqlParameter("deleted", request.User.Deleted));
            sqlParams.Add(new MySqlParameter("name", request.User.Name));
            sqlParams.Add(new MySqlParameter("surname", request.User.Surname));
            sqlParams.Add(new MySqlParameter("emailaddress", request.User.EmailAddress));
            sqlParams.Add(new MySqlParameter("mobilephone", request.User.MobilePhone));
            sqlParams.Add(new MySqlParameter("username", request.User.Username));

            var executeResult = MySqlHelper.ExecuteDataset(
                ConfigurationManager.AppSettings["MySqlConnection"],
                sql.ToString(),
                sqlParams.ToArray()
            );

            if (executeResult.Tables[0].Rows.Count < 1)
                throw new Exception("Unable to update the user with id '" + request.UserId + "'.");

            var user = LazyMapper.Hydrate<UserModel>(executeResult.Tables[0].Rows[0]);
            user.SystemRoleIds = (List<Guid>) LazyMapper.Hydrate<Guid>(executeResult.Tables[1]);


            if (request.UserId != Guid.Empty)
                return new SetResponse
                {
                    UserId = request.UserId,
                    User = user
                };

            // Create a reset link
            var userReset = LazyMapper.ExecuteObject<UserResetModel>(
                "INSERT INTO userreset ( id, expires, userid ) VALUES ( ?id, ?expires, ?userid);SELECT * FROM userreset WHERE id = ?id;",
                new MySqlParameter("id", Guid.NewGuid()),
                new MySqlParameter("expires", DateTime.UtcNow.AddDays(7)),
                new MySqlParameter("userid", user.Id)
            );
            // Now send a reset (aka welcome) email
            var replacements = new Dictionary<string, string>();
            replacements.Add("name", user.Name);
            replacements.Add("surname", user.Surname);
            replacements.Add("username", user.Username);
            replacements.Add("reseturl", "~/controlpanel/reset/" + userReset.Id);
            MailService.SendSystemMail(
                HttpContext.Current.Server.MapPath("/areas/controlpanel/_resx/templates/PasswordSetup.html"),
                replacements,
                user.EmailAddress
            );

            return new SetResponse
            {
                UserId = request.UserId,
                User = user
            };
        }

        #endregion

        #region Get

        public class GetRequest
        {
            public Guid UserId { get; set; }
        }

        public class GetResponse
        {
            public Guid UserId { get; set; }
            public UserModel User { get; set; }
        }

        public static GetResponse Get(GetRequest request)
        {
            var executeResult = MySqlHelper.ExecuteDataset(
                ConfigurationManager.AppSettings["MySqlConnection"],
                "SELECT * FROM user WHERE id = ?id;SELECT systemroleid FROM usersystemrole WHERE userid = ?id;",
                new MySqlParameter("id", request.UserId)
            );

            if (executeResult.Tables[0].Rows.Count < 1)
                throw new Exception("Unable to load the user with id '" + request.UserId + "'.");

            var user = LazyMapper.Hydrate<UserModel>(executeResult.Tables[0].Rows[0]);
            user.SystemRoleIds = (List<Guid>) LazyMapper.Hydrate<Guid>(executeResult.Tables[1]);

            return new GetResponse
            {
                UserId = request.UserId,
                User = user
            };
        }

        #endregion

        #region GetIsUsernameUnique

        public class GetIsUsernameUniqueRequest
        {
            public string Username { get; set; }
            public Guid UserId { get; set; }
        }

        public class GetIsUsernameUniqueResponse
        {
            public string Username { get; set; }
            public bool IsUnique { get; set; }
        }

        public static GetIsUsernameUniqueResponse GetIsUsernameUnique(GetIsUsernameUniqueRequest request)
        {
            var isUnique = LazyMapper.ExecuteScalar<long>(
                               "SELECT count(*) FROM user WHERE username = ?username " +
                               (request.UserId == Guid.Empty ? ";" : " AND id <> ?userid;"),
                               new MySqlParameter("username", request.Username),
                               new MySqlParameter("userid", request.UserId)
                           ) == 0;

            return new GetIsUsernameUniqueResponse
            {
                Username = request.Username,
                IsUnique = isUnique
            };
        }

        #endregion
    }
}