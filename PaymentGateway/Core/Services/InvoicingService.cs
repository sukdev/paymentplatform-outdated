﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using StreamAMG.OTTPlatform.Core.Utilities;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class InvoicingService
    {

        public static void RunInvoiceBatch()
        {
            // check if its a monday to friday 10:00am to 4:00pm if not abort

            if (DateTime.UtcNow.DayOfWeek == DayOfWeek.Saturday || DateTime.UtcNow.DayOfWeek == DayOfWeek.Sunday)
                return;

            if (DateTime.UtcNow.Hour < 10 || DateTime.UtcNow.Hour > 16)
                return;

            var random = new Random();
            

            NLog.LogManager.GetCurrentClassLogger().Info("InvoicingService.RunInvoiceBatch started");
            System.Threading.Thread.Sleep(random.Next(15000, 60000));

            var lockKey = BatchLockService.GetLock("invoicebatch", new TimeSpan(0, 15, 0));
            if (lockKey == Guid.Empty)
            {
                NLog.LogManager.GetCurrentClassLogger().Info("InvoicingService.RunInvoiceBatch process already running, unable to get lock");
            }
            else
            {
                
                var customerSusbcriptionIds = MySqlHelper.ExecuteDataset(
                    System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                    "SELECT customersubscription.id FROM customersubscription INNER JOIN customer ON customer.id = customersubscription.customerid WHERE customersubscription.status = 1 AND ( customersubscription.expirydate IS NULL OR (customersubscription.expirydate >= ?now AND customersubscription.expirydate > customersubscription.renewaldate ) ) AND customersubscription.renewaldate <= ?now AND (SELECT count(id) FROM customertransaction WHERE customersubscriptionid = customersubscription.id AND submittedat >= ?submittedafter AND customertransaction.customerbillingprofileid = customer.customerbillingprofileid) = 0 LIMIT 0,100;",
                    new MySqlParameter("now", DateTime.UtcNow),
                    new MySqlParameter("submittedafter", DateTime.UtcNow.AddHours(-24))
                ).Tables[0];

                var i = 0;
                foreach (DataRow dr in customerSusbcriptionIds.Rows)
                {
                    System.Threading.Thread.Sleep(random.Next(5000, 10000));

                    if (BatchLockService.KeepLock("invoicebatch", new TimeSpan(0, 15, 0), lockKey) == Guid.Empty)
                    {
                        NLog.LogManager.GetCurrentClassLogger().Info("InvoicingService.RunInvoiceBatch aborting, lock was taken");
                        break;
                    }

                    try
                    {
                        // Check we dont have a transaction in warning for this customer subscription, if we don dont try and invoice again
                        if (LazyMapper.ExecuteScalar<long>(
                                "SELECT count(*) FROM customertransaction WHERE customerid = (SELECT customerid FROM customersubscription WHERE id = ?customersubscriptionid) AND submittedat IS NOT NULL AND status = 0;",
                                new MySqlParameter("customersubscriptionid", new Guid(dr[0].ToString()))
                            ) > 0)
                        {
                            NLog.LogManager.GetCurrentClassLogger().Warn("Not issuing invoice for customer subscription with the id '" + dr[0] + "' because the customer has a transaction in warning state.");
                            continue;
                        }

                        // Check we havent tried billing for this in the past 24 hours
                        if (LazyMapper.ExecuteScalar<long>(
                                "SELECT count(customersubscriptionid) FROM customertransaction INNER JOIN customersubscription ON customersubscription.id = customertransaction.customersubscriptionid INNER JOIN customer ON customer.id = customersubscription.customerid WHERE customertransaction.customersubscriptionid = ?customersubscriptionid AND customertransaction.submittedat >= ?submittedafter AND customertransaction.customerbillingprofileid = customer.customerbillingprofileid;",
                                new MySqlParameter("customersubscriptionid", new Guid(dr[0].ToString())),
                                new MySqlParameter("submittedafter", DateTime.UtcNow.AddHours(-24))
                            ) > 0)
                        {
                            NLog.LogManager.GetCurrentClassLogger().Warn("Not issuing invoice for customer subscription with the id '" + dr[0] + "' because a invoice has been issued in teh past 24 hours.");
                            continue;
                        }

                        var prepareTransactionResult =
                            CustomerTransactionService.PrepareTransaction(
                                new CustomerTransactionService.PrepareTransactionRequest
                                {
                                    Batch = true,
                                    CustomerSubscriptionId = new Guid(dr[0].ToString())
                                });

                        CustomerTransactionService.ProcessTransaction(new CustomerTransactionService.
                            ProcessTransactionRequest
                            {
                                TransactionId = prepareTransactionResult.Transaction.Id
                            });
                        NLog.LogManager.GetCurrentClassLogger()
                            .Info("Issued invoice for customer subscription with the id '" + dr[0] + "'.");
                    }
                    catch (Exception ex)
                    {
                        NLog.LogManager.GetCurrentClassLogger().Warn("Exception issiung invoice for customer subscription with the id '" + dr[0] + "'" + ex.Message + ".", ex);
                    }
                    i++;
                    System.Threading.Thread.Sleep(random.Next(5000, 10000));
                }
            }
            NLog.LogManager.GetCurrentClassLogger().Info("InvoicingService.RunInvoiceBatch completed");
        }
    }
}