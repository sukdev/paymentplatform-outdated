﻿using StreamAMG.OTTPlatform.Core.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Threading;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class BatchLockService
    {
        private static Object getLock = new Object();
        private static Object keepLock = new Object();

        public static Guid GetLock(string id, TimeSpan timeout)
        {
            lock (getLock)
            {
                var now = DateTime.UtcNow;
                var result = LazyMapper.ExecuteScalar<Guid>(
                    "INSERT IGNORE INTO batchlock (id,machine,lockkey,expiresat) VALUES (?id,?machine,?key,?now);UPDATE batchlock SET lockkey = ?key, machine = ?machine, expiresat = ?expiresat WHERE id = ?id AND expiresat <= ?now;SELECT IFNULL( (SELECT lockkey FROM batchlock WHERE id = ?id AND lockkey = ?key AND machine = ?machine), '00000000-0000-0000-0000-000000000000' );",
                    new MySqlParameter("id", id),
                    new MySqlParameter("key", Guid.NewGuid()),
                    new MySqlParameter("machine", Environment.MachineName),
                    new MySqlParameter("now", now),
                    new MySqlParameter("expiresat", now.Add(timeout))
                );

                if (result == Guid.Empty)
                    return Guid.Empty;


                // Wait 5 seconds and double check
                Thread.Sleep(5000);
                result = LazyMapper.ExecuteScalar<Guid>(
                    "SELECT IFNULL( (SELECT lockkey FROM batchlock WHERE id = ?id AND lockkey = ?key AND machine = ?machine), '00000000-0000-0000-0000-000000000000' );",
                    new MySqlParameter("id", id),
                    new MySqlParameter("key", result),
                    new MySqlParameter("machine", Environment.MachineName),
                    new MySqlParameter("now", now),
                    new MySqlParameter("expiresat", now.Add(timeout))
                );

                return result;
            }
        }

        public static Guid KeepLock(string id, TimeSpan timeout, Guid key)
        {
            lock (keepLock)
            {
                var now = DateTime.UtcNow;
                var result = LazyMapper.ExecuteScalar<Guid>(
                    "UPDATE batchlock SET expiresat = ?expiresat WHERE id = ?id AND lockkey = ?key AND machine = ?machine;SELECT IFNULL( (SELECT lockkey FROM batchlock WHERE id = ?id AND lockkey = ?key AND machine = ?machine), '00000000-0000-0000-0000-000000000000' );",
                    new MySqlParameter("id", id),
                    new MySqlParameter("key", key),
                     new MySqlParameter("machine", Environment.MachineName),
                    new MySqlParameter("now", now),
                    new MySqlParameter("expiresat", now.Add(timeout))
                );

                if (result == Guid.Empty)
                    return Guid.Empty;

                // Wait 5 seconds and double check
                Thread.Sleep(5000);
                result = LazyMapper.ExecuteScalar<Guid>(
                    "SELECT IFNULL( (SELECT lockkey FROM batchlock WHERE id = ?id AND lockkey = ?key AND machine = ?machine), '00000000-0000-0000-0000-000000000000' );",
                    new MySqlParameter("id", id),
                    new MySqlParameter("key", key),
                    new MySqlParameter("machine", Environment.MachineName),
                    new MySqlParameter("expiresat", now.Add(timeout))
                );

                return result;
            }
        }
    }
}