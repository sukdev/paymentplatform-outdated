﻿using MySql.Data.MySqlClient;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace StreamAMG.OTTPlatform.Core.Services
{

    public class OfferVoucherService
    {

        #region List 

        public class GetOfferVoucherListResponse
        {
            public class Item
            {
                public Guid Id { get; set; }
                public string Code { get; set; }
                public DateTime CreatedAt { get; set; }
                public DateTime ValidFrom { get; set; }
                public DateTime ValidTo { get; set; }
                public string Campaign { get; set; }
                public DateTime? RedeemedAt { get; set; }
                public Guid? RedeemedByCustomerId { get; set; }
                public string RedeemedByCustomerEmailAddress { get; set; }

            }
            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }
        }

        public class GetOfferVoucherListRequest
        {
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public Guid OfferId { get; set; }
        }

        public static GetOfferVoucherListResponse GetOfferVoucherList(GetOfferVoucherListRequest request)
        {
            var sqlWhere = "1 = 1";

            var sql = new StringBuilder();

            if (request.OfferId != Guid.Empty)
                sqlWhere += " AND offerid = ?offerid";

            sql.AppendLine("SELECT COUNT(*) FROM offervoucher WHERE " + sqlWhere + ";");
            sql.AppendLine("SELECT customer.emailaddress AS redeemedbycustomeremailaddress, IFNULL(customer.id,'00000000-0000-0000-0000-000000000000') AS redeemedbycustomerid, offervoucher.id, offervoucher.code, offervoucher.createdat, offervoucher.validfrom, offervoucher.validto, offervoucher.campaign, offervoucher.redeemedat FROM offervoucher LEFT JOIN customersubscription ON customersubscription.id = offervoucher.customersubscriptionplanid LEFT JOIN customer ON customer.id = customersubscription.customerid WHERE " + sqlWhere + " ORDER BY offervoucher.createdat DESC LIMIT ?pageOffset, ?pageLimit;");


            var sourceDataSet = MySqlHelper.ExecuteDataset(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                sql.ToString(),
                new MySqlParameter("offerId", request.OfferId),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize)
           );

            var result = new GetOfferVoucherListResponse();
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<GetOfferVoucherListResponse.Item>)LazyMapper.Hydrate<GetOfferVoucherListResponse.Item>(sourceDataSet.Tables[1]);
            return result;
        }

        #endregion

        public class CreateOfferVoucherRequest
        {
            public Guid OfferId { get; set; }
            public int QuantityToCreate { get; set; }
            public DateTime ValidFrom { get; set; }
            public DateTime ValidTo { get; set; }
            public string Campaign { get; set; }
        }

        public class CreateOfferVoucherResponse
        {

        }

        public static CreateOfferVoucherResponse CreateOfferVoucher(CreateOfferVoucherRequest request)
        {
            if (request.QuantityToCreate > 10000)
                throw new ArgumentOutOfRangeException("Cannot create more than 10,000 vouchers");

            if (request.QuantityToCreate < 1)
                throw new ArgumentOutOfRangeException("Must create at least one voucher");

            var offer = LazyMapper.GetObjectById<OfferModel>(request.OfferId);

            if (offer == null)
                throw new Exception("Unable to create new offer vouchers, offer '" + request.OfferId.ToString() + "' does not exist.");

            var sql = new StringBuilder();
            for (var i = 0; i < request.QuantityToCreate; i++)
                sql.AppendLine("INSERT INTO offervoucher(id, createdat, updatedat, offerid, validfrom, validto, campaign, code) VALUES ( '" + Guid.NewGuid().ToString() + "', ?utcnow, ?utcnow, ?offerid, ?validfrom, ?validto, ?campaign, null );");

            DataBase.ExecuteQuery(
                sql.ToString(),
                new MySqlParameter("utcnow", DateTime.UtcNow),
                new MySqlParameter("offerid", request.OfferId),
                new MySqlParameter("validfrom", request.ValidFrom),
                new MySqlParameter("validto", request.ValidTo),
                new MySqlParameter("campaign", request.Campaign)
            );

            return new CreateOfferVoucherResponse { };
        }


        private static Object PopulateVoucherCodesLock = new Object();

        public static void PopulateVoucherCodes()
        {
            NLog.LogManager.GetCurrentClassLogger().Info("OfferVoucherService.PopulateVoucherCodes started");

            lock (PopulateVoucherCodesLock)
            {
                var lockKey = BatchLockService.GetLock("populatevouchercodes", new TimeSpan(0, 1, 0));

                if (lockKey == Guid.Empty)
                {
                    NLog.LogManager.GetCurrentClassLogger().Info("Process already running, unable to get lock");
                    return;
                }
                else
                {
                    var customerSusbcriptionIds = MySqlHelper.ExecuteDataset(
                        System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                        "SELECT id FROM offervoucher WHERE code IS NULL LIMIT 0, 1000;"
                    ).Tables[0];

                    var i = 0;
                    foreach (DataRow dr in customerSusbcriptionIds.Rows)
                    {
                        var vouchercode = Utility.Helpers.RandomString(7, "ABCDEFGHIJKLMNOPQRSTUVWXYZ").ToUpper();

                        vouchercode = vouchercode + Regex.Match(Utility.Helpers.CalculateMD5Hash(vouchercode), @"[A-Z]").ToString().ToUpper();

                        if (vouchercode.Length == 7)
                            vouchercode = vouchercode + "A";


                        try
                        {
                            MySqlHelper.ExecuteNonQuery(
                                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                                "UPDATE offervoucher SET code = ?code WHERE id = ?id AND code IS NULL;",
                                new MySqlParameter("id", dr[0]),
                                new MySqlParameter("code", vouchercode)
                           );
                        }
                        catch (Exception ex)
                        {
                            NLog.LogManager.GetCurrentClassLogger().Error("Unable to populate the offer voucher '" + dr[0].ToString() + "' with a unique code " + ex.Message, ex);
                        }
                        i++;
                    }
                }
                NLog.LogManager.GetCurrentClassLogger().Info("OfferVoucherService.PopulateVoucherCodes complete");
            }
        }

    }
}
