﻿using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using NLog;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class MailService
    {
        

        public static void SendSystemMail(string templatePath, Dictionary<string, string> contentReplacements, string mailTo)
        {
            var templateHtml = new XmlDocument();
            templateHtml.Load(templatePath);
            var emailTitle = templateHtml.SelectSingleNode("html/head/title").InnerText;
            var emailBody = templateHtml.SelectSingleNode("html/body").InnerXml;

            var message = new System.Net.Mail.MailMessage();
            message.To.Add(mailTo);
            message.Subject = DoSystemMailReplacements(emailTitle, contentReplacements);
            message.Body = DoSystemMailReplacements(emailBody, contentReplacements);
            message.IsBodyHtml = true;
            new System.Net.Mail.SmtpClient().Send(message);
        }


        public static void SendMail(string mailTemplateId, Guid customerId, Dictionary<string, string> replacements)
        {
            var getCustomerProfileResponse = CustomerService.GetCustomerProfile(new CustomerService.GetCustomerProfileRequest
            {
                CustomerId = customerId
            });

            var mailTemplate = LazyMapper.GetObjectById<MailTemplateModel>(mailTemplateId, getCustomerProfileResponse.CustomerProfile.Language);           

            replacements.Add("firstname", getCustomerProfileResponse.CustomerProfile.FirstName);
            replacements.Add("lastname", getCustomerProfileResponse.CustomerProfile.LastName);
            replacements.Add("emailaddress", getCustomerProfileResponse.CustomerProfile.EmailAddress);

            var mailLog = LazyMapper.ExecuteObject<MailLogModel>(
                "INSERT INTO maillog ( id, createdat, updatedat, status, customerid, mailto, subject, body,mailtemplateid ) VALUES ( ?id, ?now, ?now, 0, ?customerid, ?mailto, ?subject, ?body, ?mailtemplateid ); SELECT * FROM maillog WHERE id = ?id;",
                new MySqlParameter("id", Guid.NewGuid()),
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("customerid", customerId),
                new MySqlParameter("mailtemplateid", mailTemplateId),
                new MySqlParameter("mailto", getCustomerProfileResponse.CustomerProfile.EmailAddress),
                new MySqlParameter("subject", DoMailReplacements(mailTemplate.Subject, replacements)),
                new MySqlParameter("body", DoMailReplacements(mailTemplate.Body, replacements))
            );


            try
            {
                var client = new RestClient();
                client.BaseUrl = new Uri("https://api.mailgun.net/v3");

                client.Authenticator = new HttpBasicAuthenticator("api", System.Configuration.ConfigurationManager.AppSettings["MailGunApiKey"]);
                var request = new RestRequest();
                request.AddParameter("domain", System.Configuration.ConfigurationManager.AppSettings["MailGunDomain"], ParameterType.UrlSegment);
                request.Resource = System.Configuration.ConfigurationManager.AppSettings["MailGunDomain"] + "/messages";
                request.AddParameter("from", String.Format("{0} <{1}>", System.Configuration.ConfigurationManager.AppSettings["MailGunFromName"], System.Configuration.ConfigurationManager.AppSettings["MailGunFromEmail"]));
                request.AddParameter("o:tracking", "yes");
                request.AddParameter("o:tracking-clicks", "yes");
                request.AddParameter("o:tracking-opens", "yes");
                request.AddParameter("to", mailLog.MailTo);
                request.AddParameter("subject", mailLog.Subject);
                request.AddParameter("html", mailLog.Body);



                request.Method = Method.POST;
                var mailGunResult = client.Execute(request);

                if (mailGunResult.StatusCode != System.Net.HttpStatusCode.OK)
                    throw new Exception(mailGunResult.Content);


                var responseObject = JObject.Parse(mailGunResult.Content);
                mailLog.ReferenceId = responseObject["id"].ToString();
                mailLog.Status = 1;

            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to SendMail using the template '" + mailTemplateId + "'. Exception: {0}", ex.Message);
                mailLog.Status = -1;
            }

            mailLog = LazyMapper.ExecuteObject<MailLogModel>(
               "UPDATE maillog SET status = ?status, referenceid = ?referenceid WHERE id = ?id;SELECT * FROM maillog WHERE id = ?id;",
               new MySqlParameter("id", mailLog.Id),
               new MySqlParameter("referenceid", mailLog.ReferenceId),
               new MySqlParameter("status", mailLog.Status)
           );

        }


        private static string DoMailReplacements(string content, Dictionary<string, string> contentReplacements)
        {

            foreach (var key in contentReplacements.Keys)
                content = content.Replace("{{" + key + "}}", contentReplacements[key]);

            content = content.Replace("{{siteurl}}", System.Configuration.ConfigurationManager.AppSettings["SiteUrl"]);
            content = content.Replace("{{sitename}}", System.Configuration.ConfigurationManager.AppSettings["SiteName"]);
            return content;
        }


        private static string DoSystemMailReplacements(string content, Dictionary<string, string> contentReplacements)
        {

            var siteUrl = HttpContext.Current.Request.Url.Scheme + Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);

            content = content.Replace("{{siteurl}}", siteUrl);
            content = content.Replace("{{sitename}}", System.Configuration.ConfigurationManager.AppSettings["SiteName"]);

            foreach (var key in contentReplacements.Keys)
                content = content.Replace("{{" + key + "}}", contentReplacements[key]);

            content = content.Replace("~/", siteUrl + "/");
            return content;
        }


        #region GetMailTemplate

        public class GetMailTemplateRequest
        {
            public string MailTemplateId { get; set; }
        }

        public class GetMailTemplateResponse
        {
            public string MailTemplateId { get; set; }
            public MailTemplateModel MailTemplate { get; set; }
        }

        public static GetMailTemplateResponse GetMailtemplate(GetMailTemplateRequest request)
        {
            var mailTemplate = LazyMapper.ExecuteObject<MailTemplateModel>(
                "SELECT subject, body FROM mailtemplate WHERE id = ?id",
                new MySqlParameter("id", request.MailTemplateId)
            );

            if (mailTemplate == null)
                throw new Exception("Mail template with the id '" + request.MailTemplateId + "' not found");

            return new GetMailTemplateResponse
            {
                MailTemplateId = request.MailTemplateId,
                MailTemplate = mailTemplate
            };
        }

        #endregion

        #region SetMailTemplate

        public class SetMailTemplateRequest
        {
            public string MailTemplateId { get; set; }
            public MailTemplateModel MailTemplate { get; set; }
        }

        public class SetMailTemplateResponse
        {
            public string MailTemplateId { get; set; }
            public MailTemplateModel MailTemplate { get; set; }
        }

        public static SetMailTemplateResponse SetMailtemplate(SetMailTemplateRequest request)
        {
            var mailTemplate = LazyMapper.ExecuteObject<MailTemplateModel>(
                "UPDATE mailtemplate SET subject = ?subject, body = ?body WHERE id = ?id;SELECT subject, body FROM mailtemplate WHERE id = ?id",
                new MySqlParameter("id", request.MailTemplateId),
                new MySqlParameter("subject", request.MailTemplate.Subject),
                new MySqlParameter("body", request.MailTemplate.Body)
            );

            if (mailTemplate == null)
                throw new Exception("Mail template with the id '" + request.MailTemplateId + "' not found");

            return new SetMailTemplateResponse
            {
                MailTemplateId = request.MailTemplateId,
                MailTemplate = mailTemplate
            };
        }

        #endregion

        #region GetMailTemplateList

        public class GetMailTemplateListResult
        {
            public class Item
            {
                public string Id { get; set; }
                public string Subject { get; set; }
            }
            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }
        }

        public class GetMailTemplateListRequest
        {
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
        }

        public static GetMailTemplateListResult GetMailTemplateList(GetMailTemplateListRequest request)
        {
            var sqlWhere = "1 = 1";

            var sql = new System.Text.StringBuilder();

            sql.AppendLine("SELECT COUNT(*) FROM mailtemplate WHERE " + sqlWhere + ";");
            sql.AppendLine("SELECT * FROM mailtemplate WHERE " + sqlWhere + " ORDER BY subject LIMIT ?pageOffset, ?pageLimit;");


            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize)
           );

            var result = new GetMailTemplateListResult();
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<GetMailTemplateListResult.Item>)LazyMapper.Hydrate<GetMailTemplateListResult.Item>(sourceDataSet.Tables[1]);
            return result;
        }
        #endregion

        #region GetMailTemplateTranslation

        public class GetMailTemplateTranslationRequest
        {
            public string MailTemplateId { get; set; }
            public string Language { get; set; }
        }

        public class GetMailTemplateTranslationResponse
        {
            public string MailTemplateId { get; set; }
            public string Language { get; set; }
            public MailTemplateTranslationModel MailTemplateTranslation { get; set; }
        }

        public static GetMailTemplateTranslationResponse GetMailTemplateTransaltion(GetMailTemplateTranslationRequest request)
        {
            var mailTemplateTranslation = LazyMapper.ExecuteObject<MailTemplateTranslationModel>(
                "SELECT * FROM mailtemplatetranslation WHERE id = ?id AND language = ?language",
                new MySqlParameter("id", request.MailTemplateId),
                new MySqlParameter("language", request.Language)
            );

            if (mailTemplateTranslation == null)
                mailTemplateTranslation = new MailTemplateTranslationModel();

            return new GetMailTemplateTranslationResponse
            {
                MailTemplateId = request.MailTemplateId,
                Language = request.Language,
                MailTemplateTranslation = mailTemplateTranslation
            };
        }

        #endregion


        #region SetTransaltion

        public class SetMailTemplateTranslationRequest
        {
            public string MailTemplateId { get; set; }
            public string Language { get; set; }
            public MailTemplateTranslationModel MailTemplateTranslation { get; set; }
        }

        public class SetMailTemplateTranslationResponse
        {
            public string MailTemplateId { get; set; }
            public string Language { get; set; }
            public MailTemplateTranslationModel MailTemplateTranslation { get; set; }
        }

        public static SetMailTemplateTranslationResponse SetMailTemplateTranslation(SetMailTemplateTranslationRequest request)
        {
            var mailTemplateTranslation = LazyMapper.ExecuteObject<MailTemplateTranslationModel>(
                 "INSERT INTO mailtemplatetranslation ( id, language, subject, body ) VALUES ( ?id, ?language, ?subject, ?body ) ON DUPLICATE KEY UPDATE subject = ?subject, body = ?body; SELECT * FROM mailtemplatetranslation WHERE id = ?id;",
                new MySqlParameter("id", request.MailTemplateId),
                new MySqlParameter("language", request.Language),
                new MySqlParameter("subject", request.MailTemplateTranslation.Subject),
                new MySqlParameter("body", request.MailTemplateTranslation.Body)
            );

            if (mailTemplateTranslation == null)
                mailTemplateTranslation = new MailTemplateTranslationModel();

            return new SetMailTemplateTranslationResponse
            {
                MailTemplateId = request.MailTemplateId,
                Language = request.Language,
                MailTemplateTranslation = mailTemplateTranslation
            };
        }

        #endregion

        #region GetMailLogList

        public class GetMailLogListResponse
        {
            public class Item
            {
                public Guid Id { get; set; }
                public Guid CustomerId { get; set; }
                public DateTime CreatedAt { get; set; }
                public string Subject { get; set; }
                public string MailTo { get; set; }
                public int Status { get; set; }
            }
            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }
            public GetMailLogListRequest Request { get; set; }
        }

        public class GetMailLogListRequest
        {
            
            public enum MailLogStatuses
            {
                Rejected = -2,
                Failed = -1,
                Prepared = 0,
                Sent = 1,
                Delivered = 2,
                Opened = 3,
                Interacted = 4                     
            }

            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public Guid CustomerId { get; set; }
            public MailLogStatuses? MailLogStatus { get; set; }
            public string MailTemplateId { get; set; }
            public DateTime? DateFrom { get; set; }
            public DateTime? DateTo { get; set; }
        }

        public static GetMailLogListResponse GetMailLogList(GetMailLogListRequest request)
        {
            var sqlWhere = "1 = 1";

            if (request.CustomerId != Guid.Empty)
                sqlWhere += " AND maillog.customerid = '" + request.CustomerId + "'";

            if (!string.IsNullOrWhiteSpace(request.MailTemplateId))
                sqlWhere += " AND maillog.mailtemplateid = ?mailtemplateid";

            if (request.DateFrom.HasValue)
                sqlWhere += " AND maillog.createdat >= ?datefrom";

            if (request.DateTo.HasValue)
                sqlWhere += " AND maillog.createdat <= ?dateto";


            if (request.MailLogStatus.HasValue)
            {
                switch (request.MailLogStatus)
                {
                    case GetMailLogListRequest.MailLogStatuses.Rejected:
                        sqlWhere += " AND status = -2";
                        break;

                    case GetMailLogListRequest.MailLogStatuses.Failed:
                        sqlWhere += " AND status = -1";
                        break;

                    case GetMailLogListRequest.MailLogStatuses.Prepared:
                        sqlWhere += " AND status = 0";
                        break;

                    case GetMailLogListRequest.MailLogStatuses.Sent:
                        sqlWhere += " AND status = 1";                        
                        break;
                    case GetMailLogListRequest.MailLogStatuses.Delivered:
                        sqlWhere += " AND status = 2";
                        break;

                    case GetMailLogListRequest.MailLogStatuses.Opened:
                        sqlWhere += " AND status = 3";
                        break;

                    case GetMailLogListRequest.MailLogStatuses.Interacted:
                        sqlWhere += " AND status = 4";
                        break;                        
                }

            }

            var sql = new System.Text.StringBuilder();

            sql.AppendLine("SELECT COUNT(*) FROM maillog WHERE " + sqlWhere + ";");
            sql.AppendLine("SELECT * FROM maillog WHERE " + sqlWhere + " ORDER BY createdat DESC LIMIT ?pageOffset, ?pageLimit;");


            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize),
                new MySqlParameter("mailtemplateid", request.MailTemplateId),
                new MySqlParameter("datefrom", request.DateFrom),
                new MySqlParameter("dateto", request.DateTo)
           );

            var result = new GetMailLogListResponse();
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<GetMailLogListResponse.Item>)LazyMapper.Hydrate<GetMailLogListResponse.Item>(sourceDataSet.Tables[1]);
            result.Request = request;
            return result;
        }

        #endregion

        #region GetMailLog

        public class GetMailLogRequest
        {            
            public Guid MailLogId { get; set; }
            
        }

        public class GetMailLogResponse
        {
            public Guid MailLogId { get; set; }
            public MailLogModel MailLog { get; set; }
        }

        public static GetMailLogResponse GetMailLog(GetMailLogRequest request)
        {
            var mailLog = LazyMapper.ExecuteObject<MailLogModel>(
                "SELECT * FROM maillog WHERE id = ?id",
                new MySqlParameter("id", request.MailLogId)
            );

            if (mailLog == null)
                throw new Exception("Mail log with the id '" + request.MailLogId + "' not found");

            return new GetMailLogResponse
            {
                MailLogId = request.MailLogId,
                MailLog = mailLog
            };
        }

        #endregion

    }
}
