﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Security;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class CustomerService
    {
       

        public class CustomerProfile
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailAddress { get; set; }
            public string Password { get; set; }
            public string Language { get; set; }
        }

        public class CustomerPermissions
        {            
            public DateTime? FullAccessUntil { get; set; }
            public bool Blocked { get; set; }
        }


        #region " Get "

        public class GetCustomerRequest
        {
            public Guid CustomerId { get; set; }
        }

        public class GetCustomerResponse
        {
            public CustomerModel Customer { get; set; }
        }

        public static GetCustomerResponse GetCustomer(GetCustomerRequest request)
        {
            var customer = LazyMapper.ExecuteObject<CustomerModel>(
                "SELECT * FROM customer WHERE id = ?id",
                new MySqlParameter("id", request.CustomerId)
            );

            if (customer == null)
                throw new Exception("Customer with the id '" + request.CustomerId + "' not found");

            return new GetCustomerResponse
            {
                Customer = customer
            };
        }

        #endregion

        #region " GetProfile "

        public class GetCustomerProfileRequest
        {
            public Guid CustomerId { get; set; }
        }

        public class GetCustomerProfileResponse
        {
            public CustomerProfile CustomerProfile { get; set; }
        }

        public static GetCustomerProfileResponse GetCustomerProfile(GetCustomerProfileRequest request)
        {
            var customerProfile = LazyMapper.ExecuteObject<CustomerProfile>(
                "SELECT firstname, lastname, emailaddress, language FROM customer WHERE id = ?id",
                new MySqlParameter("id", request.CustomerId)
            );

            if (customerProfile == null)
                throw new Exception("Customer with the id '" + request.CustomerId + "' not found");

            return new GetCustomerProfileResponse
            {
                CustomerProfile = customerProfile
            };
        }

        #endregion

        #region " SetProfile "

        public class SetCustomerProfileRequest
        {
            public Guid CustomerId { get; set; }
            public CustomerProfile CustomerProfile { get; set; }
        }

        public class SetCustomerProfileResponse
        {
            public CustomerProfile CustomerProfile { get; set; }
        }

        public static SetCustomerProfileResponse SetCustomerProfile(SetCustomerProfileRequest request)
        {
            var customerProfile = LazyMapper.ExecuteObject<CustomerProfile>(
                "SELECT firstname, lastname, emailaddress, password FROM customer WHERE id = ?id",
                new MySqlParameter("id", request.CustomerId)
            );

            if (customerProfile == null)
                throw new Exception("Customer with the id '" + request.CustomerId + "' not found");

            customerProfile = LazyMapper.ExecuteObject<CustomerProfile>(
              "UPDATE customer SET updatedat = ?now, firstname = ?firstname, lastname = ?lastname, emailaddress = ?emailaddress, language = ?language, password = ?password WHERE id = ?id;SELECT firstname, lastname, emailaddress FROM customer WHERE id = ?id;",
              new MySqlParameter("id", request.CustomerId),
              new MySqlParameter("now", DateTime.UtcNow),
              new MySqlParameter("firstname", request.CustomerProfile.FirstName),
              new MySqlParameter("lastname", request.CustomerProfile.LastName),
              new MySqlParameter("emailaddress", request.CustomerProfile.EmailAddress),
              new MySqlParameter("language", request.CustomerProfile.Language),
              new MySqlParameter("password", string.IsNullOrWhiteSpace(request.CustomerProfile.Password) ? customerProfile.Password : BCrypt.HashPassword(request.CustomerProfile.Password))
            );

            if (customerProfile == null)
                throw new Exception("Customer with the id '" + request.CustomerId + "' could not be loaded after committing changes");

            Refresh(new RefreshRequest
            {
                CustomerId = request.CustomerId
            });

            return new SetCustomerProfileResponse
            {
                CustomerProfile = customerProfile
            };
        }

        #endregion

        #region " Refresh "

        public class RefreshRequest
        {
            public Guid CustomerId { get; set; }
        }

        public class RefreshResponse
        {

        }

        public static RefreshResponse Refresh(RefreshRequest request)
        {

            var customer = LazyMapper.GetObjectById<CustomerModel>(request.CustomerId);
            var customerSubscriptionCount = 0;
            var customerNonExpiringSubscriptionCount = 0;
            var customerEntitlements = new List<string>();
            foreach (DataRow subscriptionRow in MySqlHelper.ExecuteDataset(System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"], "SELECT subscriptionplan.entitlements, customersubscription.expirydate FROM subscriptionplan INNER JOIN customersubscription ON customersubscription.subscriptionplanid = subscriptionplan.id WHERE customersubscription.customerid = ?customerid AND customersubscription.status = 1 AND (customersubscription.expirydate >= ?now OR customersubscription.expirydate IS NULL);", new MySqlParameter("now", DateTime.UtcNow), new MySqlParameter("customerid", customer.Id)).Tables[0].Rows)
            {
                customerSubscriptionCount++;
                if (string.IsNullOrWhiteSpace(subscriptionRow["expirydate"].ToString()))
                    customerNonExpiringSubscriptionCount++;

                foreach (var entitlement in subscriptionRow["entitlements"].ToString().Split(','))
                {
                    if (string.IsNullOrWhiteSpace(entitlement) || customerEntitlements.Contains(entitlement.Trim().ToLowerInvariant()))
                        continue;
                    customerEntitlements.Add(entitlement.Trim().ToLowerInvariant());
                }
            }

            var customerBillingProfile = LazyMapper.GetObjectById<CustomerBillingProfileModel>(customer.CustomerBillingProfileId);

            MySqlHelper.ExecuteNonQuery(
                 System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                 @"INSERT INTO customersessionsummary ( 
                createdat, 
                updatedat, 
                customerid, 
                customerfirstname, 
                customerlastname, 
                customeremailaddress, 
                customerdeleted, 
                customersubscriptioncount, 
                customernonexpiringsubscriptioncount,
                customerentitlements, 
                customerfullaccessuntil, 
                customerbillingprofileprovider, 
                customerbillingprofilereference, 
                customerbillingprofileexpires, 
                customerbillingprofilecreatedat, 
                customerbillingprofilelastfailedat
            ) VALUES ( 
                ?now, 
                ?now, 
                ?customerid, 
                ?customerfirstname, 
                ?customerlastname, 
                ?customeremailaddress, 
                ?customerdeleted, 
                ?customersubscriptioncount, 
                ?customernonexpiringsubscriptioncount,
                ?customerentitlements, 
                ?customerfullaccessuntil, 
                ?customerbillingprofileprovider, 
                ?customerbillingprofilereference, 
                ?customerbillingprofileexpires, 
                ?customerbillingprofilecreatedat, 
                ?customerbillingprofilelastfailedat
            ) ON DUPLICATE KEY UPDATE 
                updatedat = ?now, 
                customerfirstname = ?customerfirstname, 
                customerlastname = ?customerlastname, 
                customeremailaddress = ?customeremailaddress, 
                customerdeleted = ?customerdeleted, 
                customersubscriptioncount = ?customersubscriptioncount, 
                customernonexpiringsubscriptioncount = ?customernonexpiringsubscriptioncount,
                customerentitlements = ?customerentitlements, 
                customerfullaccessuntil = ?customerfullaccessuntil, 
                customerbillingprofileprovider = ?customerbillingprofileprovider, 
                customerbillingprofilereference = ?customerbillingprofilereference, 
                customerbillingprofileexpires = ?customerbillingprofileexpires,
                customerbillingprofilecreatedat = ?customerbillingprofilecreatedat,
                customerbillingprofilelastfailedat = ?customerbillingprofilelastfailedat;",
                 new MySqlParameter("now", DateTime.UtcNow),
                 new MySqlParameter("customerid", customer.Id),
                 new MySqlParameter("customerfirstname", customer.FirstName),
                 new MySqlParameter("customerlastname", customer.LastName),
                 new MySqlParameter("customeremailaddress", customer.EmailAddress),
                 new MySqlParameter("customerdeleted", customer.Deleted),
                 new MySqlParameter("customersubscriptioncount", customerSubscriptionCount),
                 new MySqlParameter("customernonexpiringsubscriptioncount", customerNonExpiringSubscriptionCount),
                 new MySqlParameter("customerentitlements", String.Join(",", customerEntitlements.Select(x => x.ToString()).ToArray())),
                 new MySqlParameter("customerfullaccessuntil", customer.FullAccessUntil),
                 new MySqlParameter("customerbillingprofileprovider", customerBillingProfile == null ? null : customerBillingProfile.Provider),
                 new MySqlParameter("customerbillingprofilereference", customerBillingProfile == null ? null : customerBillingProfile.Reference),
                 new MySqlParameter("customerbillingprofileexpires", customerBillingProfile == null ? null : (DateTime?)customerBillingProfile.Expires),
                 new MySqlParameter("customerbillingprofilecreatedat", customerBillingProfile == null ? null : (DateTime?)customerBillingProfile.CreatedAt),
                 new MySqlParameter("customerbillingprofilelastfailedat", null)
             );

            return new RefreshResponse
            {

            };

        }

        #endregion


        #region Get Permission

        public class GetPermissionsRequest
        {
            public Guid CustomerId { get; set; }
        }

        public class GetPermissionsResponse
        {
            public Guid CustomerId { get; set; }
            public CustomerPermissions CustomerPermissions { get; set; }

        }


        public static GetPermissionsResponse GetCustomerPermissions(GetPermissionsRequest request)
        {
            var customerPermissions = LazyMapper.ExecuteObject<CustomerPermissions>(
                "SELECT deleted AS blocked, fullaccessuntil FROM customer WHERE id = ?id",
                new MySqlParameter("id", request.CustomerId)
            );

            if (customerPermissions == null)
                throw new Exception("Customer with the id '" + request.CustomerId + "' not found");

            return new GetPermissionsResponse
            {
                CustomerId = request.CustomerId,
                CustomerPermissions = customerPermissions
            };
        }

        #endregion

        #region Set Permission

        public class SetPermissionsRequest
        {
            public Guid CustomerId { get; set; }
            public CustomerPermissions CustomerPermissions { get; set; }
        }

        public class SetPermissionsResponse
        {
            public Guid CustomerId { get; set; }
            public CustomerPermissions CustomerPermissions { get; set; }
        }

        public static SetPermissionsResponse SetCustomerPermissions(SetPermissionsRequest request)
        {
            var customerPermissions = LazyMapper.ExecuteObject<CustomerPermissions>(
                "UPDATE customer SET deleted = ?blocked, fullaccessuntil = ?fullaccessuntil WHERE id = ?id; SELECT deleted AS blocked, fullaccessuntil FROM customer WHERE id = ?id;",
                new MySqlParameter("id", request.CustomerId),
                new MySqlParameter("blocked", request.CustomerPermissions.Blocked),
                new MySqlParameter("fullaccessuntil", request.CustomerPermissions.FullAccessUntil)
            );

            if (customerPermissions == null)
                throw new Exception("Customer with the id '" + request.CustomerId + "' not found");

            Refresh(new RefreshRequest
            {
                CustomerId = request.CustomerId
            });

            return new SetPermissionsResponse
            {
                CustomerId = request.CustomerId,
                CustomerPermissions = customerPermissions
            };
        }

        #endregion

    }
}