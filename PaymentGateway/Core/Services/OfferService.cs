﻿using MySql.Data.MySqlClient;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class OfferService
    {
        #region List 

        public class GetOfferListResponse
        {
            public class Item
            {
                public Guid Id { get; set; }
                public string Name { get; set; }

            }
            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }
        }

        public class GetOfferListRequest
        {
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
        }

        public static GetOfferListResponse GetOfferList(GetOfferListRequest request)
        {
            var sqlWhere = "1 = 1";

            var sql = new System.Text.StringBuilder();

            sql.AppendLine("SELECT COUNT(*) FROM offer WHERE " + sqlWhere + ";");
            sql.AppendLine("SELECT * FROM offer WHERE " + sqlWhere + " ORDER BY name ASC LIMIT ?pageOffset, ?pageLimit;");


            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize)
           );

            var result = new GetOfferListResponse();
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<GetOfferListResponse.Item>)LazyMapper.Hydrate<GetOfferListResponse.Item>(sourceDataSet.Tables[1]);
            return result;
        }

        #endregion


        #region Set

        public class SetRequest
        {
            public Guid OfferId { get; set; }
            public OfferModel Offer { get; set; }
        }

        public class SetResponse
        {
            public Guid OfferId { get; set; }
            public OfferModel Offer { get; set; }
        }

        public static SetResponse Set(SetRequest request)
        {
            var sql = new StringBuilder();

            var sqlParams = new List<MySqlParameter>();

            if (request.OfferId == Guid.Empty)
            {
                sql.AppendLine("INSERT INTO offer(id, name, appliesfor, discount) VALUES( ?id, ?name, ?appliesfor, ?discount );");
                sqlParams.Add(new MySqlParameter("id", Guid.NewGuid()));
            }
            else
            {
                sql.AppendLine("UPDATE offer SET name = ?name, appliesfor = ?appliesfor, discount = ?discount WHERE id = ?id;");
                sql.AppendLine("DELETE FROM offersubscriptionplan WHERE offerid = ?id;");
                sqlParams.Add(new MySqlParameter("id", request.OfferId));
            }
            for (int i = 0; i < request.Offer.SubscriptionPlanId.Count; i++)
            {
                sql.AppendLine("INSERT INTO offersubscriptionplan (offerid, subscriptionplanid) VALUES( ?id, ?subscriptionplanid_" + i + ");");
                sqlParams.Add(new MySqlParameter("subscriptionplanid_" + i, request.Offer.SubscriptionPlanId[i]));
            }

            sql.AppendLine("SELECT * FROM offer WHERE id = ?id;");
            sql.AppendLine("SELECT subscriptionplanid FROM offersubscriptionplan WHERE offerid = ?id;");


            sqlParams.Add(new MySqlParameter("name", request.Offer.Name));
            sqlParams.Add(new MySqlParameter("appliesfor", request.Offer.AppliesFor));
            sqlParams.Add(new MySqlParameter("discount", request.Offer.Discount));


            var executeResult = MySqlHelper.ExecuteDataset(
                    System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                    sql.ToString(),
                   sqlParams.ToArray()

            );


            if (executeResult.Tables[0].Rows.Count < 1)
                throw new Exception("Unable to create the new offer.");

            var offer = LazyMapper.Hydrate<OfferModel>(executeResult.Tables[0].Rows[0]);
            offer.SubscriptionPlanId = (List<Guid>)LazyMapper.Hydrate<Guid>(executeResult.Tables[1]);

            return new SetResponse
            {
                OfferId = request.OfferId,
                Offer = offer
            };
        }

        #endregion

        #region Get

        public class GetRequest
        {
            public Guid OfferId { get; set; }
        }

        public class GetResponse
        {
            public Guid OfferId { get; set; }
            public OfferModel Offer { get; set; }
        }

        public static GetResponse Get(GetRequest request)
        {

            var executeResult = MySqlHelper.ExecuteDataset(
                  System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                  "SELECT * FROM offer WHERE id = ?id;SELECT subscriptionplanid FROM offersubscriptionplan WHERE offerid = ?id;",
                 new MySqlParameter("id", request.OfferId)

          );

            if (executeResult.Tables[0].Rows.Count < 1)
                throw new Exception("Unable to load the new offer with id '" + request.OfferId + "'.");

            var offer = LazyMapper.Hydrate<OfferModel>(executeResult.Tables[0].Rows[0]);
            offer.SubscriptionPlanId = (List<Guid>)LazyMapper.Hydrate<Guid>(executeResult.Tables[1]);

            return new GetResponse
            {
                OfferId = request.OfferId,
                Offer = offer
            };
        }

        #endregion
    }
}