﻿using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class SubscriptionPlanService
    {
        
        public class GetSubscriptionPlanPriceListResponse
        {
            public class Item
            {
                public Guid Id { get; set; }
                public Guid SubscriptionPlanId { get; set; }                
                public DateTime EffectiveFrom { get; set; }
                public string Currency { get; set; }
                public Decimal Amount { get; set; }
                public string Interval { get; set; }
                public string TrialDuration { get; set; }
                public string Duration { get; set; }

            }
            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }
        }

        public class GetSubscriptionPlanPriceListRequest
        {
            public Guid SubscriptionPlanId { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
        }

        public static GetSubscriptionPlanPriceListResponse GetSubscriptionPlanPriceList(GetSubscriptionPlanPriceListRequest request)
        {
            var sqlWhere = "1 = 1";


            sqlWhere += " AND subscriptionplanid = ?subscriptionplanid";

            var sql = new System.Text.StringBuilder();

            sql.AppendLine("SELECT COUNT(*) FROM subscriptionplanprice WHERE " + sqlWhere + ";");
            sql.AppendLine("SELECT * FROM subscriptionplanprice WHERE " + sqlWhere + " ORDER BY effectivefrom ASC LIMIT ?pageOffset, ?pageLimit;");


            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize),
                new MySqlParameter("subscriptionplanid", request.SubscriptionPlanId)
           );

            var result = new GetSubscriptionPlanPriceListResponse();
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<GetSubscriptionPlanPriceListResponse.Item>)LazyMapper.Hydrate<GetSubscriptionPlanPriceListResponse.Item>(sourceDataSet.Tables[1]);
            return result;
        }

        

        #region List 

        public class GetSubscriptionPlanListResponse
        {
            public class Item
            {
                public Guid Id { get; set; }
                
                public string Name { get; set; }
                public string Entitlements { get; set; }
                public int DisplayOrder { get; set; }
            }
            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }
        }

        public class GetSubscriptionPlanListRequest
        {
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public List<Guid> IdIn { get; set; }
        }

        public static GetSubscriptionPlanListResponse GetSubscriptionPlanList(GetSubscriptionPlanListRequest request)
        {
            var sqlWhere = "1 = 1";


            var sql = new System.Text.StringBuilder();

            if (request.IdIn != null && request.IdIn.Count > 0)
                sqlWhere += " AND subscriptionplan.id IN ('" + request.IdIn.Select(g => g.ToString()).Aggregate((working, next) => working + "','" + next) + "')";

            sql.AppendLine("SELECT COUNT(*) FROM subscriptionplan WHERE " + sqlWhere + ";");
            sql.AppendLine("SELECT * FROM subscriptionplan WHERE " + sqlWhere + " ORDER BY name LIMIT ?pageOffset, ?pageLimit;");


            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize)
           );

            var result = new GetSubscriptionPlanListResponse();
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<GetSubscriptionPlanListResponse.Item>)LazyMapper.Hydrate<GetSubscriptionPlanListResponse.Item>(sourceDataSet.Tables[1]);
            return result;
        }

        #endregion

        #region Get

        public class GetRequest
        {
            public Guid SubscriptionPlanId { get; set; }
        }

        public class GetResponse
        {
            public Guid SubscriptionPlanId { get; set; }
            public SubscriptionPlanModel SubscriptionPlan { get; set; }
        }

        public static GetResponse Get(GetRequest request)
        {
            var subscriptionPlan = LazyMapper.GetObjectById<SubscriptionPlanModel>(request.SubscriptionPlanId);
            if (subscriptionPlan == null)
                throw new Exception("Subscription plan with the id '" + request.SubscriptionPlanId + "' not found.");

            return new GetResponse
            {
                SubscriptionPlanId = request.SubscriptionPlanId,
                SubscriptionPlan = subscriptionPlan
            };
        }


        #endregion


        #region Set

        public class SetRequest
        {
            public Guid SubscriptionPlanId { get; set; }
            public SubscriptionPlanModel SubscriptionPlan { get; set; }
        }

        public class SetResponse
        {
            public Guid SubscriptionPlanId { get; set; }
            public SubscriptionPlanModel SubscriptionPlan { get; set; }
        }

        public static SetResponse Set(SetRequest request)
        {
            var subscriptionPlan = LazyMapper.ExecuteObject<SubscriptionPlanModel>(
                "UPDATE subscriptionplan SET name = ?name, entitlements = ?entitlements, statementdescription = ?statementdescription, listingtitle = ?listingtitle, listingbody = ?listingbody, listingstyle = ?listingstyle, checkouttitle = ?checkouttitle, checkoutbody = ?checkoutbody, checkoutstyle = ?checkoutstyle, displayorder = ?displayorder, listingtitlewithfreetrial = ?listingtitlewithfreetrial, listingbodywithfreetrial = ?listingbodywithfreetrial, listingstylewithfreetrial = ?listingstylewithfreetrial, checkouttitlewithfreetrial = ?checkouttitlewithfreetrial, checkoutbodywithfreetrial = ?checkoutbodywithfreetrial, checkoutstylewithfreetrial = ?checkoutstylewithfreetrial, deleted = ?deleted WHERE id = ?id; SELECT * FROM subscriptionplan WHERE id = ?id;",
                new MySqlParameter("id", request.SubscriptionPlanId),
                new MySqlParameter("displayorder", request.SubscriptionPlan.DisplayOrder),
                new MySqlParameter("checkoutbody", request.SubscriptionPlan.CheckoutBody),
                new MySqlParameter("checkoutbodywithfreetrial", request.SubscriptionPlan.CheckoutBodyWithFreeTrial),
                new MySqlParameter("checkoutstyle", request.SubscriptionPlan.CheckoutStyle),
                new MySqlParameter("checkoutstylewithfreetrial", request.SubscriptionPlan.CheckoutStyleWithFreeTrial),
                new MySqlParameter("checkouttitle", request.SubscriptionPlan.CheckoutTitle),
                new MySqlParameter("checkouttitlewithfreetrial", request.SubscriptionPlan.CheckoutTitleWithFreeTrial),
                new MySqlParameter("deleted", request.SubscriptionPlan.Deleted),
                new MySqlParameter("entitlements", request.SubscriptionPlan.Entitlements),
                new MySqlParameter("statementdescription", request.SubscriptionPlan.StatementDescription),
                new MySqlParameter("listingbody", request.SubscriptionPlan.ListingBody),
                new MySqlParameter("listingbodywithfreetrial", request.SubscriptionPlan.ListingBodyWithFreeTrial),
                new MySqlParameter("listingstyle", request.SubscriptionPlan.ListingStyle),
                new MySqlParameter("listingstylewithfreetrial", request.SubscriptionPlan.ListingStyleWithFreeTrial),
                new MySqlParameter("listingtitle", request.SubscriptionPlan.ListingTitle),
                new MySqlParameter("listingtitlewithfreetrial", request.SubscriptionPlan.ListingTitleWithFreeTrial),
                new MySqlParameter("name", request.SubscriptionPlan.Name)

            );
            if (subscriptionPlan == null)
                throw new Exception("Subscription plan with the id '" + request.SubscriptionPlanId + "' not found.");

            return new SetResponse
            {
                SubscriptionPlanId = request.SubscriptionPlanId,
                SubscriptionPlan = subscriptionPlan
            };
        }



        #endregion

        #region Add

        public class AddRequest
        {
            public SubscriptionPlanModel SubscriptionPlan { get; set; }
        }

        public class AddResponse
        {
            public Guid SubscriptionPlanId { get; set; }
            public SubscriptionPlanModel SubscriptionPlan { get; set; }
        }

        public static AddResponse Add(AddRequest request)
        {
            var subscriptionPlan = LazyMapper.ExecuteObject<SubscriptionPlanModel>(
                "INSERT INTO subscriptionplan ( id, name, entitlements, statementdescription, listingtitle, listingbody, listingstyle, checkouttitle, checkoutbody, checkoutstyle, displayorder, listingtitlewithfreetrial, listingbodywithfreetrial, listingstylewithfreetrial, checkouttitlewithfreetrial, checkoutbodywithfreetrial, checkoutstylewithfreetrial, deleted ) VALUES ( ?id, ?name, ?entitlements, ?statementdescription, ?listingtitle, ?listingbody, ?listingstyle, ?checkouttitle, ?checkoutbody, ?checkoutstyle, ?displayorder, ?listingtitlewithfreetrial, ?listingbodywithfreetrial, ?listingstylewithfreetrial, ?checkouttitlewithfreetrial, ?checkoutbodywithfreetrial, ?checkoutstylewithfreetrial, ?deleted ); SELECT * FROM subscriptionplan WHERE id = ?id;",
                new MySqlParameter("id", Guid.NewGuid()),
                new MySqlParameter("displayorder", request.SubscriptionPlan.DisplayOrder),
                new MySqlParameter("checkoutbody", request.SubscriptionPlan.CheckoutBody),
                new MySqlParameter("checkoutbodywithfreetrial", request.SubscriptionPlan.CheckoutBodyWithFreeTrial),
                new MySqlParameter("checkoutstyle", request.SubscriptionPlan.CheckoutStyle),
                new MySqlParameter("checkoutstylewithfreetrial", request.SubscriptionPlan.CheckoutStyleWithFreeTrial),
                new MySqlParameter("checkouttitle", request.SubscriptionPlan.CheckoutTitle),
                new MySqlParameter("checkouttitlewithfreetrial", request.SubscriptionPlan.CheckoutTitleWithFreeTrial),
                new MySqlParameter("deleted", request.SubscriptionPlan.Deleted),
                new MySqlParameter("entitlements", request.SubscriptionPlan.Entitlements),
                new MySqlParameter("statementdescription", request.SubscriptionPlan.StatementDescription),
                new MySqlParameter("listingbody", request.SubscriptionPlan.ListingBody),
                new MySqlParameter("listingbodywithfreetrial", request.SubscriptionPlan.ListingBodyWithFreeTrial),
                new MySqlParameter("listingstyle", request.SubscriptionPlan.ListingStyle),
                new MySqlParameter("listingstylewithfreetrial", request.SubscriptionPlan.ListingStyleWithFreeTrial),
                new MySqlParameter("listingtitle", request.SubscriptionPlan.ListingTitle),
                new MySqlParameter("listingtitlewithfreetrial", request.SubscriptionPlan.ListingTitleWithFreeTrial),
                new MySqlParameter("name", request.SubscriptionPlan.Name)

            );
            if (subscriptionPlan == null)
                throw new Exception("Failed to add new subscription plan.");

            return new AddResponse
            {
                SubscriptionPlan = subscriptionPlan
            };
        }



        #endregion


        #region GetTransaltion

        public class GetTranslationRequest
        {
            public Guid SubscriptionPlanId { get; set; }
            public string Language { get; set; }
        }

        public class GetTranslationResponse
        {
            public Guid SubscriptionPlanId { get; set; }
            public string Language { get; set; }
            public SubscriptionPlanTranslationModel SubscriptionPlanTranslation { get; set; }
        }

        public static GetTranslationResponse GetTranslation(GetTranslationRequest request)
        {
            var subscriptionPlanTranslation = LazyMapper.ExecuteObject<SubscriptionPlanTranslationModel>(
                "SELECT * FROM subscriptionplantranslation WHERE id = ?id AND language = ?language",
                new MySqlParameter("id", request.SubscriptionPlanId),
                new MySqlParameter("language", request.Language)
            );

            if (subscriptionPlanTranslation == null)
                subscriptionPlanTranslation = new SubscriptionPlanTranslationModel();

            return new GetTranslationResponse
            {
                SubscriptionPlanId = request.SubscriptionPlanId,
                Language = request.Language,
                SubscriptionPlanTranslation = subscriptionPlanTranslation
            };
        }

        #endregion

        #region SetTransaltion

        public class SetTranslationRequest
        {
            public Guid SubscriptionPlanId { get; set; }
            public string Language { get; set; }
            public SubscriptionPlanTranslationModel SubscriptionPlanTranslation { get; set; }
        }

        public class SetTranslationResponse
        {
            public Guid SubscriptionPlanId { get; set; }
            public string Language { get; set; }
            public SubscriptionPlanTranslationModel SubscriptionPlanTranslation { get; set; }
        }

        public static SetTranslationResponse SetTranslation(SetTranslationRequest request)
        {
            var subscriptionPlanTranslation = LazyMapper.ExecuteObject<SubscriptionPlanTranslationModel>(
                 "INSERT INTO subscriptionplantranslation ( id, language, name, statementdescription, listingtitle, listingbody, checkouttitle, checkoutbody, listingtitlewithfreetrial, listingbodywithfreetrial, checkouttitlewithfreetrial, checkoutbodywithfreetrial ) VALUES ( ?id, ?language, ?name, ?statementdescription, ?listingtitle, ?listingbody, ?checkouttitle, ?checkoutbody, ?listingtitlewithfreetrial, ?listingbodywithfreetrial, ?checkouttitlewithfreetrial, ?checkoutbodywithfreetrial ) ON DUPLICATE KEY UPDATE name = ?name, listingtitle = ?listingtitle, listingbody = ?listingbody, checkouttitle = ?checkouttitle, checkoutbody = ?checkoutbody, listingtitlewithfreetrial = ?listingtitlewithfreetrial, listingbodywithfreetrial = ?listingbodywithfreetrial, checkouttitlewithfreetrial = ?checkouttitlewithfreetrial, checkoutbodywithfreetrial = ?checkoutbodywithfreetrial, statementdescription = ?statementdescription; SELECT * FROM subscriptionplantranslation WHERE id = ?id;",
                new MySqlParameter("id", request.SubscriptionPlanId),
                new MySqlParameter("language", request.Language),
                new MySqlParameter("checkoutbody", request.SubscriptionPlanTranslation.CheckoutBody),
                new MySqlParameter("checkoutbodywithfreetrial", request.SubscriptionPlanTranslation.CheckoutBodyWithFreeTrial),
                new MySqlParameter("checkoutstylewithfreetrial", request.SubscriptionPlanTranslation.CheckoutStyleWithFreeTrial),
                new MySqlParameter("checkouttitle", request.SubscriptionPlanTranslation.CheckoutTitle),
                new MySqlParameter("checkouttitlewithfreetrial", request.SubscriptionPlanTranslation.CheckoutTitleWithFreeTrial),
                new MySqlParameter("listingbody", request.SubscriptionPlanTranslation.ListingBody),
                new MySqlParameter("listingbodywithfreetrial", request.SubscriptionPlanTranslation.ListingBodyWithFreeTrial),
                new MySqlParameter("listingtitle", request.SubscriptionPlanTranslation.ListingTitle),
                new MySqlParameter("listingtitlewithfreetrial", request.SubscriptionPlanTranslation.ListingTitleWithFreeTrial),
                new MySqlParameter("name", request.SubscriptionPlanTranslation.Name),
                new MySqlParameter("statementdescription", request.SubscriptionPlanTranslation.StatementDescription)
            );

            if (subscriptionPlanTranslation == null)
                subscriptionPlanTranslation = new SubscriptionPlanTranslationModel();

            return new SetTranslationResponse
            {
                SubscriptionPlanId = request.SubscriptionPlanId,
                Language = request.Language,
                SubscriptionPlanTranslation = subscriptionPlanTranslation
            };
        }

        #endregion

        #region GetSubscriptionPlanStatistics

        public class SubscriptionPlanStatistics
        {
            public int ActiveCount { get; set; }
            public int OverdueCount { get; set; }
        }

        public class GetSubscriptionPlanStatisticsRequest
        {
            public Guid SubscriptionPlanId { get; set; }
        }

        public class GetSubscriptionPlanStatisticsResponse
        {
            public Guid SubscriptionPlanId { get; set; }
            public SubscriptionPlanStatistics SubscriptionPlanStatistics { get; set; }
        }

        public static GetSubscriptionPlanStatisticsResponse GetSubscriptionPlanStatistics(GetSubscriptionPlanStatisticsRequest request)
        {
            var subscriptionPlanStatistics = LazyMapper.ExecuteObject<SubscriptionPlanStatistics>(
                "SELECT (SELECT count(*) FROM customersubscription WHERE status = 1 AND (expirydate IS NULL OR expirydate > ?now) AND subscriptionplanid = ?subscriptionplanid) AS activecount, (SELECT count(*) FROM customersubscription WHERE status = 1 AND (expirydate IS NULL OR expirydate > ?now) AND subscriptionplanid = ?subscriptionplanid AND renewaldate < ?now) AS overduecount",
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("subscriptionplanid", request.SubscriptionPlanId)
            );

            return new GetSubscriptionPlanStatisticsResponse
            {
                SubscriptionPlanId = request.SubscriptionPlanId,
                SubscriptionPlanStatistics = subscriptionPlanStatistics
            };

        }




        #endregion


        public class GetSubscriptionPlanUpgradeOptionsRequest
        {
            public Guid SubscriptionPlanId { get; set; }

        }

        public class GetSubscriptionPlanUpgradeOptionsResponse
        {
            public Guid SubscriptionPlanId { get; set; }
            public List<Guid> TargetSubscriptionPlanIds { get; set; }
        }

        public static GetSubscriptionPlanUpgradeOptionsResponse GetSubscriptionPlanUpgradeOptions(GetSubscriptionPlanUpgradeOptionsRequest request)
        {

            var result = new GetSubscriptionPlanUpgradeOptionsResponse();
            result.SubscriptionPlanId = request.SubscriptionPlanId;

            var sourceDataSet = MySqlHelper.ExecuteDataset(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "SELECT id FROM subscriptionplan INNER JOIN subscriptionplanupgrade ON subscriptionplan.id = subscriptionplanupgrade.targetsubscriptionplanid WHERE subscriptionplanupgrade.subscriptionplanid = ?subscriptionplanid AND subscriptionplan.deleted = 0",
                new MySqlParameter("subscriptionplanid", request.SubscriptionPlanId)
            );

            result.TargetSubscriptionPlanIds = new List<Guid>();
            foreach (DataRow dr in sourceDataSet.Tables[0].Rows)
                result.TargetSubscriptionPlanIds.Add(new Guid(dr[0].ToString()));

            return result;
        }



        #region Set

        public class SetPriceRequest
        {
            public Guid SubscriptionPlanPriceId { get; set; }
            public SubscriptionPlanPriceModel SubscriptionPlanPrice { get; set; }
        }

        public class SetPriceResponse
        {
            public Guid SubscriptionPlanPriceId { get; set; }
            public SubscriptionPlanPriceModel SubscriptionPlanPrice { get; set; }
        }

        public static SetPriceResponse SetPrice(SetPriceRequest request)
        {
            var subscriptionPlan = LazyMapper.GetObjectById<SubscriptionPlanModel>(request.SubscriptionPlanPrice.SubscriptionPlanId);

            if (subscriptionPlan == null)
                throw new Exception("Subscription Plan with the id '" + request.SubscriptionPlanPrice.SubscriptionPlanId + "' does not exist");

            if(request.SubscriptionPlanPrice.EffectiveFrom < DateTime.UtcNow.AddDays(7))
                throw new ArgumentOutOfRangeException("You can only create a subscription plan that is more than 7 days in the future.");

            var sql = new StringBuilder();

            var sqlParams = new List<MySqlParameter>();

            if (request.SubscriptionPlanPriceId == Guid.Empty)
            {
                sql.AppendLine("INSERT INTO subscriptionplanprice ( id, subscriptionplanid, effectivefrom, currency, amount, `interval`, trialduration, duration ) VALUES ( ?id, ?subscriptionplanid, ?effectivefrom, ?currency, ?amount, ?interval, ?trialduration, ?duration );");
                sqlParams.Add(new MySqlParameter("id", Guid.NewGuid()));
            }
            else
            {
                throw new NotImplementedException("You cannot update an exisitng Subscription Plan Price.");                
            }
            
            sql.AppendLine("SELECT * FROM subscriptionplanprice WHERE id = ?id;");

            sqlParams.Add(new MySqlParameter("utcnow", DateTime.UtcNow));
            sqlParams.Add(new MySqlParameter("amount", request.SubscriptionPlanPrice.Amount));
            sqlParams.Add(new MySqlParameter("currency", request.SubscriptionPlanPrice.Currency));
            sqlParams.Add(new MySqlParameter("duration", request.SubscriptionPlanPrice.Duration));
            sqlParams.Add(new MySqlParameter("effectivefrom", request.SubscriptionPlanPrice.EffectiveFrom));
            sqlParams.Add(new MySqlParameter("interval", request.SubscriptionPlanPrice.Interval));
            sqlParams.Add(new MySqlParameter("subscriptionplanid", request.SubscriptionPlanPrice.SubscriptionPlanId));
            sqlParams.Add(new MySqlParameter("trialduration", request.SubscriptionPlanPrice.TrialDuration));
                        
            var subscriptionPrice = LazyMapper.ExecuteObject<SubscriptionPlanPriceModel>(sql.ToString(),sqlParams.ToArray());

            if (subscriptionPrice == null)
                throw new Exception("Unable to create the new Subscription Plan Price.");

            return new SetPriceResponse
            {
                SubscriptionPlanPriceId = request.SubscriptionPlanPriceId,
                SubscriptionPlanPrice = subscriptionPrice
            };
        }

        #endregion

    }
}