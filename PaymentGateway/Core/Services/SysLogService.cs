﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using MySql.Data.MySqlClient;
using PaymentsApi.Services;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class SysLogService
    {

        public class LogEventRequest
        {
            public string Group { get; set; }
            public string Name { get; set; }
            public string Title { get; set; }
            public object Detail { get; set; }

            public Dictionary<string, Guid> Associations { get; set; }

            public LogEventRequest()
            {
                Associations = new Dictionary<string, Guid>();
            }

        }
        public static void LogEvent(LogEventRequest request)
        {
            try
            {
                var getLocationResponse = GeoLocationService.GetLocation();
                var getDeviceInfoResponse = DeviceDetectionService.GetDeviceInfo();

                LazyMapper.ExecuteObject<SysLogModel>(
                    @"INSERT INTO syslog (id, createdat, group, name, detail, title, ipaddress, ipaddresslocation, useragent, useragentos, useragentosversion, useragentbrowser, useragentbrowserversion, useragentdevicetype) VALUES (?id,?createdat,?group,?name,?detail,?title,?ipaddress,?ipaddresslocation,?useragent,?useragentos,?useragentosversion,?useragentbrowser,?useragentbrowserversion,?useragentdevicetype);SELECT * FROM syslog WHERE id = ?id;",
                    new MySqlParameter("id", Guid.NewGuid()),
                    new MySqlParameter("createdat", DateTime.UtcNow),
                    new MySqlParameter("group", request.Group),
                    new MySqlParameter("name", request.Name),
                    new MySqlParameter("title", request.Title),
                    new MySqlParameter("detail", request.Detail),
                    new MySqlParameter("ipaddress", getLocationResponse.IpAddress),
                    new MySqlParameter("ipaddresslocation", LazyMapper.TrimToMaxLen(getLocationResponse.Name, 500)),
                    new MySqlParameter("useragent", LazyMapper.TrimToMaxLen(getDeviceInfoResponse.UserAgent, 1000)),
                    new MySqlParameter("useragentos", LazyMapper.TrimToMaxLen(getDeviceInfoResponse.OperatingSystem, 100)),
                    new MySqlParameter("useragentosversion", LazyMapper.TrimToMaxLen(getDeviceInfoResponse.OperatingSystemVersion, 100)),
                    new MySqlParameter("useragentbrowser", LazyMapper.TrimToMaxLen(getDeviceInfoResponse.Browser, 100)),
                    new MySqlParameter("useragentbrowserversion", LazyMapper.TrimToMaxLen(getDeviceInfoResponse.BrowserVersion, 100)),
                    new MySqlParameter("useragentdevicetype", LazyMapper.TrimToMaxLen(getDeviceInfoResponse.DeviceType, 50))
                );
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Warn(ex, "Unable to save SysLog for '" + request.Name + "', '" + request.Group + "'.");
            }
        }

        public class SysLogSearchResult
        {
            public class Item
            {
                public Guid Id { get; set; }

                public string Title { get; set; }
                public DateTime CreatedAt { get; set; }
                public string IpAddress { get; set; }
                public string IpAddressLocation { get; set; }

                public string UserAgent { get; set; }
                public string UserAgentOs { get; set; }
                public string UserAgentOsVersion { get; set; }
                public string UserAgentBrowser { get; set; }
                public string UserAgentBrowserVersion { get; set; }
                public string UserAgentDeviceType { get; set; }
            }

            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }
        }

        public class SysLogSearchRequest
        {
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public string ObjectType { get; set; }
            public Guid ObjectId { get; set; }
        }


        public static SysLogSearchResult SysLogSearch(SysLogSearchRequest request)
        {

            var sqlWhere = "1 = 1";

            if (!string.IsNullOrWhiteSpace(request.ObjectType))
                sqlWhere += " AND sysloglink.objecttype = ?objecttype";

            if (request.ObjectId != Guid.Empty)
                sqlWhere += " AND sysloglink.objectid = ?objectid";

            var sql = new StringBuilder();
            sql.AppendLine("SELECT COUNT(syslog.id) FROM syslog INNER JOIN sysloglink ON sysloglink.syslogid = syslog.id WHERE " + sqlWhere + " ORDER BY syslog.createdat DESC;");
            sql.AppendLine("SELECT * FROM syslog INNER JOIN sysloglink ON sysloglink.syslogid = syslog.id WHERE " + sqlWhere + " ORDER BY syslog.createdat DESC LIMIT ?pageOffset, ?pageLimit;");

            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize),
                new MySqlParameter("objecttype", request.ObjectType),
                new MySqlParameter("objectid", request.ObjectId)
           );

            var result = new SysLogSearchResult();
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<SysLogSearchResult.Item>)LazyMapper.Hydrate<SysLogSearchResult.Item>(sourceDataSet.Tables[1]);
            return result;
        }
    }
}
