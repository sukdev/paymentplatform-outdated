﻿using StreamAMG.OTTPlatform.Core.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class CustomerSearchService
    {


        public class SearchResult
        {
            public class Item
            {
                public Guid Id { get; set; }
                public DateTime CreatedAt { get; set; }
                public DateTime UpdatedAt { get; set; }
                public string FirstName { get; set; }
                public string LastName { get; set; }
                public string EmailAddress { get; set; }
                public string Language { get; set; }
                public int ActiveSubscriptionCount { get; set; }
                public int DueSubscriptionCount { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public bool LastTransactionSuccessful { get; set; }
                public string LastTransactionProviderMessage { get; set; }
                public bool Deleted { get; set; }
            }
            public List<Item> Items { get; set; }
            public string Keywords { get; set; }
            public int? ActiveSubscriptionCountFrom { get; set; }
            public int? ActiveSubscriptionCountTo { get; set; }
            public int? DueSubscriptionCountFrom { get; set; }
            public int? DueSubscriptionCountTo { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }
        }

        public class SearchRequest
        {
            public string Keywords { get; set; }
            public int? ActiveSubscriptionCountFrom { get; set; }
            public int? ActiveSubscriptionCountTo { get; set; }
            public int? DueSubscriptionCountFrom { get; set; }
            public int? DueSubscriptionCountTo { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public Guid? ActiveSubscriptionPlanId { get; set; }
        }

        public static SearchResult Search(SearchRequest request)
        {

            var keywordsTrimmed = request.Keywords == null? "" : request.Keywords.Trim();
            var sqlWhere = "1 = 1";

            if (request.ActiveSubscriptionCountFrom.HasValue)
                sqlWhere += " AND activesubscriptioncount >= " + request.ActiveSubscriptionCountFrom;

            if (request.ActiveSubscriptionCountTo.HasValue)
                sqlWhere += " AND activesubscriptioncount <= " + request.ActiveSubscriptionCountTo;


            if (request.DueSubscriptionCountFrom.HasValue)
                sqlWhere += " AND duesubscriptioncount >= " + request.DueSubscriptionCountFrom;

            if (request.DueSubscriptionCountTo.HasValue)
                sqlWhere += " AND duesubscriptioncount <= " + request.DueSubscriptionCountTo;
            

            var sql = new StringBuilder();

            sql.AppendLine("DROP TABLE IF EXISTS serachresultcustomer;");
            sql.AppendLine("CREATE TEMPORARY TABLE IF NOT EXISTS serachresultcustomer(");
            sql.AppendLine("    id char(36) NOT NULL,");
            sql.AppendLine("    createdat datetime NOT NULL,");
            sql.AppendLine("    updatedat datetime NOT NULL,");
            sql.AppendLine("    emailaddress varchar(100) DEFAULT NULL,");
            sql.AppendLine("    language varchar(5) DEFAULT NULL,");
            sql.AppendLine("    deleted bit(1) NOT NULL DEFAULT b'0',");
            sql.AppendLine("    firstname varchar(50) DEFAULT NULL,");
            sql.AppendLine("    lastname varchar(50) DEFAULT NULL,");
            sql.AppendLine("    fullaccessuntil datetime DEFAULT NULL,");
            sql.AppendLine("    activesubscriptioncount int(11) NOT NULL DEFAULT '0',");
            sql.AppendLine("    duesubscriptioncount int(11) NOT NULL DEFAULT '0',");
            sql.AppendLine("    lasttransactionprovidermessage VARCHAR(1000),");
            sql.AppendLine("    lasttransactiondate datetime,");
            sql.AppendLine("    lasttransactionsuccesful BIT,");
            sql.AppendLine("    PRIMARY KEY(id)");
            sql.AppendLine(");");

            

            if (!string.IsNullOrWhiteSpace(keywordsTrimmed))
            {
                sql.AppendLine("INSERT INTO serachresultcustomer(id, createdat, updatedat, emailaddress, deleted, firstname, lastname, fullaccessuntil, language)");
                sql.AppendLine("SELECT id, createdat, updatedat, emailaddress, deleted, firstname, lastname, fullaccessuntil, language FROM customer WHERE 1 = 1 AND (emailaddress LIKE '%" + keywordsTrimmed.Replace("'", "''") + "%' OR CONCAT(customer.firstname,' ',customer.lastname) LIKE '%" + keywordsTrimmed.Replace("'", "''") + "%');");

                if (Regex.IsMatch(keywordsTrimmed, "^[a-zA-Z]{8}$"))
                {
                    sql.AppendLine("INSERT IGNORE INTO serachresultcustomer(id, createdat, updatedat, emailaddress, deleted, firstname, lastname, fullaccessuntil, language)");
                    sql.AppendLine("SELECT customer.id, customer.createdat, customer.updatedat, customer.emailaddress, customer.deleted, customer.firstname, customer.lastname, customer.fullaccessuntil, customer.language FROM customer INNER JOIN customersubscription ON customer.id = customersubscription.customerid INNER JOIN offervoucher ON customersubscription.id = offervoucher.customersubscriptionplanid WHERE offervoucher.code = '" + keywordsTrimmed.Replace("'", "''") + "';");
                }


                if (Regex.IsMatch(keywordsTrimmed, "^[0-9]{4}$"))
                {
                    sql.AppendLine("INSERT IGNORE INTO serachresultcustomer(id, createdat, updatedat, emailaddress, deleted, firstname, lastname, fullaccessuntil, language)");
                    sql.AppendLine("SELECT customer.id, customer.createdat, customer.updatedat, customer.emailaddress, customer.deleted, customer.firstname, customer.lastname, customer.fullaccessuntil, customer.language FROM customer INNER JOIN customerbillingprofile ON customer.id = customerbillingprofile.customerid WHERE customerbillingprofile.reference LIKE '%" + keywordsTrimmed.Replace("'", "''") + "';");
                }
            }            
            else
            {
                sql.AppendLine("INSERT IGNORE INTO serachresultcustomer(id, createdat, updatedat, emailaddress, deleted, firstname, lastname, fullaccessuntil, language)");
                sql.AppendLine("SELECT id, createdat, updatedat, emailaddress, deleted, firstname, lastname, fullaccessuntil, language FROM customer;");
            }


            sql.AppendLine("UPDATE serachresultcustomer SET activesubscriptioncount = (SELECT count(*) FROM customersubscription WHERE status = 1 AND(expirydate IS NULL OR expirydate >= NOW()) AND customerid = serachresultcustomer.id);");
            sql.AppendLine("UPDATE serachresultcustomer SET duesubscriptioncount = (SELECT count(*) FROM customersubscription WHERE status = 1 AND(expirydate IS NULL OR expirydate > renewaldate) AND renewaldate <= NOW() AND customerid = serachresultcustomer.id);");
            sql.AppendLine("UPDATE serachresultcustomer SET lasttransactionprovidermessage = (SELECT providermessage FROM customertransaction WHERE status <> 0 AND customerid = serachresultcustomer.id ORDER BY createdat DESC LIMIT 1);");
            sql.AppendLine("UPDATE serachresultcustomer SET lasttransactiondate = (SELECT updatedat FROM customertransaction WHERE status <> 0 AND customerid = serachresultcustomer.id ORDER BY createdat DESC LIMIT 1);");
            sql.AppendLine("UPDATE serachresultcustomer SET lasttransactionsuccesful = (SELECT CASE status WHEN 1 THEN 1 ELSE 0 END AS status FROM customertransaction WHERE status <> 0 AND customerid = serachresultcustomer.id ORDER BY createdat DESC LIMIT 1);");

            if (request.ActiveSubscriptionPlanId.HasValue)
                sql.AppendLine("DELETE FROM serachresultcustomer WHERE (SELECT count(*) FROM customersubscription WHERE status = 1 AND(expirydate IS NULL OR expirydate >= NOW()) AND customerid = serachresultcustomer.id AND subscriptionplanid = '" + request.ActiveSubscriptionPlanId.Value.ToString() + "') < 1;");
            

            sql.AppendLine("SELECT COUNT(*) FROM serachresultcustomer WHERE " + sqlWhere+ ";");
            sql.AppendLine("SELECT * FROM serachresultcustomer WHERE " + sqlWhere + " ORDER BY firstname, lastname LIMIT ?pageOffset, ?pageLimit;");
            sql.AppendLine("DROP TABLE IF EXISTS serachresultcustomer;");


            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize)
           );

            var result = new SearchResult();
            result.Keywords = request.Keywords;
            result.ActiveSubscriptionCountFrom = request.ActiveSubscriptionCountFrom;
            result.ActiveSubscriptionCountTo = request.ActiveSubscriptionCountTo;
            result.DueSubscriptionCountFrom = request.DueSubscriptionCountFrom;
            result.DueSubscriptionCountTo = request.DueSubscriptionCountTo;
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<SearchResult.Item>)LazyMapper.Hydrate<SearchResult.Item>(sourceDataSet.Tables[1]);
            return result;
        }

    }
}