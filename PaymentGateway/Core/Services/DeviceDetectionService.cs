﻿using System;
using System.Collections.Generic;
using System.Web;
using Newtonsoft.Json;

namespace PaymentsApi.Services
{
    public class DeviceDetectionService
    {

        public class DeviceInfo
        {
            public string UserAgent { get; set; }
            public string OperatingSystem { get; set; }
            public string OperatingSystemVersion { get; set; }
            public string Browser { get; set; }
            public string BrowserVersion { get; set; }
            public string DeviceType { get; set; }
        }

        public static DeviceInfo GetDeviceInfo()
        {

            var result = new DeviceInfo
            {
                UserAgent = HttpContext.Current.Request.UserAgent
            };

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    var json = webClient.DownloadString(String.Format(
                        "https://cloud.51degrees.com/api/v1/{0}/match?user-agent={1}",
                        "FBTAAANZJAAASA2ADJAGAWSMMJDYT3CX86B9GMZE578PAC7JSJWM4EMUZWKYXUW25ZJJLPR5WYJPYSU5WVPEDCA",
                        HttpUtility.UrlEncode(result.UserAgent)));

                    dynamic match = Newtonsoft.Json.Linq.JObject.Parse(json);
                    var values = JsonConvert.DeserializeObject<SortedList<string, string[]>>(match.Values.ToString());
                    string[] hvValues;

                    if (values.TryGetValue("BrowserName", out hvValues))
                        result.Browser = hvValues[0];

                    if (values.TryGetValue("BrowserVersion", out hvValues))
                        result.BrowserVersion = hvValues[0];

                    if (values.TryGetValue("PlatformVendor", out hvValues))
                        result.OperatingSystem = hvValues[0];

                    if (values.TryGetValue("PlatformName", out hvValues))
                    {
                        if(!string.IsNullOrWhiteSpace(result.OperatingSystem))
                            result.OperatingSystem = result.OperatingSystem + " " + hvValues[0];
                        else                        
                            result.OperatingSystem = hvValues[0];
                    }

                    if (values.TryGetValue("PlatformVersion", out hvValues))
                        result.OperatingSystemVersion = hvValues[0];

                    if (values.TryGetValue("DeviceType", out hvValues))
                        result.DeviceType = hvValues[0];

                }
            }
            catch
            {
                
            }

            return result;
        }
    }
}