﻿using MaxMind.GeoIP2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymentsApi.Services
{
    public static class GeoLocationService
    {
        static DatabaseReader reader;

        static GeoLocationService()
        {
            try
            {
                if (System.Web.HttpContext.Current != null)
                    reader = new DatabaseReader(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/GeoIP2-City.mmdb"), global::MaxMind.Db.FileAccessMode.Memory);
                else
                    reader = new DatabaseReader(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\App_Data\\GeoIP2-City.mmdb", global::MaxMind.Db.FileAccessMode.Memory);
            }
            catch (Exception ex)
            {
                if (System.Web.HttpContext.Current != null)
                    System.Web.HttpContext.Current.Response.Headers.Add("GeoIp-Exception", ex.Message);
            }
        }


        private static string GetIPAddress()
        {
            var context = System.Web.HttpContext.Current;
            string ipList = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0].Trim();
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }


        public class GetLocationResponse
        {
            public string Name { get; set; }
            public string Country { get; set; }
            public string CountryCode { get; set; }
            public string State { get; set; }
            public string City { get; set; }
            public string IpAddress { get; set; }
        }

        public class GetLocationRequest
        {
            public string IpAddress { get; set; }
        }

        public static GetLocationResponse GetLocation()
        {
            return GetLocation(new GetLocationRequest { IpAddress = GetIPAddress() });

        }
        public static GetLocationResponse GetLocation(GetLocationRequest request)
        {
            var result = new GetLocationResponse();

            result.IpAddress = request.IpAddress;
            result.Name = "";

            try
            {
                var cityResponse = reader.City(request.IpAddress);
                if (cityResponse != null)
                {
                    if (cityResponse.Country != null && cityResponse.Country.IsoCode != null)
                        result.CountryCode = cityResponse.Country.IsoCode.ToUpper();

                    if (cityResponse.Country != null)
                    {
                        result.Name = cityResponse.Country.Name + (result.Name == "" ? "" : ", ") + result.Name;
                        result.Country = cityResponse.Country.Name;
                    }

                    if (cityResponse.Subdivisions != null && cityResponse.Subdivisions.Count > 0)
                    {
                        result.Name = cityResponse.Subdivisions[0].Name + (result.Name == "" ? "" : ", ") + result.Name;
                        result.State = cityResponse.Subdivisions[0].Name + (string.IsNullOrWhiteSpace(result.Name) ? "" : ", ") + result.Name;

                    }

                    if (cityResponse.City != null && cityResponse.City.Name != null)
                    {
                        result.Name = cityResponse.City.Name + (result.Name == "" ? "" : ", ") + result.Name;
                        result.City = cityResponse.City.Name + (string.IsNullOrWhiteSpace(result.Name) ? "" : ", ") + result.Name;
                    }
                    if (System.Web.HttpContext.Current != null)
                        System.Web.HttpContext.Current.Response.Headers.Add("GeoIp-Location", result.Name);
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                if (System.Web.HttpContext.Current != null)
                    System.Web.HttpContext.Current.Response.Headers.Add("GeoIp-Exception", ex.Message);
            }
            return result;
        }
    }
}