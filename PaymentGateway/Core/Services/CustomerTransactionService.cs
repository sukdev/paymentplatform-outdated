﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;
using StreamAMG.OTTPlatform.Utility;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using PayPal.PayPalAPIInterfaceService;
using PayPal.PayPalAPIInterfaceService.Model;
using Stripe;
using System;
using System.Collections.Generic;
using System.Text;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class CustomerTransactionService
    {


        public class TransationSearchResult
        {
          

            public class Item
            {
                public Guid Id { get; set; }
                public DateTime CreatedAt { get; set; }
                public DateTime UpdatedAt { get; set; }
                public DateTime? SubmittedAt { get; set; }
                public DateTime? WarnAt { get; set; }
                public Guid CustomerId { get; set; }
                public string CustomerEmailAddress { get; set; }
                public string CustomerFirstName { get; set; }
                public string CustomerLastName { get; set; }

                public string SubscriptionPlanName { get; set; }

                public int Status { get; set; }
                public Decimal Amount { get; set; }
                public Decimal AmountRefunded { get; set; }
                public string Currency { get; set; }
                public string Reference { get; set; }
                public string Provider { get; set; }
                public string ProviderId { get; set; }
                public string ProviderMessage { get; set; }
                public string Description { get; set; }
            }

            public List<Item> Items { get; set; }            
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }

            public TransationSearchRequest Request { get; set; }
        }

        public class TransationSearchRequest
        {
            public enum TransactionStatuses
            {                
                Processing = -2,
                Warning = -3,
                Failure = -1,
                Success = 1,
                Prepared = 0
            }

            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public TransactionStatuses? TransactionStatus { get; set;}
            public Guid CustomerId { get; set; }
            public Guid SubscriptionPlanId { get; set; }
            public DateTime? DateFrom { get; set; }
            public DateTime? DateTo { get; set; }
        }


        public static TransationSearchResult TransactionSearch(TransationSearchRequest request)
        {

            var sqlWhere = "1 = 1";            

            if (request.TransactionStatus.HasValue)
            {
                switch (request.TransactionStatus)
                {
                    case TransationSearchRequest.TransactionStatuses.Failure:
                        sqlWhere += " AND status = -1 AND submittedat IS NOT NULL";
                        break;

                    case TransationSearchRequest.TransactionStatuses.Success:
                        sqlWhere += " AND status = 1 AND submittedat IS NOT NULL";
                        break;

                    case TransationSearchRequest.TransactionStatuses.Warning:
                        sqlWhere += " AND status = 0 AND submittedat IS NOT NULL AND warnat <= ?now";
                        break;

                    case TransationSearchRequest.TransactionStatuses.Processing:
                        sqlWhere += " AND status = 0 AND submittedat IS NOT NULL AND warnat > ?now";
                        break;

                    case TransationSearchRequest.TransactionStatuses.Prepared:
                        sqlWhere += " AND submittedat IS NULL";
                        break;
                }

            }

            if (request.CustomerId != Guid.Empty)
                sqlWhere += " AND customerid = ?customerid";

            if (request.SubscriptionPlanId != Guid.Empty)
                sqlWhere += " AND customertransaction.subscriptionplanid = ?subscriptionplanid";

            if (request.DateFrom.HasValue)
                sqlWhere += " AND customertransaction.createdat >= ?datefrom";

            if (request.DateTo.HasValue)
                sqlWhere += " AND customertransaction.createdat <= ?dateto";

            var sql = new StringBuilder();            
            sql.AppendLine("SELECT COUNT(customertransaction.id) FROM customertransaction INNER JOIN customer ON customer.id = customertransaction.customerid INNER JOIN subscriptionplan ON subscriptionplan.id = customertransaction.subscriptionplanid WHERE " + sqlWhere + " ORDER BY customertransaction.updatedat DESC;");
            sql.AppendLine("SELECT customertransaction.id, customertransaction.createdat, customertransaction.updatedat, customertransaction.submittedat, customertransaction.warnat, customer.id AS customerid, customer.emailaddress AS customeremailaddress, customer.firstname AS customerfirstname, customer.lastname AS customerlastname, customertransaction.status, customertransaction.amount, customertransaction.currency, customertransaction.reference, customertransaction.provider, customertransaction.providerid, customertransaction.providermessage, customertransaction.description, subscriptionplan.name AS subscriptionplanname FROM customertransaction INNER JOIN customer ON customer.id = customertransaction.customerid  INNER JOIN subscriptionplan ON subscriptionplan.id = customertransaction.subscriptionplanid WHERE " + sqlWhere + " ORDER BY customertransaction.createdat DESC LIMIT ?pageOffset, ?pageLimit;");

            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize),
                new MySqlParameter("customerid", request.CustomerId),
                new MySqlParameter("subscriptionplanid", request.SubscriptionPlanId),
                new MySqlParameter("datefrom", request.DateFrom),
                new MySqlParameter("dateto", request.DateTo)
           );

            var result = new TransationSearchResult();
            result.Request = request;
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<TransationSearchResult.Item>)LazyMapper.Hydrate<TransationSearchResult.Item>(sourceDataSet.Tables[1]);
            return result;
        }

        

        public class PrepareTransactionRequest
        {
            public Guid CustomerSubscriptionId { get; set; }
            public bool Batch { get; set; }
        }

        public class PrepareTransactionResult
        {
            public CustomerTransactionModel Transaction { get; set; }
        }

        public static PrepareTransactionResult PrepareTransaction(PrepareTransactionRequest request)
        {

            var customerSubscription = LazyMapper.GetObjectById<CustomerSubscriptionModel>(request.CustomerSubscriptionId);

            if (customerSubscription == null)
                throw new Exception("CustomerSubscription with id '" + request.CustomerSubscriptionId + "' could not be found");

            if (customerSubscription.ExpiryDate.HasValue && customerSubscription.ExpiryDate < DateTime.UtcNow)
                throw new Exception("The subscription has expired and is not due to be renewed.");

            if (customerSubscription.RenewalDate > DateTime.UtcNow)
                throw new Exception("The subscription is currently active and not due to be renewed.");
            

            var customer = LazyMapper.GetObjectById<CustomerModel>(customerSubscription.CustomerId);

            if (customer == null)
                throw new Exception("Customer with id '" + customerSubscription.CustomerId + "' could not be found");

            var subscriptionPlan = LazyMapper.GetObjectById<SubscriptionPlanModel>(customerSubscription.SubscriptionPlanId, (string.IsNullOrWhiteSpace(customer.Language) ? "it" : customer.Language));

            if (subscriptionPlan == null)
                throw new Exception("SubscriptionPlan with id '" + customerSubscription.SubscriptionPlanId + "' could not be found");

            var customerBillingProfile = LazyMapper.GetObjectById<CustomerBillingProfileModel>(customer.CustomerBillingProfileId);

            if (customerBillingProfile == null)
                throw new Exception("CustomerBillingProfile with id '" + customer.CustomerBillingProfileId + "' could not be found");


            


            // Get the current pricing 
            var subscriptionPlanPrice = LazyMapper.ExecuteObject<SubscriptionPlanPriceModel>(
               "SELECT * FROM subscriptionplanprice WHERE subscriptionplanid = ?subscriptionplanid AND currency = ?currency AND effectivefrom <= ?effective ORDER BY effectivefrom DESC LIMIT 1;",
               new MySqlParameter("subscriptionplanid", subscriptionPlan.Id),
               new MySqlParameter("effective", customerSubscription.RenewalDate),
               new MySqlParameter("currency", customerSubscription.Currency)
            );

            if (subscriptionPlanPrice == null)
                throw new Exception("Subscription plan price could not be found.");

            var extendedRenewalDate = Utility.TimeSpanExpression.Add(subscriptionPlanPrice.Interval, customerSubscription.RenewalDate);
            var description = subscriptionPlan.Name + " " + customerSubscription.RenewalDate.ToString("d MMM yyyy") + " to " + extendedRenewalDate.ToString("d MMM yyyy");

            // Create a new transaction in the database
            var transaction = LazyMapper.ExecuteObject<CustomerTransactionModel>(
                "INSERT INTO customertransaction ( id, createdat, updatedat, customerid, customersubscriptionid, customerbillingprofileid, customerbillingprofileparameters, amount, currency, provider, reference, description, subscriptionplanid, renewalfrom, renewalto, country, batch, statementdescription ) VALUES ( ?id, ?now, ?now, ?customerid, ?customersubscriptionid, ?customerbillingprofileid, ?customerbillingprofileparameters, ?amount, ?currency, ?provider, ?reference, ?description, ?subscriptionplanid, ?renewalfrom, ?renewalto, ?country, ?batch, ?statementdescription);SELECT * FROM customertransaction WHERE id = ?id;",
                new MySqlParameter("id", Guid.NewGuid()),
                new MySqlParameter("customerid", customerSubscription.CustomerId),
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("customersubscriptionid", customerSubscription.Id),
                new MySqlParameter("subscriptionplanid", subscriptionPlan.Id),
                new MySqlParameter("renewalfrom", customerSubscription.RenewalDate),
                new MySqlParameter("renewalto", extendedRenewalDate),
                new MySqlParameter("customerbillingprofileid", customerBillingProfile.Id),
                new MySqlParameter("customerbillingprofileparameters", customerBillingProfile.Parameters),
                new MySqlParameter("country", customerBillingProfile.Country),
                new MySqlParameter("batch",request.Batch),
                new MySqlParameter("amount", subscriptionPlanPrice.Amount),
                new MySqlParameter("currency", customerSubscription.Currency),
                new MySqlParameter("provider", customerBillingProfile.Provider),
                new MySqlParameter("reference", customerBillingProfile.Reference),
                new MySqlParameter("description", description),
                new MySqlParameter("statementdescription", subscriptionPlan.StatementDescription)
            );

            if (transaction == null)
                throw new Exception("Unable to create a new customer transaction.");

            var result = new PrepareTransactionResult
            {
                Transaction = transaction
            };

            return result;
        }

        public class ProcessTransactionResult
        {
            public CustomerTransactionModel Transaction { get; set; }
        }

        public class ProcessTransactionRequest
        {
            public Guid TransactionId { get; set; }
        }

        public static ProcessTransactionResult ProcessTransaction(ProcessTransactionRequest request)
        {
            var transaction = LazyMapper.ExecuteObject<CustomerTransactionModel>("SELECT * FROM customertransaction WHERE id = ?id ;",new MySqlParameter("id", request.TransactionId));

            if (transaction == null)
                throw new Exception("CustomerTransaction '" + request.TransactionId + "' could not be found.");

            if (transaction.Status != 0)
                throw new Exception("CustomerTransaction with id '" + transaction.Id + "' has already been processed");

            // Check we havent tried billing for this in the past 24 hours
            if (LazyMapper.ExecuteScalar<long>(
                "SELECT count(id) FROM customertransaction WHERE customersubscriptionid = ?customersubscriptionid AND submittedat >= ?submittedafter AND customerbillingprofileid = ?customerbillingprofileid;",
                new MySqlParameter("customersubscriptionid", transaction.CustomerSubscriptionId),
                new MySqlParameter("submittedafter", DateTime.UtcNow.AddHours(-24)),
                new MySqlParameter("customerbillingprofileid", transaction.CustomerBillingProfileId)
                ) > 0)
                throw new Exception("An attempt has already been made to bill this subscription in the past 24 hours.");


            var customerSubscription = LazyMapper.GetObjectById<CustomerSubscriptionModel>(transaction.CustomerSubscriptionId);

            if (customerSubscription == null)
                throw new Exception("CustomerSubscription with id '" + transaction.CustomerSubscriptionId + "' could not be found.");

            if (customerSubscription.ExpiryDate.HasValue && customerSubscription.ExpiryDate < DateTime.UtcNow)
                throw new Exception("The subscription has expired and is not due to be renewed.");

            if (customerSubscription.RenewalDate > DateTime.UtcNow)
                throw new Exception("The subscription is currently active and not due to be renewed.");

            if (customerSubscription.RenewalDate != transaction.RenewalFrom)
                throw new Exception("CustomerSubscription with id '" + transaction.CustomerSubscriptionId + "' renewal date does not match the renewal from of the CustomerTransaction '" + transaction.Id + "'.");

            if (customerSubscription.SubscriptionPlanId != transaction.SubscriptionPlanId)
                throw new Exception("CustomerSubscription with id '" + transaction.CustomerSubscriptionId + "' renewal date does not match the subscription plan of CustomerTransaction '" + transaction.Id + "'.");

            var success = false;
            var providermessage = "";
            var country = transaction.Country;
            var paymentid = (string)null;

            if (transaction.Provider == "StripeCard")
            {
                // Mark submittedat so we can be flagged of any issues
                transaction = LazyMapper.ExecuteObject<CustomerTransactionModel>(
                    "UPDATE customertransaction SET submittedat = ?now, updatedat = ?now, warnat = DATE_ADD( ?now, INTERVAL 5 MINUTE ) WHERE submittedat IS NULL AND id = ?id;SELECT * FROM customertransaction WHERE id = ?id AND submittedat - ?now;",
                    new MySqlParameter("now", DateTime.UtcNow),
                    new MySqlParameter("id", transaction.Id)
                );

                if (transaction == null)
                    throw new Exception("CustomerTransaction '" + request.TransactionId + "' could not be after setting submittedat, the transaction has already been submitted.");

                // Add soem meta dat to help us out later on
                var metaData = new Dictionary<string, string>();
                metaData.Add("customerId", customerSubscription.CustomerId.ToString());
                metaData.Add("customerSubscriptionId", customerSubscription.Id.ToString());
                metaData.Add("customerTransactionId", transaction.Id.ToString());
                metaData.Add("customerSubscriptionDateFrom", transaction.RenewalFrom.ToString("dd MM yyyy HH:mm:ss"));
                metaData.Add("customerSubscriptionDateTo", transaction.RenewalTo.ToString("dd MM yyyy HH:mm:ss"));

                try
                {
                    // Make call to stripe!
                    var chargeService = new Stripe.StripeChargeService();
                    var cardServiceResponse = chargeService.Create(
                        new StripeChargeCreateOptions
                        {
                            Amount = Convert.ToInt32(transaction.Amount * 100),
                            Currency = transaction.Currency,
                            Description = transaction.Description,
                            CustomerId = JsonConvert.DeserializeObject<Areas.ControlPanel.Models.StripeCardParams>(transaction.CustomerBillingProfileParameters).CustomerId,
                            StatementDescriptor = transaction.StatementDescription,
                            Source = new StripeSourceOptions
                            {
                                TokenId = JsonConvert.DeserializeObject<Areas.ControlPanel.Models.StripeCardParams>(transaction.CustomerBillingProfileParameters).Id
                            },
                            Metadata = metaData
                        },
                        new StripeRequestOptions
                        {
                            ApiKey = System.Configuration.ConfigurationManager.AppSettings["StripePrivateKey"]
                        }
                    );
                    success = true;
                    try
                    {
                        country = cardServiceResponse.Source.Card.Country;
                    }
                    catch
                    {
                        country = "GB";
                    }
                    paymentid = cardServiceResponse.Id;
                }
                catch (StripeException ex)
                {
                    paymentid = ex.StripeError.ChargeId;
                    providermessage = ex.StripeError.Message;
                }
                catch(Exception ex)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error("Error occurred sending payment to stripe for transaction '" + request.TransactionId + "', " + ex.Message, ex);
                    throw;
                }


                if (!success && transaction.Batch)
                {
                    var isFinal = false;

                    try
                    {
                        var failedPayments = LazyMapper.ExecuteScalar<long>(
                            "SELECT COUNT(*) FROM customertransaction WHERE status = -1 AND batch = 1 AND customersubscriptionid = ?customersubscriptionid AND renewalfrom = ?renewalfrom AND customerbillingprofileid = ?customerbillingprofileid;",
                            new MySqlParameter("customersubscriptionid", transaction.CustomerSubscriptionId),
                            new MySqlParameter("renewalfrom", transaction.RenewalFrom),
                            new MySqlParameter("customerbillingprofileid", transaction.CustomerBillingProfileId)
                        );

                        if (failedPayments > 3)
                        {
                            customerSubscription = LazyMapper.ExecuteObject<CustomerSubscriptionModel>(
                                "UPDATE customersubscription SET cancellationischurn = 1, cancellationreason = ?cancellationreason, cancellationdate = ?now, expirydate = renewaldate WHERE id = ?customersubscriptionid;SELECT * FROM customersubscription WHERE id = ?customerSubscriptionId",
                                new MySqlParameter("now", DateTime.UtcNow),
                                new MySqlParameter("customersubscriptionid", customerSubscription.Id),
                                new MySqlParameter("cancellationreason", failedPayments + " automated payments failed")
                            );
                            isFinal = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        NLog.LogManager.GetCurrentClassLogger().Warn("Unable to termiate the subscription while processing transaction '" + transaction.Id + "'. Excpetion : " + ex.Message, ex);
                    }

                    var mailTemplateId = "PaymentFailedStripeOther";
                    try
                    {
                        if (providermessage.ToLowerInvariant().Trim().StartsWith("your card was declined"))
                            mailTemplateId = "PaymentFailedStripeDeclined";

                        if (providermessage.ToLowerInvariant().Trim().StartsWith("your card has expired"))
                            mailTemplateId = "PaymentFailedStripeExpired";

                        if (providermessage.ToLowerInvariant().Trim().StartsWith("your card does not support this type of purchase"))
                            mailTemplateId = "PaymentFailedStripeNotSupported";

                        if (providermessage.ToLowerInvariant().Trim().StartsWith("your card has insufficient funds"))
                            mailTemplateId = "PaymentFailedStripeInsufficientFunds";

                        if (isFinal)
                            mailTemplateId = mailTemplateId + "Final";

                        var replacements = new Dictionary<string, string>();
                        replacements.Add("paymentreference", transaction.Reference);
                        replacements.Add("paymentamount", Currency.FormatAmount(transaction.Currency, transaction.Amount));
                        replacements.Add("paymentdescription", transaction.Description);
                        MailService.SendMail(mailTemplateId, transaction.CustomerId, replacements);
                    }
                    catch(Exception ex)
                    {
                        NLog.LogManager.GetCurrentClassLogger().Warn("Unable to send email for the transaction '" + transaction.Id + "' using mail template '" + mailTemplateId + "'. Excpetion : " + ex.Message, ex);
                    }
                }

            }
            else if (transaction.Provider == "PayPal")
            {

                // Mark submittedat so we can be flagged of any issues
                transaction = LazyMapper.ExecuteObject<CustomerTransactionModel>(
                    "UPDATE customertransaction SET submittedat = ?now, updatedat = ?now, warnat = DATE_ADD( ?now, INTERVAL 5 MINUTE ) WHERE submittedat IS NULL AND id = ?id;SELECT * FROM customertransaction WHERE id = ?id AND submittedat - ?now;",
                    new MySqlParameter("now", DateTime.UtcNow),
                    new MySqlParameter("id", transaction.Id)
                );

                var sdkConfig = new Dictionary<string, string>();

                sdkConfig.Add("mode", System.Configuration.ConfigurationManager.AppSettings["PayPalMode"]);
                sdkConfig.Add("account1.apiUsername", System.Configuration.ConfigurationManager.AppSettings["PayPalApiUsername"]);
                sdkConfig.Add("account1.apiPassword", System.Configuration.ConfigurationManager.AppSettings["PayPalApiPassword"]);
                sdkConfig.Add("account1.apiSignature", System.Configuration.ConfigurationManager.AppSettings["PayPalApiSignature"]);

                var service = new PayPalAPIInterfaceServiceService(sdkConfig);

                var doReferenceTransactionReqResponse = service.DoReferenceTransaction(new DoReferenceTransactionReq
                {
                    DoReferenceTransactionRequest = new DoReferenceTransactionRequestType
                    {
                        DoReferenceTransactionRequestDetails = new DoReferenceTransactionRequestDetailsType
                        {
                            PaymentDetails = new PaymentDetailsType
                            {
                                OrderTotal = new BasicAmountType((CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), transaction.Currency), transaction.Amount.ToString("0.00")),
                                OrderDescription = transaction.Description,
                                InvoiceID = transaction.Id.ToString()
                            },
                            ReferenceID = JsonConvert.DeserializeObject<Areas.ControlPanel.Models.PayPalParams>(transaction.CustomerBillingProfileParameters).BillingAgreementId,
                            PaymentAction = PaymentActionCodeType.SALE,
                        }
                    }
                });

                paymentid = doReferenceTransactionReqResponse.DoReferenceTransactionResponseDetails.PaymentInfo.TransactionID;

                if (doReferenceTransactionReqResponse.Ack == AckCodeType.SUCCESS)
                    success = true;

                foreach (var errorMessage in doReferenceTransactionReqResponse.Errors)
                    providermessage += (providermessage != "" ? "" : System.Environment.NewLine) + errorMessage.ShortMessage;
            }
            else
            {
                throw new Exception("Payment provider '" + transaction.Provider + "' is not supported");
            }

            if (success)
            {
                MySqlHelper.ExecuteNonQuery(
                    System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                    "UPDATE customersubscription SET status = 1, renewaldate = ?renewaldate WHERE id = ?id;",
                    new MySqlParameter("id", customerSubscription.Id),
                    new MySqlParameter("renewaldate", transaction.RenewalTo)
                );
            }

            transaction = LazyMapper.ExecuteObject<CustomerTransactionModel>(
                "UPDATE customertransaction SET providerid = ?providerid, status = ?status, updatedat = ?now, completedat = ?now, country = ?country, providermessage = ?providermessage WHERE id = ?id;SELECT * FROM customertransaction WHERE id = ?id;",
                new MySqlParameter("id", transaction.Id),
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("providerid", paymentid),
                new MySqlParameter("providermessage", providermessage),
                new MySqlParameter("status", success ? 1 : -1),
                new MySqlParameter("country", country)
            );

            var result = new ProcessTransactionResult
            {
                Transaction = transaction
            };

            return result;
        }

        public class GetTransactionRequest
        {
            public Guid TransactionId { get; set; }
        }

        public class GetTransactionResponse
        {
            public CustomerTransactionModel Transaction { get; set; }
        }

        public static GetTransactionResponse Get(GetTransactionRequest request)
        {
            var transaction = LazyMapper.ExecuteObject<CustomerTransactionModel>(
                "SELECT *, (SELECT SUM(amount) FROM customertransactionrefund WHERE status = 1 AND customertransactionid = ?id) AS amountrefunded FROM customertransaction WHERE id = ?id",
                new MySqlParameter("id", request.TransactionId)
            );

            if (transaction == null)
                throw new Exception("Customer transaction with id '" + request.TransactionId + "' not found.");

            return new GetTransactionResponse
            {
                Transaction = transaction
            };
        }
    }
}