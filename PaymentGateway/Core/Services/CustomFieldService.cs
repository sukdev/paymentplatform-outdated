﻿using MySql.Data.MySqlClient;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class CustomFieldService
    {

        #region List 

        public class GetCustomFieldListResponse
        {
            public class Item
            {
                public Guid Id { get; set; }
                public string Name { get; set; }
                public string Label { get; set; }
                public string InputType { get; set; }

            }
            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }
        }

        public class GetCustomFieldListRequest
        {
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
        }

        public static GetCustomFieldListResponse GetCustomFieldList(GetCustomFieldListRequest request)
        {
            var sqlWhere = "1 = 1";

            var sql = new StringBuilder();

            sql.AppendLine("SELECT COUNT(*) FROM customfield WHERE " + sqlWhere + ";");
            sql.AppendLine("SELECT * FROM customfield WHERE " + sqlWhere + " ORDER BY displayorder ASC LIMIT ?pageOffset, ?pageLimit;");


            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize)
           );

            var result = new GetCustomFieldListResponse();
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<GetCustomFieldListResponse.Item>)LazyMapper.Hydrate<GetCustomFieldListResponse.Item>(sourceDataSet.Tables[1]);
            return result;
        }

        #endregion

        #region Get 

        public class GetCustomFieldResponse
        {
            public Guid CustomFieldId { get; set; }
            public CustomFieldModel CustomField { get; set; }
        }

        public class GetCustomFieldRequest
        {
            public Guid CustomFieldId { get; set; }
        }

        public static GetCustomFieldResponse GetCustomField(GetCustomFieldRequest request)
        {
            var sqlStatement = new StringBuilder();
            var sqlParams = new List<MySqlParameter>();
            sqlStatement.AppendLine("SELECT * FROM customfield WHERE id = ?id;");
            sqlStatement.AppendLine("SELECT * FROM customfieldoption WHERE customfieldid = ?id ORDER BY displayorder;");

            sqlParams.Add(new MySqlParameter("id", request.CustomFieldId));

            var sourceDataSet = MySqlHelper.ExecuteDataset(
            System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
             sqlStatement.ToString(),
              sqlParams.ToArray()
          );

            if (sourceDataSet.Tables[0].Rows.Count == 0)
                throw new Exception("Unable to load the Custom Field '" + request.CustomFieldId + "'.");

            var customField = LazyMapper.Hydrate<CustomFieldModel>(sourceDataSet.Tables[0].Rows[0]);
            customField.Options = (List<CustomFieldModel.Option>)LazyMapper.Hydrate<CustomFieldModel.Option>(sourceDataSet.Tables[1]);

            return new GetCustomFieldResponse
            {
                CustomFieldId = request.CustomFieldId,
                CustomField = customField
            };
        }

        #endregion


        #region Set 

        public class SetCustomFieldRequest
        {
            public Guid CustomFieldId { get; set; }
            public CustomFieldModel CustomField { get; set; }
        }

        public class SetCustomFieldResponse
        {
            public Guid CustomFieldId { get; set; }
            public CustomFieldModel CustomField { get; set; }
        }

        public static SetCustomFieldResponse SetCustomField(SetCustomFieldRequest request)
        {
            var sqlStatement = new StringBuilder();
            var sqlParams = new List<MySqlParameter>();

            sqlStatement.AppendLine("START TRANSACTION;");

            sqlStatement.AppendLine("UPDATE customfield SET displayorder = ?displayorder, inputtype = ?inputtype, label = ?label, name = ?name, required = ?required, requiredmessage = ?requiredmessage WHERE id = ?id; SELECT * FROM customfield WHERE id = ?id;");

            sqlParams.Add(new MySqlParameter("id", request.CustomFieldId));
            sqlParams.Add(new MySqlParameter("displayorder", request.CustomField.DisplayOrder));
            sqlParams.Add(new MySqlParameter("inputtype", request.CustomField.InputType));
            sqlParams.Add(new MySqlParameter("label", request.CustomField.Label));
            sqlParams.Add(new MySqlParameter("name", request.CustomField.Name));
            sqlParams.Add(new MySqlParameter("required", request.CustomField.Required));
            sqlParams.Add(new MySqlParameter("requiredmessage", request.CustomField.RequiredMessage));

            sqlStatement.AppendLine("DELETE FROM customfieldoption WHERE customfieldid = ?id;");

            for (var i = 0; i < request.CustomField.Options.Count; i++)
            {
                sqlStatement.AppendLine("INSERT INTO customfieldoption ( id, customfieldid, displayorder, displayvalue, reportingvalue ) VALUES ( ?id_#, ?id, ?displayorder_#, ?displayvalue_#, ?reportingvalue_# );".Replace("_#", "_" + i));
                sqlParams.Add(new MySqlParameter("id_" + i, request.CustomField.Options[i].Id));
                sqlParams.Add(new MySqlParameter("displayorder_" + i, i));
                sqlParams.Add(new MySqlParameter("displayvalue_" + i, request.CustomField.Options[i].DisplayValue));
                sqlParams.Add(new MySqlParameter("reportingvalue_" + i, request.CustomField.Options[i].ReportingValue));
            }


            sqlStatement.AppendLine("SELECT * FROM customfield WHERE id = ?id;");
            sqlStatement.AppendLine("SELECT * FROM customfieldoption WHERE customfieldid = ?id ORDER BY displayorder;");

            sqlStatement.AppendLine("COMMIT;");

            var sourceDataSet = MySqlHelper.ExecuteDataset(
              System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sqlStatement.ToString(),
                sqlParams.ToArray()
            );

            if (sourceDataSet.Tables[0].Rows.Count == 0)
                throw new Exception("Unable to save changes to the Custom Field '" + request.CustomFieldId + "'.");

            var customField = LazyMapper.Hydrate<CustomFieldModel>(sourceDataSet.Tables[0].Rows[0]);
            customField.Options = (List<CustomFieldModel.Option>)LazyMapper.Hydrate<CustomFieldModel.Option>(sourceDataSet.Tables[1]);

            return new SetCustomFieldResponse
            {
                CustomFieldId = request.CustomFieldId,
                CustomField = customField
            };
        }

        #endregion


        #region Get Transaltion

        public class GetCustomFieldTransaltionResponse
        {
            public Guid CustomFieldId { get; set; }
            public string Language { get; set; }
            public CustomFieldTransaltionModel CustomFieldTransaltion { get; set; }
        }

        public class GetCustomFieldTransaltionRequest
        {
            public Guid CustomFieldId { get; set; }
            public string Language { get; set; }
        }

        public static GetCustomFieldTransaltionResponse GetCustomFieldTranslation(GetCustomFieldTransaltionRequest request)
        {
            var sqlStatement = new StringBuilder();
            var sqlParams = new List<MySqlParameter>();

            sqlStatement.AppendLine("SELECT * FROM customfieldtranslation WHERE id = ?id AND language = ?language;");
            sqlStatement.AppendLine("SELECT * FROM customfieldoptiontranslation WHERE customfieldid = ?id AND language = ?language;");

            sqlParams.Add(new MySqlParameter("id", request.CustomFieldId));
            sqlParams.Add(new MySqlParameter("language", request.Language));

            var sourceDataSet = MySqlHelper.ExecuteDataset(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                sqlStatement.ToString(),
                sqlParams.ToArray()
            );


            if (sourceDataSet.Tables[0].Rows.Count == 0)
                return new GetCustomFieldTransaltionResponse
                {
                    CustomFieldId = request.CustomFieldId,
                    Language = request.Language,
                    CustomFieldTransaltion = new CustomFieldTransaltionModel
                    {
                        Id = request.CustomFieldId
                    }
                };

            var customFieldTransaltion = LazyMapper.Hydrate<CustomFieldTransaltionModel>(sourceDataSet.Tables[0].Rows[0]);
            customFieldTransaltion.Options = (List<CustomFieldTransaltionModel.Option>)LazyMapper.Hydrate<CustomFieldTransaltionModel.Option>(sourceDataSet.Tables[1]);

            return new GetCustomFieldTransaltionResponse
            {
                CustomFieldId = request.CustomFieldId,
                Language = request.Language,
                CustomFieldTransaltion = customFieldTransaltion
            };
        }

        #endregion

        #region Set Transaltion

        public class SetCustomFieldTransaltionResponse
        {
            public Guid CustomFieldId { get; set; }
            public string Language { get; set; }
            public CustomFieldTransaltionModel CustomFieldTransaltion { get; set; }
        }

        public class SetCustomFieldTransaltionRequest
        {
            public Guid CustomFieldId { get; set; }
            public string Language { get; set; }
            public CustomFieldTransaltionModel CustomFieldTransaltion { get; set; }
        }

        public static SetCustomFieldTransaltionResponse SetCustomFieldTranslation(SetCustomFieldTransaltionRequest request)
        {

            var sqlStatement = new StringBuilder();
            var sqlParams = new List<MySqlParameter>();

            sqlStatement.AppendLine("START TRANSACTION;");
            sqlStatement.AppendLine("DELETE FROM customfieldtranslation WHERE id = ?id and language = ?language;");
            sqlStatement.AppendLine("INSERT INTO customfieldtranslation ( id, language, label, requiredmessage ) VALUES ( ?id, ?language, ?label, ?requiredmessage );");

            sqlParams.Add(new MySqlParameter("id", request.CustomFieldId));
            sqlParams.Add(new MySqlParameter("language", request.Language));           
            sqlParams.Add(new MySqlParameter("label", request.CustomFieldTransaltion.Label));
            sqlParams.Add(new MySqlParameter("requiredmessage", request.CustomFieldTransaltion.RequiredMessage));

            sqlStatement.AppendLine("DELETE FROM customfieldoptiontranslation WHERE customfieldid = ?id AND language = ?language;");

            for (var i = 0; i < request.CustomFieldTransaltion.Options.Count; i++)
            {
                sqlStatement.AppendLine("INSERT INTO customfieldoptiontranslation ( id, customfieldid, language, displayvalue ) VALUES ( ?id_#, ?id, ?language, ?displayvalue_# );".Replace("_#", "_" + i));
                sqlParams.Add(new MySqlParameter("id_" + i, request.CustomFieldTransaltion.Options[i].Id));
                sqlParams.Add(new MySqlParameter("displayvalue_" + i,request.CustomFieldTransaltion.Options[i].DisplayValue));
            }

            sqlStatement.AppendLine("SELECT * FROM customfieldtranslation WHERE id = ?id AND language = ?language;");
            sqlStatement.AppendLine("SELECT * FROM customfieldoptiontranslation WHERE customfieldid = ?id AND language = ?language;");

            sqlStatement.AppendLine("COMMIT;");


            var sourceDataSet = MySqlHelper.ExecuteDataset(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                sqlStatement.ToString(),
                sqlParams.ToArray()
            );

            if (sourceDataSet.Tables[0].Rows.Count == 0)
                throw new Exception("Unable to save changes to the Custom Field '" + request.CustomFieldId + "'.");

            var customFieldTransaltion = LazyMapper.Hydrate<CustomFieldTransaltionModel>(sourceDataSet.Tables[0].Rows[0]);
            customFieldTransaltion.Options = (List<CustomFieldTransaltionModel.Option>)LazyMapper.Hydrate<CustomFieldTransaltionModel.Option>(sourceDataSet.Tables[1]);

            return new SetCustomFieldTransaltionResponse
            {
                CustomFieldId = request.CustomFieldId,
                Language = request.Language,
                CustomFieldTransaltion = customFieldTransaltion
            };
        }

        #endregion
    }
}