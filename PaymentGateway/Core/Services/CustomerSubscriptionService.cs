﻿using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace StreamAMG.OTTPlatform.Core.Services
{
    public class CustomerSubscriptionService
    {

        #region List

        public class GetCustomerSubscriptionListRequest
        {
            public Guid CustomerId { get; set; }
            public Guid SubscriptionPlanId { get; set; }
            public int? Status { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }            
        }

        public class GetCustomerSubscriptionListResponse
        {
            public class Item
            {
                public Guid Id { get; set; }
                public DateTime CreatedAt { get; set; }
                public string SubscriptionPlanName { get; set; }
                public Guid SubscriptionPlanId { get; set; }
                public int Status { get; set; }
                public DateTime RenewalDate { get; set; }
                public DateTime? CancellationDate { get; set; }
                public string CancellationReason { get; set; }
                public DateTime? ExpiryDate { get; set; }
                public string Currency { get; set; }
                public DateTime TrialDate { get; set; }

            }

            public List<Item> Items { get; set; }
            public int PageIndex { get; set; }
            public int PageSize { get; set; }
            public int ItemCount { get; set; }

            public GetCustomerSubscriptionListRequest Request { get; set; }
        }

        public static GetCustomerSubscriptionListResponse GetCustomerSubscriptionList(GetCustomerSubscriptionListRequest request)
        {
            var sqlWhere = "1 = 1";

            if (request.Status.HasValue)
            {
                sqlWhere += " AND status = " + request.Status;               
            }

            if (request.CustomerId != Guid.Empty)
                sqlWhere += " AND customersubscription.customerid = ?customerid";

            if (request.SubscriptionPlanId != Guid.Empty)
                sqlWhere += " AND customersubscription.subscriptionplanid = ?subscriptionplanid";


            var sql = new StringBuilder();            
            sql.AppendLine("SELECT COUNT(customersubscription.id) FROM customersubscription INNER JOIN customer ON customer.id = customersubscription.customerid INNER JOIN subscriptionplan ON subscriptionplan.id = customersubscription.subscriptionplanid WHERE " + sqlWhere + ";");
            sql.AppendLine("SELECT customersubscription.id AS id, customersubscription.createdat, subscriptionplan.name AS subscriptionplanname, customersubscription.status, customersubscription.renewaldate, customersubscription.cancellationdate, customersubscription.expirydate,customersubscription.currency,customersubscription.trialdate FROM customersubscription INNER JOIN customer ON customer.id = customersubscription.customerid INNER JOIN subscriptionplan ON subscriptionplan.id = customersubscription.subscriptionplanid WHERE " + sqlWhere + ";");

            var sourceDataSet = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               sql.ToString(),
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("pageOffset", request.PageIndex * request.PageSize),
                new MySqlParameter("pageLimit", request.PageSize),
                new MySqlParameter("customerid", request.CustomerId),
                new MySqlParameter("subscriptionplanid", request.SubscriptionPlanId)
           );

            var result = new GetCustomerSubscriptionListResponse();
            result.Request = request;
            result.ItemCount = Convert.ToInt32(sourceDataSet.Tables[0].Rows[0][0]);
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.Items = (List<GetCustomerSubscriptionListResponse.Item>)LazyMapper.Hydrate<GetCustomerSubscriptionListResponse.Item>(sourceDataSet.Tables[1]);
            return result;
        }

        #endregion

        #region Get 

        public class GetCustomerSubscriptionResponse
        {
            public CustomerSubscriptionModel CustomerSubscription { get; set; }
        }

        public class GetCustomerSubscriptionRequest
        {
            public Guid CustomerSubscriptionId { get; set; }
        }

        public static GetCustomerSubscriptionResponse GetCustomerSubscription(GetCustomerSubscriptionRequest request)
        {
            var customerSubscription = LazyMapper.GetObjectById<CustomerSubscriptionModel>(request.CustomerSubscriptionId);

            if (customerSubscription == null)
                throw new Exception("Customer Subscription with the id '" + request.CustomerSubscriptionId + "' not found");

            return new GetCustomerSubscriptionResponse
            {
                CustomerSubscription = customerSubscription
            };
        }

        #endregion

        #region Cancel 

        public class CancelSubscriptionRequest
        {
            public Guid CustomeSubscriptionId { get; set; }
            public string Reason { get; set; }
            public bool Immediate { get; set; }
        }

        public class CancelSubscriptionResponse
        {
            public CustomerSubscriptionModel CustomerSubscription { get; set; }
        }

        public static CancelSubscriptionResponse CancelSubscription(CancelSubscriptionRequest request)
        {
            var customerSubscription = LazyMapper.GetObjectById<CustomerSubscriptionModel>(request.CustomeSubscriptionId);

            if (customerSubscription == null)
                throw new Exception("Unable to load the customersubscription with the id '" + request.CustomeSubscriptionId + "'.");

            if (customerSubscription.Status != 1)
                throw new Exception("Unable to cancel an inactive subscription.");

            var customer = LazyMapper.GetObjectById<CustomerModel>(customerSubscription.CustomerId);

            if (customer == null)
                throw new Exception("Unable to load the customer with the id '" + customerSubscription.CustomerId + "'.");

            var subscriptionPlan = LazyMapper.GetObjectById<SubscriptionPlanModel>(customerSubscription.SubscriptionPlanId, customer.Language);
            if (subscriptionPlan == null)
                throw new Exception("Unable to load the subscription plan with the id '" + customerSubscription.SubscriptionPlanId + "'.");

            customerSubscription = LazyMapper.ExecuteObject<CustomerSubscriptionModel>(
                "UPDATE customersubscription SET cancellationReason = ?cancellationReason, cancellationdate = ?now, " + (request.Immediate ? "expirydate = ?now" : "expirydate = renewaldate") + " WHERE id = ?customerSubscriptionId;SELECT * FROM customersubscription WHERE id = ?customerSubscriptionId",
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("customerSubscriptionId", customerSubscription.Id),
                new MySqlParameter("cancellationReason", request.Reason)
            );

            try
            {
                var replacements = new Dictionary<string, string>();
                replacements.Add("subscriptionplanname", subscriptionPlan.Name);
                replacements.Add("renewaldate", customerSubscription.RenewalDate.ToString());
                replacements.Add("cancellationreason", customerSubscription.CancellationReason.ToString());
                if (request.Immediate)
                    MailService.SendMail("CustomerSubscriptionCancelledImmediate", customerSubscription.CustomerId, replacements);
                else
                    MailService.SendMail("CustomerSubscriptionCancelled", customerSubscription.CustomerId, replacements);

            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Warn("Unable to send email for the subscription cancellation of '" + customerSubscription.Id + "'. Excpetion : " + ex.Message, ex);
            }

            return new CancelSubscriptionResponse
            {
                CustomerSubscription = customerSubscription
            };
        }

        #endregion


        public static CustomerSubscriptionModel ReactivateSubscription(Guid id)
        {
            var customerSubscription = LazyMapper.GetObjectById<CustomerSubscriptionModel>(id);

            if (customerSubscription == null)
                throw new Exception("Unable to load the CustomerSubscription with the id '" + id + ".'");

            if (!customerSubscription.CancellationDate.HasValue)
                throw new Exception("Unable to reactivate the subscription because the subscription had not been cancelled.");

            customerSubscription = LazyMapper.ExecuteObject<CustomerSubscriptionModel>(
                @"UPDATE customersubscription SET cancellationreason = NULL, cancellationdate = NULL, expirydate = cancellationexpirydate WHERE id = ?id AND cancellationdate IS NOT NULL;SELECT * FROM customersubscription WHERE id = ?id;",
                new MySqlParameter("now", DateTime.UtcNow),
                new MySqlParameter("id", id)
            );

            return customerSubscription;
        }

        #region Change Subscription Plan

        public class ChangeSubsciptionPlanRequest
        {
            public Guid CustomerSubscriptionId { get; set; }
            public Guid SubscriptionPlanId { get; set; }
        }

        public class ChangeSubsciptionPlanResponse
        {
            public Guid CustomerSubscriptionId { get; set; }
            public CustomerSubscriptionModel CustomerSubscription { get; set; }
        }

        public static ChangeSubsciptionPlanResponse ChangeSubsciptionPlan(ChangeSubsciptionPlanRequest request)
        {
            var customerSubscription = LazyMapper.GetObjectById<CustomerSubscriptionModel>(request.CustomerSubscriptionId);

            if (customerSubscription == null)
                throw new Exception("Unable to load the CustomerSubscription with the id '" + request.CustomerSubscriptionId + ".'");

            if (customerSubscription.Status != 1)
                throw new Exception("Unable to change the subscription plan of an inactive subscription.");

            if (customerSubscription.ExpiryDate.HasValue)
                throw new Exception("Unable to change the subscription plan of an expiring subscription.");

            if (customerSubscription.CancellationDate.HasValue)
                throw new Exception("Unable to change the subscription plan of an cancelled subscription.");

            var getSubscriptionPlanUpgradeOptionsResponse = SubscriptionPlanService.GetSubscriptionPlanUpgradeOptions(new SubscriptionPlanService.GetSubscriptionPlanUpgradeOptionsRequest { SubscriptionPlanId = customerSubscription.SubscriptionPlanId });

            if (!getSubscriptionPlanUpgradeOptionsResponse.TargetSubscriptionPlanIds.Contains(request.SubscriptionPlanId))
                throw new Exception("You cannot upgrade from the subscription plan with id '" + customerSubscription.SubscriptionPlanId + "' to '" + request.SubscriptionPlanId + "'.");

            var customer = LazyMapper.GetObjectById<CustomerModel>(customerSubscription.CustomerId);

            if (customer == null)
                throw new Exception("Unable to load the customer with the id '" + customerSubscription.CustomerId + "'.");

            if (customerSubscription.SubscriptionPlanId == request.SubscriptionPlanId)
                return new ChangeSubsciptionPlanResponse
                {
                    CustomerSubscriptionId = request.CustomerSubscriptionId,
                    CustomerSubscription = customerSubscription
                };

            var subscriptionPlanFrom = LazyMapper.GetObjectById<SubscriptionPlanModel>(customerSubscription.SubscriptionPlanId, customer.Language);
            if (subscriptionPlanFrom == null)
                throw new Exception("Unable to load the subscription plan with the id '" + customerSubscription.SubscriptionPlanId + "'.");

            var subscriptionPlanTo = LazyMapper.GetObjectById<SubscriptionPlanModel>(request.SubscriptionPlanId, customer.Language);
            if (subscriptionPlanTo == null)
                throw new Exception("Unable to load the subscription plan with the id '" + request.SubscriptionPlanId + "'.");

            customerSubscription = LazyMapper.ExecuteObject<CustomerSubscriptionModel>(
            "START TRANSACTION;INSERT INTO customersubscriptionupgrade (id,customersubscriptionid,fromsubscriptionplanid,tosubscriptionplanid,createdat) VALUES (?id,?customersubscriptionid,?fromsubscriptionplanid,?tosubscriptionplanid,?now);UPDATE customersubscription SET subscriptionplanid = ?tosubscriptionplanid WHERE id = ?customersubscriptionid;SELECT * FROM customersubscription WHERE id = ?customersubscriptionid;COMMIT;",
            new MySqlParameter("id", Guid.NewGuid()),
            new MySqlParameter("now", DateTime.UtcNow),
            new MySqlParameter("customersubscriptionid", customerSubscription.Id),
            new MySqlParameter("fromsubscriptionplanid", subscriptionPlanFrom.Id),
            new MySqlParameter("tosubscriptionplanid", subscriptionPlanTo.Id)
            );

            try
            {
                var replacements = new Dictionary<string, string>();
                replacements.Add("subscriptionplantoname", subscriptionPlanTo.Name);
                replacements.Add("subscriptionplanfromname", subscriptionPlanFrom.Name);


                MailService.SendMail("CustomerSubscriptionChanged", customerSubscription.CustomerId, replacements);
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Warn("Unable to send email for the subscription chnage of '" + customerSubscription.Id + "'. Excpetion : " + ex.Message, ex);
            }


            return new ChangeSubsciptionPlanResponse
            {
                CustomerSubscriptionId = request.CustomerSubscriptionId,
                CustomerSubscription = customerSubscription
            };
        }

        #endregion
    }
}