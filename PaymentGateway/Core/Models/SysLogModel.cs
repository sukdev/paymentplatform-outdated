﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class SysLogModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Group { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
        public string Title { get; set; }
        public string IpAddress { get; set; }
        public string IpAddressLocation { get; set; }
        public string UserAgent { get; set; }
        public string UserAgentOs { get; set; }
        public string UserAgentOsVersion { get; set; }
        public string UserAgentBrowser { get; set; }
        public string UserAgentBrowserVersion { get; set; }
        public string UserAgentDeviceType { get; set; }
        public string ServerIpAddress { get; set; }
        public string ServerName { get; set; }
    }
}