﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class UserResetModel
    {
        public Guid Id { get; set; }
        public DateTime Expires { get; set; }
        public Guid UserId { get; set; }
    }
}