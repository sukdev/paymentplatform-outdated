﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class CustomerSubscriptionModel : BaseModel
    {
            
            public Guid CustomerId { get; set; }
            public Guid SubscriptionPlanId { get; set; }
            public int Status { get; set; }
            public DateTime RenewalDate { get; set; }
            public DateTime? CancellationDate { get; set; }
            public string CancellationReason { get; set; }
            public DateTime? ExpiryDate { get; set; }
            public string Currency { get; set; }
            public DateTime TrialDate { get; set; }
            public DateTime TrialExpiryDate { get; set; }

    }
}