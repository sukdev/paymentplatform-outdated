﻿using System;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class CustomerModel:BaseModel
    {        
        public string Password { get; set; }
        public string EmailAddress { get; set; }
        public int LoginAttempt { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Language { get; set; }
        public string StripeId { get; set; }
        public DateTime? FullAccessUntil { get; set; }
        public Guid CustomerBillingProfileId { get; set; }
    }
}