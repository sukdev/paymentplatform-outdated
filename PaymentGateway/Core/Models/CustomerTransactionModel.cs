﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class CustomerTransactionModel : BaseModel
    {
        
        public Guid CustomerId { get; set; }
        public Guid SubscriptionPlanId { get; set; }
        public Guid CustomerSubscriptionId { get; set; }
        public Guid CustomerBillingProfileId { get; set; }
        public string CustomerBillingProfileParameters { get; set; }
        public int Status { get; set; }
        public Decimal Amount { get; set; }
        public Decimal AmountRefunded { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public string Provider { get; set; }
        public string ProviderId { get; set; }
        public string ProviderMessage { get; set; }
        public string Country { get; set; }
        public string Reference { get; set; }
        public DateTime RenewalFrom { get; set; }
        public DateTime RenewalTo { get; set; }
        public string StatementDescription { get; set; }
        public DateTime? CompletedAt { get; set; }
        public DateTime? SubmittedAt { get; set; }
        public DateTime? WarnAt { get; set; }
        public string Log { get; set; }
        public bool Batch { get; set; }


    }
}