﻿using System;


namespace StreamAMG.OTTPlatform.Core.Models
{


    public class MailLogModel
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public int Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string MailTo { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string ReferenceId { get; set; }

    }
}