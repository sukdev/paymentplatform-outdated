﻿using System;
using System.Collections.Generic;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class OfferModel:BaseModel
    {
        public string Name { get; set; }
        public string AppliesFor { get; set; }
        public Decimal Discount { get; set; }
        public List<Guid> SubscriptionPlanId { get; set; }
    }
}