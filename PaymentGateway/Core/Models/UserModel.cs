﻿using System;
using System.Collections.Generic;


namespace StreamAMG.OTTPlatform.Core.Models
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string EmailAddress { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MobilePhone { get; set; }
        public List<Guid> SystemRoleIds { get; set; }
        public bool Deleted { get; set; }

        public UserModel()
        {
            SystemRoleIds = new List<Guid>();
        }
    }
}