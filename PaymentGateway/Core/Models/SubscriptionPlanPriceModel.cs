﻿using System;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class SubscriptionPlanPriceModel : BaseModel
    {
        public Guid SubscriptionPlanId { get; set; }
        public DateTime EffectiveFrom { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
        public string Interval { get; set; }
        public string TrialDuration { get; set; }
        public DateTime? TrialDate { get; set; }
        public string Duration { get; set; }
    }    
}