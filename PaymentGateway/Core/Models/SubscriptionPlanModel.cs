﻿using System;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class SubscriptionPlanModel : BaseModel
    {

        public string Name { get; set; }

        public int DisplayOrder { get; set; }

        public string Entitlements { get; set; }

        public string StatementDescription { get; set; }

        public string ListingTitle { get; set; }
        public string ListingBody { get; set; }
        public string ListingStyle { get; set; }

        public string CheckoutTitle { get; set; }
        public string CheckoutBody { get; set; }
        public string CheckoutStyle { get; set; }


        public string ListingTitleWithFreeTrial { get; set; }
        public string ListingBodyWithFreeTrial { get; set; }
        public string ListingStyleWithFreeTrial { get; set; }

        public string CheckoutTitleWithFreeTrial { get; set; }
        public string CheckoutBodyWithFreeTrial { get; set; }
        public string CheckoutStyleWithFreeTrial { get; set; }

    }
}