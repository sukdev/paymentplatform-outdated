﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class CustomerBillingProfileModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid CustomerId { get; set; }
        public string Provider { get; set; }
        public string Parameters { get; set; }
        public string Reference { get; set; }
        public DateTime? Expires { get; set; }
        public string Country { get; set; }
        public string FingerPrint { get; set; }
    }
}