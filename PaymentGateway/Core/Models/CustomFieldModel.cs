﻿using System;
using System.Collections.Generic;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class CustomFieldModel : BaseModel
    {
        public class Option
        {
            public Guid Id { get; set; }
            public string ReportingValue { get; set; }
            public string DisplayValue { get; set; }
        }

        public string Name { get; set; }
        public string Label { get; set; }
        public int DisplayOrder { get; set; }
        public string InputType { get; set; }
        public string RequiredMessage { get; set; }
        public bool Required { get; set; }

        public List<Option> Options { get; set; }

        public CustomFieldModel()
        {
            Options = new List<Option>();
        }
    }
}