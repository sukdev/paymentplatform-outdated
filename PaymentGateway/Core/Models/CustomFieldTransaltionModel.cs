﻿using System;
using System.Collections.Generic;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class CustomFieldTransaltionModel : BaseModel
    {
        public class Option
        {
            public Guid Id { get; set; }            
            public string DisplayValue { get; set; }            
        }

        public string Label { get; set; }
        public string RequiredMessage { get; set; }
        public List<Option> Options { get; set; }

        public CustomFieldTransaltionModel()
        {
            Options = new List<Option>();
        }
    }
}