﻿using System;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class OfferVoucherModel : BaseModel
    {
        public Guid OfferId { get; set; }
        public string Code { get; set; }
        public DateTime ValidFrom { get; set;}
        public DateTime ValidTo { get; set;}
    }
}