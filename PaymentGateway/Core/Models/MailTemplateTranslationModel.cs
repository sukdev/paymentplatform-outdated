﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class MailTemplateTranslationModel 
    {
        public string Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }

}