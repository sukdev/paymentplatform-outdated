﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Core.Models
{
    public class SystemRole
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}