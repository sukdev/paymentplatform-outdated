﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Domains;
using StreamAMG.OTTPlatform.Core.Services;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using StreamAMG.OTTPlatform.Core.Utilities;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Security
{
    public class ControlPanelAuthentication
    {
        private static List<SystemRole> _systemRoles;


        public class SystemRole
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }

        public static List<SystemRole> SystemRoles
        {
            get
            {
                return _systemRoles;
            }
        }

        static ControlPanelAuthentication()
        {
            var sourceDataSet = MySqlHelper.ExecuteDataset(
              System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
              "SELECT * FROm systemrole ORDER BY name"
            );

            _systemRoles = (List<SystemRole>)LazyMapper.Hydrate<SystemRole>(sourceDataSet.Tables[0]);
        }


        public enum VerificationResult
        {
            OK = -1,
            Incorrect = 1,
            Blocked = 3,
            NotAuthenticated = 4
        }

        public enum AuthenticationResult
        {
            OK = -1,
            Incorrect = 1,
            Blocked = 3,
            SmsFailed = 4
        }

        public enum RequestResetResult
        {
            OK = -1,
            Incorrect = 1,
            Blocked = 2,
            FailedInsert = 3,
            FailedMail = 4
        }
        public enum ResetResult
        {
            OK = -1,
            Expired = 1,
            Blocked = 2,
            Username = 3,
            Failed = 4
        }

        public static UserProfile UserProfile
        {
            get
            {
                if (!IsAuthenticated)
                    return null;

                return (UserProfile)HttpContext.Current.Items["controlpanel_session_userprofile"];
            }
        }

        public static bool VerifyCurrentUser(string password)
        {
            if (!IsAuthenticated)
                return false;

            return BCrypt.Verify(password, HttpContext.Current.Items["controlpanel_session_password"].ToString());
        }

        public static bool CurrentUserCanAccess(string role)
        {
            if (!IsAuthenticated)
                return false;

            if (role == "*")
                return true;

            return ((List<SystemRole>) HttpContext.Current.Items["controlpanel_session_systemroles"]).Any(
                x => x.Name.ToLowerInvariant().Trim() == role.ToLowerInvariant().Trim());
        }

        public static bool CurrentUserCanAccess(string[] roles)
        {
            foreach (var role in roles)
                if (CurrentUserCanAccess(role))
                    return true;

            return false;
        }

        public static ResetResult Reset(Guid id, string username, string password)
        {
            // Grab the reset row, first clear down anything that has expired
            var userResetRow = MySqlHelper.ExecuteDataRow(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "DELETE FROM userreset WHERE expires < ?now;SELECT * FROM userreset WHERE id = ?id",
                new MySqlParameter("id", id),
                new MySqlParameter("now", DateTime.UtcNow)
            );

            // If we dont have one assume its expired, return an error
            if (userResetRow == null)
                return ResetResult.Expired;

            // Now the active user
            var userRow = MySqlHelper.ExecuteDataRow(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "SELECT * FROM user WHERE id = ?id AND deleted = 0",
                new MySqlParameter("id", userResetRow["userid"])
            );

            // If we cant get one assume its blocked, return an error
            if (userRow == null)
                return ResetResult.Blocked;

            // If the username doesn't match the one passssed return an error
            if (Convert.ToString(userRow["username"]).ToLowerInvariant() != username.ToLowerInvariant())
                return ResetResult.Username;

            // Finally has teh password, and update the user row, reset login attempts so they can login
            var rowCount = MySqlHelper.ExecuteNonQuery(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "UPDATE user SET password = ?password, loginattempt = 0 WHERE id = ?id",
                new MySqlParameter("id", userRow["id"]),
                new MySqlParameter("password", BCrypt.HashPassword(password, 8))
            );

            if (rowCount < 1)
                return ResetResult.Failed;

            return ResetResult.OK;
        }

        public static RequestResetResult RequestReset(string username)
        {
            var userRow = MySqlHelper.ExecuteDataRow(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "SELECT * FROM user WHERE username = ?username",
                new MySqlParameter("username", username)
            );

            if (userRow == null)
                return RequestResetResult.Incorrect;

            if (Convert.ToBoolean(userRow["deleted"]))
                return RequestResetResult.Blocked;

            var id = System.Guid.NewGuid();

            var rowCount = MySqlHelper.ExecuteNonQuery(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "INSERT INTO userreset (id,userid,expires) VALUES (?id,?userid,?expires)",
                new MySqlParameter("id", id),
                new MySqlParameter("userid", userRow["id"]),
                new MySqlParameter("expires", DateTime.UtcNow.AddHours(1))
            );

            if (rowCount < 1)
                return RequestResetResult.FailedInsert;

            try
            {
                
                var replacements = new Dictionary<string, string>();
                replacements.Add("name", userRow["name"].ToString());
                replacements.Add("surname", userRow["surname"].ToString());
                replacements.Add("username", userRow["username"].ToString());
                replacements.Add("reseturl", "~/controlpanel/reset/" + id.ToString());
                MailService.SendSystemMail(
                    HttpContext.Current.Server.MapPath("/areas/controlpanel/_resx/templates/PasswordReset.html"),
                    replacements,
                    userRow["emailaddress"].ToString()
                );
            }
            catch (Exception ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error("Unable to send password reset email " + ex.Message);
                return RequestResetResult.FailedMail;
            }

            return RequestResetResult.OK;
        }

        public static bool IsVerified
        {
            get
            {
                if (!IsAuthenticated)
                    return false;

                return (bool)HttpContext.Current.Items["controlpanel_verified"];
            }
        }

        public static bool IsAuthenticated
        {
            get
            {
                if (!HttpContext.Current.Items.Contains("controlpanel_session_id"))
                {

                    // Default to empty
                    HttpContext.Current.Items["controlpanel_session_id"] = Guid.Empty.ToString();

                    // If we have a cookie validate it
                    if (HttpContext.Current.Request.Cookies["controlpanel"] != null && HttpContext.Current.Request.Cookies["controlpanel"]["session"] != null)
                    {
                        // Validate the value in mysql and extend if needed
                        var sessionRow = MySqlHelper.ExecuteDataRow(
                            System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                            "UPDATE usersession SET expires = ?newexpiry WHERE id = ?id AND expires >= ?now;DELETE FROM usersession WHERE id = ?id AND expires < ?now;SELECT * FROM usersession WHERE id = ?id AND expires >= ?now;",
                            new MySqlParameter("id", HttpContext.Current.Request.Cookies["controlpanel"]["session"]),
                            new MySqlParameter("now", DateTime.UtcNow),
                            new MySqlParameter("newexpiry", DateTime.UtcNow.AddHours(1))
                        );

                        if (sessionRow != null && sessionRow["useragent"].ToString() == GetUserAgent())
                        {
                            HttpContext.Current.Items["controlpanel_session_id"] = sessionRow["id"].ToString();
                            HttpContext.Current.Items["controlpanel_verified"] = Convert.ToBoolean(sessionRow["verificationpassed"]);

                            var userDataSet = MySqlHelper.ExecuteDataset(
                                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                                "SELECT * FROM user WHERE id = ?id; SELECT systemroleid AS id, systemrole.name AS name FROM usersystemrole INNER JOIN systemrole ON systemrole.id = usersystemrole.systemroleid WHERE userid = ?id;",
                                new MySqlParameter("id", sessionRow["userid"])
                            );

                            HttpContext.Current.Items["controlpanel_session_userprofile"] = new UserProfile
                            {
                                Id = new Guid(userDataSet.Tables[0].Rows[0]["Id"].ToString()),
                                Name = userDataSet.Tables[0].Rows[0]["Name"].ToString(),
                                Surname = userDataSet.Tables[0].Rows[0]["Surname"].ToString(),
                                EmailAddress = userDataSet.Tables[0].Rows[0]["EmailAddress"].ToString(),
                                Languages = userDataSet.Tables[0].Rows[0]["Languages"].ToString().Split(',').ToList<string>()
                            };

                            HttpContext.Current.Items["controlpanel_session_systemroles"] = (List<SystemRole>)LazyMapper.Hydrate<SystemRole>(userDataSet.Tables[1]);

                            HttpContext.Current.Items["controlpanel_session_password"] = userDataSet.Tables[0].Rows[0]["Password"].ToString();
                        }
                        else if (sessionRow != null && sessionRow["useragent"].ToString() != GetUserAgent())
                        {
                            //TO DO add some logging
                            HttpContext.Current.Response.Cookies.Remove("controlpanel");
                        }
                        else
                        {
                            HttpContext.Current.Response.Cookies.Remove("controlpanel");
                        }
                    }
                }

                return HttpContext.Current.Items["controlpanel_session_id"].ToString() != Guid.Empty.ToString();
            }
        }

        private static bool SendSMS(string phone, string code)
        {
            var result = "";

            using (var wb = new WebClient())
            {
                byte[] response = wb.UploadValues("https://api.txtlocal.com/send/", new NameValueCollection()
                {
                {"username" , System.Configuration.ConfigurationManager.AppSettings["TxtLocalUsername"] },
                {"hash" , System.Configuration.ConfigurationManager.AppSettings["TxtLocalHash"] },
                {"numbers" , phone},
                {"message" , HttpUtility.UrlEncode(code + " CouncilConnect verification code")},
                {"sender" , "CouncilCast"}
                });
                result = System.Text.Encoding.UTF8.GetString(response);
            }

            //TODO this shoudl be better but its 1am
            return result.Contains("\"success\"");
        }

        private static string GetUserAgent()
        {
            var result = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(result))
                result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            else
                result = result.Split(',')[0];

            result = result + HttpContext.Current.Request.UserAgent;
            return result;
        }

        public static VerificationResult Verify(string verificationCode)
        {
            if (!IsAuthenticated)
                return VerificationResult.NotAuthenticated;

            // Grab the current
            var sessionRow = MySqlHelper.ExecuteDataRow(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "UPDATE usersession SET verificationattempt = verificationattempt + 1 WHERE id = ?id;SELECT * FROM usersession WHERE id = ?id;",
                new MySqlParameter("id", HttpContext.Current.Items["controlpanel_session_id"])
            );

            // If teh session doesnt exist then return not authenticated
            if (sessionRow == null)
                return VerificationResult.NotAuthenticated;

            if (Convert.ToInt32(sessionRow["verificationattempt"]) >= 10)
                return VerificationResult.Blocked;

            if (BCrypt.Verify(verificationCode, sessionRow["verificationpassword"].ToString()))
            {
                MySqlHelper.ExecuteDataRow(
                    System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                    "UPDATE usersession SET verificationpassed = 1 WHERE id = ?id;",
                    new MySqlParameter("id", HttpContext.Current.Items["controlpanel_session_id"])
                );

                return VerificationResult.OK;
            }
            else
            {
                return VerificationResult.Incorrect;
            }
        }

        public static AuthenticationResult Login(string username, string password)
        {
            var sessionGuid = Guid.NewGuid();
            var result = AuthenticationResult.OK;

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
                return AuthenticationResult.Incorrect;

            var userRow = MySqlHelper.ExecuteDataRow(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "UPDATE user SET loginattempt = loginattempt + 1 WHERE username = ?username;SELECT * FROM user WHERE username = ?username",
                new MySqlParameter("username", username)
            );

            if (userRow == null)
                return AuthenticationResult.Incorrect;

            if (Convert.ToBoolean(userRow["deleted"]))
                return AuthenticationResult.Blocked;

            if (Convert.ToInt32(userRow["loginattempt"]) >= 10)
                return AuthenticationResult.Blocked;

            if (!BCrypt.Verify(password, Convert.ToString(userRow["password"])))
                return AuthenticationResult.Incorrect;
            

            var verificationDisabled = System.Configuration.ConfigurationManager.AppSettings["TwoFactorAuthenticationDisabled"] == "true";


            var generator = new Random();
            var verificationCode = generator.Next(0, 1000000).ToString("D6");

            // Send the SMS if we can
            if (!verificationDisabled)
            {
                if (!SendSMS(userRow["mobilephone"].ToString(), verificationCode))
                    return AuthenticationResult.SmsFailed;
            }

            MySqlHelper.ExecuteNonQuery(
                System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                "UPDATE user SET loginattempt = 0 WHERE id = ?userid;INSERT INTO usersession (id,userid,expires,useragent,verificationpassed,verificationpassword) VALUES (?id,?userid,?expires,?useragent,?verificationpassed,?verificationpassword)",
                new MySqlParameter("id", sessionGuid),
                new MySqlParameter("userid", userRow["id"].ToString()),
                new MySqlParameter("expires", DateTime.UtcNow.AddHours(1)),
                new MySqlParameter("useragent", GetUserAgent()),
                new MySqlParameter("verificationpassed", verificationDisabled),
                new MySqlParameter("verificationpassword", BCrypt.HashPassword(verificationCode, 8))
            );

            HttpContext.Current.Response.Cookies["controlpanel"]["session"] = sessionGuid.ToString();
            HttpContext.Current.Items["controlpanel_session_id"] = sessionGuid.ToString();
            return result;
        }

        public static void Logout()
        {
            if (!IsAuthenticated)
                return;

            // Delete the record from the db
            MySqlHelper.ExecuteNonQuery(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
               "DELETE FROM usersession WHERE id = ?id",
               new MySqlParameter("id", HttpContext.Current.Items["controlpanel_session_id"].ToString())
           );

            HttpContext.Current.Response.Cookies.Remove("controlpanel");
            // Clear the cookie
        }
    }
}
