﻿using NLog;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Core.Services;
using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{
    [RouteArea("ControlPanel")]
    public class CustomerController : Controller
    {

        #region Index

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customer")]
        public ActionResult Index()
        {
            var exportToCsv = !string.IsNullOrWhiteSpace(Request.QueryString["export"]);
            var searchRequest = new CustomerSearchService.SearchRequest
            {
                PageIndex = 0,
                PageSize = exportToCsv ? int.MaxValue : 20
            };

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pageindex"]))
                searchRequest.PageIndex = Convert.ToInt32(Request.QueryString["pageindex"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pagesize"]))
                searchRequest.PageSize = Convert.ToInt32(Request.QueryString["pagesize"]);

            if (Request.QueryString["activesubscriptions"] == "1")
                searchRequest.ActiveSubscriptionCountFrom = 1;

            if (Request.QueryString["activesubscriptions"] == "2")
                searchRequest.ActiveSubscriptionCountFrom = 2;

            if (Request.QueryString["activesubscriptions"] == "-1")
                searchRequest.ActiveSubscriptionCountTo = 0;

            if (Request.QueryString["duesubscriptions"] == "1")
                searchRequest.DueSubscriptionCountFrom = 1;

            if (Request.QueryString["duesubscriptions"] == "-1")
                searchRequest.DueSubscriptionCountTo = 0;

            if (!string.IsNullOrWhiteSpace(Request.QueryString["activesubscriptions"]) && Regex.IsMatch(Request.QueryString["activesubscriptions"], @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"))
                searchRequest.ActiveSubscriptionPlanId = new Guid(Request.QueryString["activesubscriptions"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["keywords"]))
                searchRequest.Keywords = Request.QueryString["keywords"];

            var searchResult = CustomerSearchService.Search(searchRequest);

            if (!exportToCsv)
                return View(searchResult);

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=customers.csv");
            Response.ContentType = "text/csv";
            Response.Write(Utility.Helpers.CreateCSVAndAppendCustomerFieldsFromGenericList(searchResult.Items,"Id"));
            Response.End();
            return null;
        }

        #endregion

        #region View

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customer/{customerId:guid}")]
        public ActionResult View(Guid customerId)
        {
            var getCustomerResponse = CustomerService.GetCustomer(
                new CustomerService.GetCustomerRequest
                {
                    CustomerId = customerId
                }
            );

            return View(getCustomerResponse.Customer);
        }

        #endregion

        #region Edit Customer

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customer/{customerId:guid}/edit")]
        public ActionResult Edit(Guid customerId)
        {

            var getCustomerProfileResponse = CustomerService.GetCustomerProfile(new CustomerService.GetCustomerProfileRequest { CustomerId = customerId });
            var getCustomerPermissionsResponse = CustomerService.GetCustomerPermissions(new CustomerService.GetPermissionsRequest { CustomerId = customerId });


            var model = new CustomerEditViewModel
            {
                EmailAddress = getCustomerProfileResponse.CustomerProfile.EmailAddress,
                FirstName = getCustomerProfileResponse.CustomerProfile.FirstName,
                LastName = getCustomerProfileResponse.CustomerProfile.LastName,
                Language = getCustomerProfileResponse.CustomerProfile.Language,
                Blocked = getCustomerPermissionsResponse.CustomerPermissions.Blocked,
                FullAccessUntil = getCustomerPermissionsResponse.CustomerPermissions.FullAccessUntil
            };

            return View(model);
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customer/{customerId:guid}/edit")]
        public ActionResult Edit(Guid customerId, CustomerEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var setCustomerProfileResponse = CustomerService.SetCustomerProfile(new CustomerService.SetCustomerProfileRequest
                {
                    CustomerId = customerId,
                    CustomerProfile = new CustomerService.CustomerProfile
                    {
                        EmailAddress = model.EmailAddress,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Password = model.Password,
                        Language = model.Language
                    }
                });

                var setCustomerPermissionsResponse = CustomerService.SetCustomerPermissions(new CustomerService.SetPermissionsRequest
                {
                    CustomerId = customerId,
                    CustomerPermissions = new CustomerService.CustomerPermissions
                    {
                        Blocked = model.Blocked,
                        FullAccessUntil = model.FullAccessUntil
                    }
                });
                ViewBag.Success = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to save customer profile. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }

        #endregion

        #region Cancel Customer Subscription

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customers/{customerId:guid}/customersubscription/{customerSubscriptionId:guid}/cancel")]
        public ActionResult CancelSubscription(Guid customerId, Guid customerSubscriptionId)
        {
            var customerSubscriptionGetResponse = CustomerSubscriptionService.GetCustomerSubscription(
                new CustomerSubscriptionService.GetCustomerSubscriptionRequest
                {
                    CustomerSubscriptionId = customerSubscriptionId
                }
            );

            var subscriptionPlanGetResponse = SubscriptionPlanService.Get(
                new SubscriptionPlanService.GetRequest
                {
                    SubscriptionPlanId = customerSubscriptionGetResponse.CustomerSubscription.SubscriptionPlanId
                }
            );

            var model = new CustomerCancelSubscriptionViewModel
            {
                SubscriptionPlanName = subscriptionPlanGetResponse.SubscriptionPlan.Name,
                SubscriptionRenewalDate = customerSubscriptionGetResponse.CustomerSubscription.RenewalDate
            };

            return View(model);
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customers/{customerId:guid}/customersubscription/{customerSubscriptionId:guid}/cancel")]
        public ActionResult CancelSubscription(Guid customerId, Guid customerSubscriptionId, CustomerCancelSubscriptionViewModel model)
        {

            var customerSubscriptionGetResponse = CustomerSubscriptionService.GetCustomerSubscription(
                new CustomerSubscriptionService.GetCustomerSubscriptionRequest
                {
                    CustomerSubscriptionId = customerSubscriptionId
                }
            );

            var subscriptionPlanGetResponse = SubscriptionPlanService.Get(
                new SubscriptionPlanService.GetRequest
                {
                    SubscriptionPlanId = customerSubscriptionGetResponse.CustomerSubscription.SubscriptionPlanId
                }
            );

            model.SubscriptionPlanName = subscriptionPlanGetResponse.SubscriptionPlan.Name;
            model.SubscriptionRenewalDate = customerSubscriptionGetResponse.CustomerSubscription.RenewalDate;

            if (!ModelState.IsValid)
                return View(model);

            try
            {
                // Update Customer Subscription
                CustomerSubscriptionService.CancelSubscription(new CustomerSubscriptionService.CancelSubscriptionRequest { CustomeSubscriptionId = customerSubscriptionId, Reason = model.CancellationReason, Immediate = model.CancellationImmediate });
                return Redirect("/controlpanel/customer/" + customerId + "/?returnurl=" + HttpUtility.UrlEncode(Request.QueryString["returnurl"]));
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to cancel Customer Subscription. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }

        #endregion

        #region Reactivate Customer Subscription

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customers/{customerId:guid}/customersubscription/{customerSubscriptionId:guid}/reactivate")]
        public ActionResult ReactivateSubscription(Guid customerId, Guid customerSubscriptionId)
        {
            var customerSubscriptionGetResponse = CustomerSubscriptionService.GetCustomerSubscription(
                new CustomerSubscriptionService.GetCustomerSubscriptionRequest
                {
                    CustomerSubscriptionId = customerSubscriptionId
                }
            );

            var subscriptionPlanGetResponse = SubscriptionPlanService.Get(
                new SubscriptionPlanService.GetRequest
                {
                    SubscriptionPlanId = customerSubscriptionGetResponse.CustomerSubscription.SubscriptionPlanId
                }
            );

            var model = new CustomerReactivateSubscriptionViewModel
            {
                SubscriptionPlanName = subscriptionPlanGetResponse.SubscriptionPlan.Name,
                SubscriptionRenewalDate = customerSubscriptionGetResponse.CustomerSubscription.RenewalDate
            };

            return View(model);
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customers/{customerId:guid}/customersubscription/{customerSubscriptionId:guid}/reactivate")]
        public ActionResult ReactivateSubscription(Guid customerId, Guid customerSubscriptionId, CustomerReactivateSubscriptionViewModel model)
        {

            var customerSubscriptionGetResponse = CustomerSubscriptionService.GetCustomerSubscription(
                new CustomerSubscriptionService.GetCustomerSubscriptionRequest
                {
                    CustomerSubscriptionId = customerSubscriptionId
                }
            );

            var subscriptionPlanGetResponse = SubscriptionPlanService.Get(
                new SubscriptionPlanService.GetRequest
                {
                    SubscriptionPlanId = customerSubscriptionGetResponse.CustomerSubscription.SubscriptionPlanId
                }
            );


            model.SubscriptionPlanName = subscriptionPlanGetResponse.SubscriptionPlan.Name;
            model.SubscriptionRenewalDate = customerSubscriptionGetResponse.CustomerSubscription.RenewalDate;
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                // Update Customer Subscription
                CustomerSubscriptionService.ReactivateSubscription(customerSubscriptionId);
                return Redirect("/controlpanel/customer/" + customerId + "/?returnurl=" + HttpUtility.UrlEncode(Request.QueryString["returnurl"]));
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to Reactivate Subscription. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }

        #endregion


        #region Edit Customer Subscription

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customers/{customerId:guid}/customersubscription/{customerSubscriptionId:guid}/edit")]
        public ActionResult EditSubscription(Guid customerId, Guid customerSubscriptionId)
        {
            var customerSubscriptionGetResponse = CustomerSubscriptionService.GetCustomerSubscription(
                new CustomerSubscriptionService.GetCustomerSubscriptionRequest
                {
                    CustomerSubscriptionId = customerSubscriptionId
                }
            );

            var subscriptionPlanGetResponse = SubscriptionPlanService.Get(
                new SubscriptionPlanService.GetRequest
                {
                    SubscriptionPlanId = customerSubscriptionGetResponse.CustomerSubscription.SubscriptionPlanId
                }
            );

            var model = new CustomerSubscriptionEditViewModel
            {
                CurrentSubscriptionPlanId = subscriptionPlanGetResponse.SubscriptionPlanId,
                CurrentSubscriptionPlanName = subscriptionPlanGetResponse.SubscriptionPlan.Name,
                CurrentSubscriptionRenewalDate = customerSubscriptionGetResponse.CustomerSubscription.RenewalDate
            };


            var getSubscriptionPlanUpgradeOptionsResponse = SubscriptionPlanService.GetSubscriptionPlanUpgradeOptions(
                new SubscriptionPlanService.GetSubscriptionPlanUpgradeOptionsRequest
                {
                    SubscriptionPlanId = customerSubscriptionGetResponse.CustomerSubscription.SubscriptionPlanId
                }
            );

            model.SubscriptionPlanIds.Add(new SelectListItem { Text = subscriptionPlanGetResponse.SubscriptionPlan.Name, Value = subscriptionPlanGetResponse.SubscriptionPlanId.ToString() });
            foreach (var targetSubscriptionPlanId in getSubscriptionPlanUpgradeOptionsResponse.TargetSubscriptionPlanIds)
            {
                var targetSubscriptionPlanGetResponse = SubscriptionPlanService.Get(new SubscriptionPlanService.GetRequest { SubscriptionPlanId = targetSubscriptionPlanId });
                model.SubscriptionPlanIds.Add(new SelectListItem { Text = targetSubscriptionPlanGetResponse.SubscriptionPlan.Name, Value = targetSubscriptionPlanId.ToString() });
            }

            return View(model);
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customers/{customerId:guid}/customersubscription/{customerSubscriptionId:guid}/edit")]
        public ActionResult EditSubscription(Guid customerId, Guid customerSubscriptionId, CustomerSubscriptionEditViewModel model)
        {

            var customerSubscriptionGetResponse = CustomerSubscriptionService.GetCustomerSubscription(
                new CustomerSubscriptionService.GetCustomerSubscriptionRequest
                {
                    CustomerSubscriptionId = customerSubscriptionId
                }
            );

            var subscriptionPlanGetResponse = SubscriptionPlanService.Get(
                new SubscriptionPlanService.GetRequest
                {
                    SubscriptionPlanId = customerSubscriptionGetResponse.CustomerSubscription.SubscriptionPlanId
                }
            );

            model.CurrentSubscriptionPlanId = subscriptionPlanGetResponse.SubscriptionPlanId;
            model.CurrentSubscriptionPlanName = subscriptionPlanGetResponse.SubscriptionPlan.Name;
            model.CurrentSubscriptionRenewalDate = customerSubscriptionGetResponse.CustomerSubscription.RenewalDate;

            var getSubscriptionPlanUpgradeOptionsResponse = SubscriptionPlanService.GetSubscriptionPlanUpgradeOptions(
                new SubscriptionPlanService.GetSubscriptionPlanUpgradeOptionsRequest
                {
                    SubscriptionPlanId = customerSubscriptionGetResponse.CustomerSubscription.SubscriptionPlanId
                }
            );

            model.SubscriptionPlanIds.Add(new SelectListItem { Text = subscriptionPlanGetResponse.SubscriptionPlan.Name, Value = subscriptionPlanGetResponse.SubscriptionPlanId.ToString() });
            foreach (var targetSubscriptionPlanId in getSubscriptionPlanUpgradeOptionsResponse.TargetSubscriptionPlanIds)
            {
                var targetSubscriptionPlanGetResponse = SubscriptionPlanService.Get(new SubscriptionPlanService.GetRequest { SubscriptionPlanId = targetSubscriptionPlanId });
                model.SubscriptionPlanIds.Add(new SelectListItem { Text = targetSubscriptionPlanGetResponse.SubscriptionPlan.Name, Value = targetSubscriptionPlanId.ToString() });
            }

            if (!ModelState.IsValid)
                return View(model);

            try
            {
                // Update Customer Subscription
                CustomerSubscriptionService.ChangeSubsciptionPlan(new CustomerSubscriptionService.ChangeSubsciptionPlanRequest { CustomerSubscriptionId = customerSubscriptionId, SubscriptionPlanId = model.SubscriptionPlanId });
                return Redirect("/controlpanel/customer/" + customerId + "/?returnurl=" + HttpUtility.UrlEncode(Request.QueryString["returnurl"]));
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to edit the subscription. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }
        #endregion
    }
}