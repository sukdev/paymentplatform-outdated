﻿using MySql.Data.MySqlClient;
using NLog;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Core.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{
    [RouteArea("ControlPanel")]
    public class AutoCompleteController : Controller
    {

        //
        // GET: /AutoComplete/
        public class Nvp
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }


        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/autocomplete/")]
        public ActionResult Get()
        {
            var result = new List<Nvp>();

            var sql = @"SELECT  " + Request.QueryString["i"].Replace("'", "''") + " , " + Request.QueryString["v"].Replace("'", "''") + "  AS name FROM " + Request.QueryString["t"].Replace("'", "''") + " WHERE " + Request.QueryString["v"].Replace("'", "''") + " LIKE '" + Request.QueryString["q"].Replace("'", "''") + "%';";

            foreach (DataRow dataRow in MySqlHelper.ExecuteDataset(System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"], sql).Tables[0].Rows)
            {
                result.Add(new Nvp
                {
                    Id = dataRow["id"].ToString(),
                    Name = dataRow["name"].ToString()
                });
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }



    }
}