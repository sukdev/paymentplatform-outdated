﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Security;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{
    [RouteArea("ControlPanel")]
    public class HomeController : Controller
    {
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("*")]
        public ActionResult Index()
        {
            return View();
        }

        [ControlPanelRequireHttps]
        [Route("~/controlpanel/verify")]
        public ActionResult Verify()
        {
            return View(new LoginVerifyViewModel());
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [Route("~/controlpanel/verify")]
        public ActionResult Verify(LoginVerifyViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            // Validate!
            switch (ControlPanelAuthentication.Verify(model.VerificationCode))
            {
                case ControlPanelAuthentication.VerificationResult.OK:
                    return Redirect(string.IsNullOrWhiteSpace(Request.QueryString["returnUrl"]) ? "/" : Request.QueryString["returnUrl"]);
                case ControlPanelAuthentication.VerificationResult.Incorrect:
                    ModelState.AddModelError("", "Invalid verification code, please try again");
                    break;
                case ControlPanelAuthentication.VerificationResult.Blocked:                    
                    ModelState.AddModelError("", "The verification code has now been blocked, please start again");
                    break;
                case ControlPanelAuthentication.VerificationResult.NotAuthenticated:
                    ModelState.AddModelError("", "Unable to verify you at this time, an unknown error occurred, please start again");
                    break;                
            }
            return View(model);
        }

        [ControlPanelRequireHttps]
        [Route("~/controlpanel/login")]
        public ActionResult Login()
        {
            return View(
                new LoginViewModel
                {
                    Username = string.IsNullOrWhiteSpace(Request.QueryString["username"]) ? "" : Request.QueryString["username"]
                }
            );
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [Route("~/controlpanel/login")]
        public ActionResult Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var loginResult = ControlPanelAuthentication.Login(model.Username, model.Password);

            if (loginResult == Security.ControlPanelAuthentication.AuthenticationResult.OK)
                return Redirect(string.IsNullOrWhiteSpace(Request.QueryString["returnUrl"]) ? "/controlpanel/" : Request.QueryString["returnUrl"]);                

            if (loginResult == Security.ControlPanelAuthentication.AuthenticationResult.Incorrect)
                ModelState.AddModelError("", "Invalid username or password");

            if (loginResult == Security.ControlPanelAuthentication.AuthenticationResult.Blocked)
                ModelState.AddModelError("", "Your account has been suspended");


            if (loginResult == Security.ControlPanelAuthentication.AuthenticationResult.SmsFailed)
                ModelState.AddModelError("", "We were unable to send a text message to your phone; please contact an administrator");

            return View(model);
        }

        [ControlPanelRequireHttps]
        [Route("~/controlpanel/logout")]
        public ActionResult Logout()
        {
            ControlPanelAuthentication.Logout();
            return RedirectToAction("Login");
        }

        [ControlPanelRequireHttps]
        [Route("~/controlpanel/reset")]
        public ActionResult ResetRequest()
        {
            return View();
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [Route("~/controlpanel/reset/")]
        public ActionResult ResetRequest(ResetRequestViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            switch (ControlPanelAuthentication.RequestReset(model.Username))
            {
                case ControlPanelAuthentication.RequestResetResult.OK:
                    ViewBag.Success = true;
                    break;
                case ControlPanelAuthentication.RequestResetResult.Blocked:
                    ModelState.AddModelError("", "The account as been suspended by an administrator.");
                    break;
                case ControlPanelAuthentication.RequestResetResult.FailedInsert:
                    ModelState.AddModelError("", "We could not generate a password reset key, please try again.");
                    break;
                case ControlPanelAuthentication.RequestResetResult.FailedMail:
                    ModelState.AddModelError("", "We could not send the email containing your reset link, please try again.");
                    break;
                case ControlPanelAuthentication.RequestResetResult.Incorrect:
                    ModelState.AddModelError("", "No matching user could be found within the system, please try again.");
                    break;

            }
            return View(model);
        }

        [Route("~/controlpanel/reset/{id:guid}")]
        [ControlPanelRequireHttps]
        public ActionResult Reset(Guid id)
        {
            return View();
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [Route("~/controlpanel/reset/{id:guid}")]
        public ActionResult Reset(Guid id, ResetViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            switch (ControlPanelAuthentication.Reset(id, model.Username, model.NewPassword))
            {
                case ControlPanelAuthentication.ResetResult.OK:
                    ViewBag.Success = true;
                    break;
                case ControlPanelAuthentication.ResetResult.Blocked:
                    ModelState.AddModelError("", "The account as been suspended by an administrator.");
                    break;
                case ControlPanelAuthentication.ResetResult.Expired:
                    ModelState.AddModelError("", "The link you have used has expired, request a new password reset email.");
                    break;
                case ControlPanelAuthentication.ResetResult.Username:
                    ModelState.AddModelError("", "The link you have used is not for the username '" + model.Username + "'.");
                    break;
                case ControlPanelAuthentication.ResetResult.Failed:
                    ModelState.AddModelError("", "We could not update the account, please try again.");
                    break;

            }
            return View(model);
        }


        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("*")]
        [Route("~/controlpanel/stats")]
        public ActionResult Stats()
        {

            var sql = new System.Text.StringBuilder();

            sql.AppendLine("DROP TEMPORARY TABLE IF EXISTS reportingday;");
            sql.AppendLine("CREATE TEMPORARY TABLE reportingday ( day DATE );");

            var reportingDayTo = DateTime.ParseExact(Request["dateto"], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
            var reportingDayFrom = DateTime.ParseExact(Request["datefrom"], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);            

            while (reportingDayFrom <= reportingDayTo)
            {
                sql.AppendLine("INSERT INTO reportingday (day) VALUES ('" + reportingDayFrom.ToString("yyyy-MM-dd") + "');");
                reportingDayFrom = reportingDayFrom.AddDays(1);
            }


            var sqlGroupQuery = "";
            var sqlTotalQuery = "";
            
            switch (Request.QueryString["metric"].ToLowerInvariant().Trim())
            {
                case "payments-total":
                    sqlGroupQuery = "IFNULL((SELECT SUM(amount) FROM customertransaction WHERE subscriptionplanid = '{subscriptionPlanId}' AND DATE(submittedat) = reportingday.day AND status = 1),0) - IFNULL((SELECT SUM(amount) FROM customertransactionrefund WHERE subscriptionplanid = '{subscriptionPlanId}' AND DATE(submittedat) = reportingday.day AND status = 1),0) AS '{subscriptionPlanName}',";
                    sqlTotalQuery = "IFNULL((SELECT SUM(amount) FROM customertransaction WHERE DATE(submittedat) = reportingday.day AND status = 1),0) - IFNULL((SELECT SUM(amount) FROM customertransactionrefund WHERE DATE(submittedat) = reportingday.day AND status = 1),0) AS 'EUR'";
                    break;
                case "payments-in":
                    sqlGroupQuery = "IFNULL((SELECT SUM(amount) FROM customertransaction WHERE subscriptionplanid = '{subscriptionPlanId}' AND DATE(submittedat) = reportingday.day AND status = 1),0) AS '{subscriptionPlanName}',";
                    sqlTotalQuery = "IFNULL((SELECT SUM(amount) FROM customertransaction WHERE DATE(submittedat) = reportingday.day AND status = 1),0) AS 'EUR'";                    
                    break;
                case "payments-out":
                    sqlGroupQuery = "IFNULL((SELECT SUM(amount) FROM customertransactionrefund  WHERE subscriptionplanid = '{subscriptionPlanId}' AND DATE(submittedat) = reportingday.day AND status = 1),0) AS '{subscriptionPlanName}',";
                    sqlTotalQuery = "IFNULL((SELECT SUM(amount) FROM customertransactionrefund WHERE DATE(submittedat) = reportingday.day AND status = 1),0) AS 'EUR'";
                    break;
                case "subscriptions-total":                    
                    sqlGroupQuery = "IFNULL((SELECT COUNT(id) FROM customersubscription WHERE subscriptionplanid = '{subscriptionPlanId}' AND DATE(createdat) <= reportingday.day AND (DATE(expirydate) >= reportingday.day OR expirydate IS NULL) AND status = 1),0) AS '{subscriptionPlanName} Subscriptions',";
                    sqlTotalQuery = "IFNULL((SELECT COUNT(id) FROM customersubscription WHERE DATE(createdat) <= reportingday.day AND (DATE(expirydate) >= reportingday.day OR expirydate IS NULL) AND status = 1),0) AS 'Total Subscriptions'";                    
                    break;
                case "subscriptions-new":
                    sqlGroupQuery = "IFNULL((SELECT COUNT(id) FROM customersubscription WHERE subscriptionplanid = '{subscriptionPlanId}' AND DATE(createdat) = reportingday.day AND status = 1),0) AS 'New {subscriptionPlanName} Subscriptions',";
                    sqlTotalQuery = "IFNULL((SELECT COUNT(id) FROM customersubscription WHERE DATE(createdat) = reportingday.day AND status = 1),0) AS 'Total New Subscriptions'";
                    break;
                case "cancellations-total":
                    sqlGroupQuery = "IFNULL((SELECT COUNT(id) FROM customersubscription WHERE subscriptionplanid = '{subscriptionPlanId}' AND DATE(cancellationdate) = reportingday.day AND status = 1),0) AS 'Total {subscriptionPlanName} Cancellations', ";
                    sqlTotalQuery = "IFNULL((SELECT COUNT(id) FROM customersubscription WHERE DATE(cancellationdate) = reportingday.day AND status = 1),0) AS 'Total Cancellations'";
                    break;
                case "cancellations-manual":                    
                    sqlGroupQuery = "IFNULL((SELECT COUNT(id) FROM customersubscription WHERE subscriptionplanid = '{subscriptionPlanId}' AND DATE(cancellationdate) = reportingday.day AND status = 1 AND cancellationischurn = 0),0) AS 'Total {subscriptionPlanName} Cancellations',";
                    sqlTotalQuery = "IFNULL((SELECT COUNT(id) FROM customersubscription WHERE DATE(cancellationdate) = reportingday.day AND status = 1 AND cancellationischurn = 0),0) AS 'Manual Cancellations'";
                    break;
                case "cancellations-auto":
                    sqlGroupQuery = "IFNULL((SELECT COUNT(id) FROM customersubscription WHERE subscriptionplanid = '{subscriptionPlanId}' AND DATE(cancellationdate) = reportingday.day AND status = 1 AND cancellationischurn = 1),0) AS '{subscriptionPlanName} Failed Renewals',";
                    sqlTotalQuery = "IFNULL((SELECT COUNT(id) FROM customersubscription WHERE DATE(cancellationdate) = reportingday.day AND status = 1 AND cancellationischurn = 1),0) AS 'Total Failed Renewals'";
                    break;
                default:
                    throw new Exception("The metric '" + Request.QueryString["metric"].ToLowerInvariant().Trim() + " is not supported");
            }
            sql.AppendLine("SELECT reportingday.day AS 'Day',");

            foreach (DataRow dr in MySqlHelper.ExecuteDataset(System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],"SELECT id,name FROM subscriptionplan ORDER BY displayorder").Tables[0].Rows)
            {
                sql.AppendLine(
                    sqlGroupQuery.Replace("{subscriptionPlanId}", dr["id"].ToString())
                    .Replace("{subscriptionPlanName}", dr["name"].ToString().Replace("\'", "\'\'"))
                );
            }
            sql.AppendLine(sqlTotalQuery);
            sql.AppendLine("FROM reportingday;");
            sql.AppendLine("DROP TEMPORARY TABLE IF EXISTS reportingday;");

            var sourceTable = MySqlHelper.ExecuteDataset(
               System.Configuration.ConfigurationManager.AppSettings["MySqlConnection"],
                sql.ToString()
            ).Tables[0];


            var sb = new System.Text.StringBuilder();

            sb.AppendLine("{");
            sb.AppendLine("   \"cols\": ");
            sb.AppendLine("   [");
            foreach (DataColumn column in sourceTable.Columns)
            {
                var json = "";
                switch (column.DataType.ToString())
                {
                    case "System.String":
                        json = "{ \"type\": \"string\", \"label\": \"" + column.ColumnName + "\"}";
                        break;
                    case "System.DateTime":
                        json = "{ \"type\": \"datetime\", \"label\": \"" + column.ColumnName + "\"}";
                        break;
                    default:
                        json = "{ \"type\": \"number\", \"label\": \"" + column.ColumnName + "\"}";
                        break;
                }
                sb.AppendLine("    " + json + (sourceTable.Columns.IndexOf(column) +1 < sourceTable.Columns.Count ? "," : ""));
            }
            sb.AppendLine("   ],");
            sb.AppendLine("   \"rows\": ");
            
            sb.AppendLine("   [");
            foreach (DataRow row in sourceTable.Rows)
            {
                var jsonRow = "";
                foreach (DataColumn column in sourceTable.Columns)
                {
                    switch (column.DataType.ToString())
                    {
                        case "System.String":
                            jsonRow += "{ \"v\" : \"" + row[column.ColumnName].ToString().Replace("\"", "\\\"") + "\" }" + (sourceTable.Columns.IndexOf(column) + 1 < sourceTable.Columns.Count ? "," : "");
                            break;
                        case "System.DateTime":
                            jsonRow += "{ \"v\" : \"Date(" + ((DateTime)row[column.ColumnName]).Year + "," + (((DateTime)row[column.ColumnName]).Month - 1) + "," + ((DateTime)row[column.ColumnName]).Day + "," + ((DateTime)row[column.ColumnName]).Hour + "," + ((DateTime)row[column.ColumnName]).Minute + "," + +((DateTime)row[column.ColumnName]).Second + ")\" }" + (sourceTable.Columns.IndexOf(column) + 1 < sourceTable.Columns.Count ? "," : "");
                            break;
                        default:
                            jsonRow += "{ \"v\" : \"" + row[column.ColumnName].ToString().Replace("\"", "\\\"") + "\" }" + (sourceTable.Columns.IndexOf(column) + 1 < sourceTable.Columns.Count ? "," : "");
                            break;
                    }                    
                }
                jsonRow = "{ \"c\": [" + jsonRow + "]}";
                sb.AppendLine("    " + jsonRow + (sourceTable.Rows.IndexOf(row) + 1 < sourceTable.Rows.Count ? "," : ""));
            }
            sb.AppendLine("   ]");
            sb.AppendLine("}");

            return Content(sb.ToString(), "application/json");
        }

        [Route("~/controlpanel/denied/")]
        [ControlPanelRequireHttps]
        public ActionResult Denied()
        {
            return View();
        }
    }
}
