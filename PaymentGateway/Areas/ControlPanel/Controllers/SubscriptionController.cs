﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Services;
using NLog;
using System;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using StreamAMG.OTTPlatform.Core.Utilities;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{
    [RouteArea("ControlPanel")]
    public class SubscriptionController : Controller
    {

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/subscriptionplan/{subscriptionPlanId:Guid}/price/create")]
        public ActionResult PriceAdd(Guid subscriptionPlanId)
        {
            var searchRequest = new SubscriptionPlanService.GetSubscriptionPlanListRequest
            {
                PageIndex = 0,
                PageSize = 20
            };


            var getSubscriptionPlanPriceListResponse = SubscriptionPlanService.GetSubscriptionPlanPriceList(new SubscriptionPlanService.GetSubscriptionPlanPriceListRequest { PageIndex = 0, PageSize = int.MaxValue, SubscriptionPlanId = subscriptionPlanId });


            var effectiveFrom = DateTime.UtcNow.AddDays(7).AddHours(2);
            effectiveFrom = new DateTime(effectiveFrom.Year, effectiveFrom.Month, effectiveFrom.Day, effectiveFrom.Hour, 0, 0);

            var model = new SubscriptionPlanPriceViewModel
            {
                EffectiveFrom = effectiveFrom
            };

            if (getSubscriptionPlanPriceListResponse.ItemCount > 0)
            {
                model.Currency = getSubscriptionPlanPriceListResponse.Items[getSubscriptionPlanPriceListResponse.Items.Count - 1].Currency;
                model.Duration = getSubscriptionPlanPriceListResponse.Items[getSubscriptionPlanPriceListResponse.Items.Count - 1].Duration;
                model.Interval = getSubscriptionPlanPriceListResponse.Items[getSubscriptionPlanPriceListResponse.Items.Count - 1].Interval;
                model.TrialDuration = getSubscriptionPlanPriceListResponse.Items[getSubscriptionPlanPriceListResponse.Items.Count - 1].TrialDuration;
            }

            return View(model);
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/subscriptionplan/{subscriptionPlanId:Guid}/price/create")]
        public ActionResult PriceAdd(Guid subscriptionPlanId, SubscriptionPlanPriceViewModel model)
        {

            var subscriptionPlanPrice = LazyMapper.ExecuteObject<SubscriptionPlanPriceModel>(
              "SELECT * FROM subscriptionplanprice WHERE subscriptionplanid = ?subscriptionplanid AND currency = ?currency AND effectivefrom <= ?effective ORDER BY effectivefrom DESC LIMIT 1;",
              new MySqlParameter("subscriptionplanid", subscriptionPlanId),
              new MySqlParameter("effective", DateTime.UtcNow),
              new MySqlParameter("currency", model.Currency)
           );


            if (!ModelState.IsValid)
                return View(model);

            // Enforce no proce increases for seven days
            if (subscriptionPlanPrice != null && model.EffectiveFrom < DateTime.UtcNow.AddDays(7) && subscriptionPlanPrice.Amount < model.Amount)
                ModelState.AddModelError("EffectiveFrom", "The date on which the changes take affect must be at least seven days in the future if the price is being increased.");

            if (subscriptionPlanPrice != null && model.EffectiveFrom < DateTime.UtcNow.AddDays(7) && subscriptionPlanPrice.Interval != model.Interval)
                ModelState.AddModelError("Interval", "The date on which the changes take affect must be at least seven days in the future if the billing interval is being changed.");

            if (model.Amount < 1)
                ModelState.AddModelError("Amount", "The price must be at least 1.00" + Utility.Currency.GetSymbol(model.Currency) + ".");


            if (!ModelState.IsValid)
                return View(model);
            try
            {
                var subscriptionPlanPriceSetResponse = SubscriptionPlanService.SetPrice(new SubscriptionPlanService.SetPriceRequest
                {
                    SubscriptionPlanPriceId = Guid.Empty,
                    SubscriptionPlanPrice = new SubscriptionPlanPriceModel
                    {
                        Amount = model.Amount,
                        Currency = model.Currency,
                        Duration = model.Duration,
                        EffectiveFrom = model.EffectiveFrom,
                        Interval = model.Interval,
                        SubscriptionPlanId = subscriptionPlanId,
                        TrialDuration = model.TrialDuration
                    }
                }
                );

                return RedirectToAction("View", new { subscriptionPlanId = subscriptionPlanId, returnurl = Request["returnurl"] });
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to create a the new price. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }
            return View(model);
        }


        #region Index

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/subscriptionplans")]
        public ActionResult Index()
        {
            var searchRequest = new SubscriptionPlanService.GetSubscriptionPlanListRequest
            {
                PageIndex = 0,
                PageSize = 20
            };

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pageindex"]))
                searchRequest.PageIndex = Convert.ToInt32(Request.QueryString["pageindex"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pagesize"]))
                searchRequest.PageSize = Convert.ToInt32(Request.QueryString["pagesize"]);

            var searchResult = SubscriptionPlanService.GetSubscriptionPlanList(searchRequest);

            return View(searchResult);
        }

        #endregion

        #region View

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/subscriptionplan/{subscriptionPlanId:Guid}")]
        public ActionResult view(Guid subscriptionPlanId)
        {
            var subscriptionPlanGetResponse = SubscriptionPlanService.Get(new SubscriptionPlanService.GetRequest { SubscriptionPlanId = subscriptionPlanId });

            var model = new SubscriptionPlanViewModel
            {
                CheckoutBody = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutBody,
                CheckoutBodyWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutBodyWithFreeTrial,
                CheckoutStyle = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutStyle,
                CheckoutStyleWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutStyleWithFreeTrial,
                CheckoutTitle = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutTitle,
                CheckoutTitleWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutTitleWithFreeTrial,
                Deleted = subscriptionPlanGetResponse.SubscriptionPlan.Deleted,
                DisplayOrder = subscriptionPlanGetResponse.SubscriptionPlan.DisplayOrder,
                Entitlements = subscriptionPlanGetResponse.SubscriptionPlan.Entitlements,
                ListingBody = subscriptionPlanGetResponse.SubscriptionPlan.ListingBody,
                ListingBodyWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.ListingBodyWithFreeTrial,
                ListingStyle = subscriptionPlanGetResponse.SubscriptionPlan.ListingStyle,
                ListingStyleWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.ListingStyleWithFreeTrial,
                ListingTitle = subscriptionPlanGetResponse.SubscriptionPlan.ListingTitle,
                ListingTitleWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.ListingTitleWithFreeTrial,
                Name = subscriptionPlanGetResponse.SubscriptionPlan.Name,
                StatementDescription = subscriptionPlanGetResponse.SubscriptionPlan.StatementDescription
            };
            return View(model);
        }

        #endregion

        #region Edit 

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/subscriptionplan/{subscriptionPlanId:Guid}/edit")]
        public ActionResult Edit(Guid subscriptionPlanId)
        {
            var subscriptionPlanGetResponse = SubscriptionPlanService.Get(new SubscriptionPlanService.GetRequest { SubscriptionPlanId = subscriptionPlanId });

            var model = new SubscriptionPlanViewModel
            {
                CheckoutBody = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutBody,
                CheckoutBodyWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutBodyWithFreeTrial,
                CheckoutStyle = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutStyle,
                CheckoutStyleWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutStyleWithFreeTrial,
                CheckoutTitle = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutTitle,
                CheckoutTitleWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutTitleWithFreeTrial,
                Deleted = subscriptionPlanGetResponse.SubscriptionPlan.Deleted,
                DisplayOrder = subscriptionPlanGetResponse.SubscriptionPlan.DisplayOrder,
                Entitlements = subscriptionPlanGetResponse.SubscriptionPlan.Entitlements,
                ListingBody = subscriptionPlanGetResponse.SubscriptionPlan.ListingBody,
                ListingBodyWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.ListingBodyWithFreeTrial,
                ListingStyle = subscriptionPlanGetResponse.SubscriptionPlan.ListingStyle,
                ListingStyleWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.ListingStyleWithFreeTrial,
                ListingTitle = subscriptionPlanGetResponse.SubscriptionPlan.ListingTitle,
                ListingTitleWithFreeTrial = subscriptionPlanGetResponse.SubscriptionPlan.ListingTitleWithFreeTrial,
                Name = subscriptionPlanGetResponse.SubscriptionPlan.Name,
                StatementDescription = subscriptionPlanGetResponse.SubscriptionPlan.StatementDescription
            };

            return View(model);
        }

        [HttpPost ValidateInput(false)]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/subscriptionplan/{subscriptionPlanId:Guid}/edit")]
        public ActionResult Edit(Guid subscriptionPlanId, SubscriptionPlanViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var subscriptionPlanSetResponse = SubscriptionPlanService.Set(new SubscriptionPlanService.SetRequest
                {
                    SubscriptionPlanId = subscriptionPlanId,
                    SubscriptionPlan = new SubscriptionPlanModel
                    {
                        CheckoutBody = model.CheckoutBody,
                        CheckoutBodyWithFreeTrial = model.CheckoutBodyWithFreeTrial,
                        CheckoutStyle = model.CheckoutStyle,
                        CheckoutStyleWithFreeTrial = model.CheckoutStyleWithFreeTrial,
                        CheckoutTitle = model.CheckoutTitle,
                        CheckoutTitleWithFreeTrial = model.CheckoutTitleWithFreeTrial,
                        Deleted = model.Deleted,
                        DisplayOrder = model.DisplayOrder,
                        Entitlements = model.Entitlements,
                        ListingBody = model.ListingBody,
                        ListingBodyWithFreeTrial = model.ListingBodyWithFreeTrial,
                        ListingStyle = model.ListingStyle,
                        ListingStyleWithFreeTrial = model.ListingStyleWithFreeTrial,
                        ListingTitle = model.ListingTitle,
                        ListingTitleWithFreeTrial = model.ListingTitleWithFreeTrial,
                        Name = model.Name,
                        StatementDescription = model.StatementDescription
                    }
                });
                ViewBag.Success = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to save Subscription Plan. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }

        #endregion

        #region Create 

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/subscriptionplan/create")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost ValidateInput(false)]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/subscriptionplan/create")]
        public ActionResult Create(SubscriptionPlanViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var subscriptionPlanSetResponse = SubscriptionPlanService.Add(new SubscriptionPlanService.AddRequest
                {                    
                    SubscriptionPlan = new SubscriptionPlanModel
                    {
                        CheckoutBody = model.CheckoutBody,
                        CheckoutBodyWithFreeTrial = model.CheckoutBodyWithFreeTrial,
                        CheckoutStyle = model.CheckoutStyle,
                        CheckoutStyleWithFreeTrial = model.CheckoutStyleWithFreeTrial,
                        CheckoutTitle = model.CheckoutTitle,
                        CheckoutTitleWithFreeTrial = model.CheckoutTitleWithFreeTrial,
                        Deleted = model.Deleted,
                        DisplayOrder = model.DisplayOrder,
                        Entitlements = model.Entitlements,
                        ListingBody = model.ListingBody,
                        ListingBodyWithFreeTrial = model.ListingBodyWithFreeTrial,
                        ListingStyle = model.ListingStyle,
                        ListingStyleWithFreeTrial = model.ListingStyleWithFreeTrial,
                        ListingTitle = model.ListingTitle,
                        ListingTitleWithFreeTrial = model.ListingTitleWithFreeTrial,
                        Name = model.Name,
                        StatementDescription = model.StatementDescription
                    }
                });

                return RedirectToAction("View", new { subscriptionPlanId = subscriptionPlanSetResponse.SubscriptionPlan.Id, returnUrl = Request.QueryString["returnurl"] });
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to create Subscription Plan. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            
                
            return View(model);
        }

        #endregion

        #region Translate

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/subscriptionplan/{subscriptionPlanId:Guid}/translate/{language}/")]
        public ActionResult EditTranslation(Guid subscriptionPlanId, string language)
        {
            var subscriptionPlanGetResponse = SubscriptionPlanService.Get(new SubscriptionPlanService.GetRequest { SubscriptionPlanId = subscriptionPlanId });
            var subscriptionPlanGetTransaltionResponse = SubscriptionPlanService.GetTranslation(new SubscriptionPlanService.GetTranslationRequest { SubscriptionPlanId = subscriptionPlanId, Language = language });

            var model = new SubscriptionPlanTranslationViewModel
            {
                CheckoutBody = subscriptionPlanGetTransaltionResponse.SubscriptionPlanTranslation.CheckoutBody,
                CheckoutBodyOriginal = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutBody,
                CheckoutBodyWithFreeTrial = subscriptionPlanGetTransaltionResponse.SubscriptionPlanTranslation.CheckoutBodyWithFreeTrial,
                CheckoutBodyWithFreeTrialOriginal = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutBodyWithFreeTrial,
                CheckoutStyleOriginal = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutStyle,
                CheckoutStyleWithFreeTrialOriginal = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutStyleWithFreeTrial,
                CheckoutTitle = subscriptionPlanGetTransaltionResponse.SubscriptionPlanTranslation.CheckoutTitle,
                CheckoutTitleOriginal = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutTitle,
                CheckoutTitleWithFreeTrial = subscriptionPlanGetTransaltionResponse.SubscriptionPlanTranslation.CheckoutTitleWithFreeTrial,
                CheckoutTitleWithFreeTrialOriginal = subscriptionPlanGetResponse.SubscriptionPlan.CheckoutTitleWithFreeTrial,
                DeletedOriginal = subscriptionPlanGetResponse.SubscriptionPlan.Deleted,
                DisplayOrderOriginal = subscriptionPlanGetResponse.SubscriptionPlan.DisplayOrder,
                EntitlementsOriginal = subscriptionPlanGetResponse.SubscriptionPlan.Entitlements,
                ListingBody = subscriptionPlanGetTransaltionResponse.SubscriptionPlanTranslation.ListingBody,
                ListingBodyOriginal = subscriptionPlanGetResponse.SubscriptionPlan.ListingBody,
                ListingBodyWithFreeTrial = subscriptionPlanGetTransaltionResponse.SubscriptionPlanTranslation.ListingBodyWithFreeTrial,
                ListingBodyWithFreeTrialOriginal = subscriptionPlanGetResponse.SubscriptionPlan.ListingBodyWithFreeTrial,
                ListingStyleOriginal = subscriptionPlanGetResponse.SubscriptionPlan.ListingStyle,
                ListingStyleWithFreeTrialOriginal = subscriptionPlanGetResponse.SubscriptionPlan.ListingStyleWithFreeTrial,
                ListingTitle = subscriptionPlanGetTransaltionResponse.SubscriptionPlanTranslation.ListingTitle,
                ListingTitleOriginal = subscriptionPlanGetResponse.SubscriptionPlan.ListingTitle,
                ListingTitleWithFreeTrial = subscriptionPlanGetTransaltionResponse.SubscriptionPlanTranslation.ListingTitleWithFreeTrial,
                ListingTitleWithFreeTrialOriginal = subscriptionPlanGetResponse.SubscriptionPlan.ListingTitleWithFreeTrial,
                Name = subscriptionPlanGetTransaltionResponse.SubscriptionPlanTranslation.Name,
                NameOriginal = subscriptionPlanGetResponse.SubscriptionPlan.Name,
                StatementDescription = subscriptionPlanGetTransaltionResponse.SubscriptionPlanTranslation.StatementDescription,
                StatementDescriptionOriginal = subscriptionPlanGetResponse.SubscriptionPlan.StatementDescription
            };

            return View(model);
        }


        [HttpPost ValidateInput(false)]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/subscriptionplan/{subscriptionPlanId:Guid}/translate/{language}/")]
        public ActionResult EditTranslation(Guid subscriptionPlanId, string language, SubscriptionPlanTranslationViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var subscriptionPlanSetTranslationResponse = SubscriptionPlanService.SetTranslation(new SubscriptionPlanService.SetTranslationRequest
                {
                    SubscriptionPlanId = subscriptionPlanId,
                    Language = language,
                    SubscriptionPlanTranslation = new SubscriptionPlanTranslationModel
                    {
                        CheckoutBody = model.CheckoutBody,
                        CheckoutTitle = model.CheckoutTitle,
                        CheckoutBodyWithFreeTrial = model.CheckoutBodyWithFreeTrial,
                        CheckoutTitleWithFreeTrial = model.CheckoutTitleWithFreeTrial,
                        ListingBody = model.ListingBody,
                        ListingBodyWithFreeTrial = model.ListingBodyWithFreeTrial,
                        ListingTitle = model.ListingTitle,
                        ListingTitleWithFreeTrial = model.ListingTitleWithFreeTrial,
                        Name = model.Name,
                        StatementDescription = model.StatementDescription
                    }
                });
                ViewBag.Success = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to save Subscription Plan. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }

        #endregion


    }
}