﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Core.Services;
using System;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{
    [RouteArea("ControlPanel")]
    public class TransactionController : Controller
    {

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/transaction")]
        public ActionResult Index()
        {
            var exportToCsv = !string.IsNullOrWhiteSpace(Request.QueryString["export"]);
            var searchRequest = new CustomerTransactionService.TransationSearchRequest
            {
                PageIndex = 0,
                PageSize = exportToCsv ? int.MaxValue : 20
            };

            searchRequest.DateTo = DateTime.UtcNow;
            searchRequest.DateFrom = DateTime.UtcNow.AddMonths(-1);

            if (!string.IsNullOrWhiteSpace(Request["dateto"]))
                searchRequest.DateTo = DateTime.ParseExact(Request["dateto"], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (!string.IsNullOrWhiteSpace(Request["datefrom"]))
                searchRequest.DateFrom = DateTime.ParseExact(Request["datefrom"], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);



            if (!string.IsNullOrWhiteSpace(Request["status"]))
                searchRequest.TransactionStatus = (CustomerTransactionService.TransationSearchRequest.TransactionStatuses)Enum.Parse(typeof(CustomerTransactionService.TransationSearchRequest.TransactionStatuses), Request["status"], true);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pageindex"]))
                searchRequest.PageIndex = Convert.ToInt32(Request.QueryString["pageindex"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pagesize"]))
                searchRequest.PageSize = Convert.ToInt32(Request.QueryString["pagesize"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["subscriptionplanid"]) && Regex.IsMatch(Request.QueryString["subscriptionplanid"], @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"))
                searchRequest.SubscriptionPlanId = new Guid(Request.QueryString["subscriptionplanid"]);

            var searchResult = CustomerTransactionService.TransactionSearch(searchRequest);

            if (!exportToCsv)
                return View(searchResult);

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=payments.csv");
            Response.ContentType = "text/csv";
            Response.Write(Utility.Helpers.CreateCSVAndAppendCustomerFieldsFromGenericList(searchResult.Items, "CustomerId"));                
            Response.End();
            return null;
        }

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/transaction/{customerTransactionId}")]
        public ActionResult View(Guid customerTransactionId)
        {

            var getTransactionResult = CustomerTransactionService.Get(new CustomerTransactionService.GetTransactionRequest { TransactionId = customerTransactionId });
            var getSubscriptionPlanResult = SubscriptionPlanService.Get(new SubscriptionPlanService.GetRequest {  SubscriptionPlanId = getTransactionResult.Transaction.SubscriptionPlanId });

            var result = new TransactionViewViewModel
            {
                Amount = getTransactionResult.Transaction.Amount,
                Batch = getTransactionResult.Transaction.Batch,
                Country = getTransactionResult.Transaction.Country,
                CreatedAt = getTransactionResult.Transaction.CreatedAt,
                Currency = getTransactionResult.Transaction.Currency,
                Description = getTransactionResult.Transaction.Description,
                Provider = getTransactionResult.Transaction.Provider,
                ProviderId = getTransactionResult.Transaction.ProviderId,
                ProviderMessage = getTransactionResult.Transaction.ProviderMessage,
                Reference = getTransactionResult.Transaction.Reference,
                RenewalFrom = getTransactionResult.Transaction.RenewalFrom,
                RenewalTo = getTransactionResult.Transaction.RenewalTo,
                StatementDescription = getTransactionResult.Transaction.StatementDescription,
                Status = getTransactionResult.Transaction.Status,
                SubscriptionPlanId = getTransactionResult.Transaction.SubscriptionPlanId,
                SubscriptionPlanName = getSubscriptionPlanResult.SubscriptionPlan.Name,
                UpdatedAt = getTransactionResult.Transaction.UpdatedAt
            };



            return View(result);
        }


        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/transaction/{customerTransactionId}/refund")]
        public ActionResult IssueRefund(Guid customerTransactionId)
        {
            var customerTransactionGetResponse = CustomerTransactionService.Get(new CustomerTransactionService.GetTransactionRequest
            {
                TransactionId = customerTransactionId
            });

            var model = new TransacrionIssueRefundViewModel
            {
                TransactionAmount = customerTransactionGetResponse.Transaction.Amount,
                TransactionAmountRefunded = customerTransactionGetResponse.Transaction.AmountRefunded,
                TransactionCurrency = customerTransactionGetResponse.Transaction.Currency
            };

            return View(model);
        }

        [HttpPost]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/transaction/{customerTransactionId}/refund")]
        public ActionResult IssueRefund(Guid customerTransactionId, TransacrionIssueRefundViewModel model)
        {
            var customerTransactionGetResponse = CustomerTransactionService.Get(new CustomerTransactionService.GetTransactionRequest
            {
                TransactionId = customerTransactionId
            });

            model.TransactionAmount = customerTransactionGetResponse.Transaction.Amount;
            model.TransactionAmountRefunded = customerTransactionGetResponse.Transaction.AmountRefunded;
            model.TransactionCurrency = customerTransactionGetResponse.Transaction.Currency;

            if (model.RefundAmount > (model.TransactionAmount - model.TransactionAmountRefunded))
                ModelState.AddModelError("RefundAmount", "The maximum amount that can be refunded is " + (model.TransactionAmount - model.TransactionAmountRefunded).ToString("0.00"));

            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var issueTransactionRefundResponse = CustomerTransactionRefundService.IssueTransactionRefund(new CustomerTransactionRefundService.IssueTransactionRefundRequest { CustomerTransactionId = customerTransactionId, Amount = model.RefundAmount, Reason = model.RefundReason });
                ViewBag.Success = issueTransactionRefundResponse.TransactionRefund.Status == 1;
                ViewBag.ErrorMessage = issueTransactionRefundResponse.TransactionRefund.ProviderMessage;
            }
            catch (Exception ex)
            {
                ViewBag.Success = false;
                ViewBag.ErrorMessage = ex.Message;
            }

            return View(model);
        }
    }
}