﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Services;
using NLog;
using System;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{
    [RouteArea("ControlPanel")]
    public class MailTemplateController : Controller
    {
        #region Index

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/mailtemplate")]
        public ActionResult Index()
        {
            var searchRequest = new MailService.GetMailTemplateListRequest
            {
                PageIndex = 0,
                PageSize = 20
            };

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pageindex"]))
                searchRequest.PageIndex = Convert.ToInt32(Request.QueryString["pageindex"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pagesize"]))
                searchRequest.PageSize = Convert.ToInt32(Request.QueryString["pagesize"]);

            var searchResult = MailService.GetMailTemplateList(searchRequest);
            return View(searchResult);
        }

        #endregion

        #region Edit
        
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/mailtemplate/{mailTemplateId}")]
        public ActionResult Edit(string mailTemplateId)
        {

            
            var getMailtemplateRepsonse = MailService.GetMailtemplate(new MailService.GetMailTemplateRequest { 
                 MailTemplateId = mailTemplateId
            });

            var model = new MailTemplateEditViewModel
            {
                Subject = getMailtemplateRepsonse.MailTemplate.Subject,
                Body = getMailtemplateRepsonse.MailTemplate.Body
            };

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/mailtemplate/{mailTemplateId}")]
        public ActionResult Edit(string mailTemplateId, MailTemplateEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var setCustomerProfileResponse = MailService.SetMailtemplate(new MailService.SetMailTemplateRequest
                {
                    MailTemplateId = mailTemplateId,
                    MailTemplate = new MailTemplateModel
                    {
                        Subject = model.Subject,
                        Body = model.Body                        
                    }
                });
                ViewBag.Success = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to save Mail Template. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }

        #endregion


        #region Edit

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/mailtemplate/{mailTemplateId}/translate/{language}/")]
        public ActionResult EditTransaltion(string mailTemplateId, string language)
        {

            var mailTemplatePlanGetResponse = MailService.GetMailtemplate(
                new MailService.GetMailTemplateRequest {
                    MailTemplateId = mailTemplateId
                }
            );
            var mailTemplatePlanTransaltionGetResponse = MailService.GetMailTemplateTransaltion(
                new MailService.GetMailTemplateTranslationRequest {
                    MailTemplateId = mailTemplateId,
                    Language = language
                }
            );
       
            var model = new MailTemplateEditTranslationViewModel
            {
                SubjectOriginal = mailTemplatePlanGetResponse.MailTemplate.Subject,
                Subject = mailTemplatePlanTransaltionGetResponse.MailTemplateTranslation.Subject,
                BodyOriginal = mailTemplatePlanGetResponse.MailTemplate.Body,
                Body = mailTemplatePlanTransaltionGetResponse.MailTemplateTranslation.Body
            };

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/mailtemplate/{mailTemplateId}/translate/{language}/")]        
        public ActionResult EditTransaltion(string mailTemplateId, string language, MailTemplateEditTranslationViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var setMailTemplateTranslationResponse = MailService.SetMailTemplateTranslation(new MailService.SetMailTemplateTranslationRequest
                {
                    MailTemplateId = mailTemplateId,
                    Language = language,
                     MailTemplateTranslation = new MailTemplateTranslationModel
                     {
                        Subject = model.Subject,
                        Body = model.Body
                    }
                });
                ViewBag.Success = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to save Mail Template Translation. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }

        #endregion
    }
}