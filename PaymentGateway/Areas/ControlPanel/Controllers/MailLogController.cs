﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Core.Services;
using System;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{
    [RouteArea("ControlPanel")]
    public class MailLogController : Controller
    {

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/maillog")]
        public ActionResult Index()
        {

            var ExportToCsv = !string.IsNullOrWhiteSpace(Request.QueryString["export"]);

            var searchRequest = new MailService.GetMailLogListRequest
            {
                PageIndex = 0,
                PageSize = ExportToCsv ? int.MaxValue : 20
            };

            searchRequest.DateTo = DateTime.UtcNow;
            searchRequest.DateFrom = DateTime.UtcNow.AddMonths(-1);

            if (!string.IsNullOrWhiteSpace(Request["dateto"]))
                searchRequest.DateTo = DateTime.ParseExact(Request["dateto"], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);

            if (!string.IsNullOrWhiteSpace(Request["datefrom"]))
                searchRequest.DateFrom = DateTime.ParseExact(Request["datefrom"], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["mailtemplateid"]) )
                searchRequest.MailTemplateId = Request.QueryString["mailtemplateid"];

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pageindex"]))
                searchRequest.PageIndex = Convert.ToInt32(Request.QueryString["pageindex"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pagesize"]))
                searchRequest.PageSize = Convert.ToInt32(Request.QueryString["pagesize"]);


            if (!string.IsNullOrWhiteSpace(Request["status"]))
                searchRequest.MailLogStatus = (MailService.GetMailLogListRequest.MailLogStatuses)Enum.Parse(typeof(MailService.GetMailLogListRequest.MailLogStatuses), Request["status"], true);

            var searchResult = MailService.GetMailLogList(searchRequest);

            if (ExportToCsv)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=emaillogs.csv");
                Response.ContentType = "text/csv";
                Response.Write(Utility.Helpers.CreateCSVFromGenericList(searchResult.Items));
                Response.End();
            }
            return View(searchResult);
        }

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/maillog/view/{id:guid}")]
        public ActionResult View(Guid id)
        {

            var getMailLogResponse = MailService.GetMailLog(new MailService.GetMailLogRequest { MailLogId = id });

            var model = new Models.CustomerViewMailLogViewModel
            {
                Body = getMailLogResponse.MailLog.Body,
                CreatedAt = getMailLogResponse.MailLog.CreatedAt,
                MailTo = getMailLogResponse.MailLog.MailTo,
                Status = getMailLogResponse.MailLog.Status,
                Subject = getMailLogResponse.MailLog.Subject,

            };

            return View(model);
        }

    }
}