﻿
using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Core.Services;
using System;
using System.Web.Mvc;
using NLog;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Core.Models;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{
    [RouteArea("ControlPanel")]
    public class UserController : Controller
    {
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/user")]
        public ActionResult Index()
        {
            var searchRequest = new UserService.GetUserListRequest
            {
                PageIndex = 0,
                PageSize = 20
            };

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pageindex"]))
                searchRequest.PageIndex = Convert.ToInt32(Request.QueryString["pageindex"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pagesize"]))
                searchRequest.PageSize = Convert.ToInt32(Request.QueryString["pagesize"]);

            var searchResult = UserService.GetUserList(searchRequest);

            return View(searchResult);
        }


        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/user/{userId}/edit")]
        public ActionResult Edit(Guid userId)
        {
            

            var getUserResponse = UserService.Get(new UserService.GetRequest {UserId = userId});


            var model = new UserEditViewModel
            {
                Name = getUserResponse.User.Name,
                Surname = getUserResponse.User.Surname,
                MobilePhone = getUserResponse.User.MobilePhone,
                EmailAddress = getUserResponse.User.EmailAddress,
                Username = getUserResponse.User.Username,
                SystemRoleIds = getUserResponse.User.SystemRoleIds
            };
            
            return View(model);
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/user/{userId}/edit")]
        public ActionResult Edit(Guid userId, UserEditViewModel model)
        {

            if (!ModelState.IsValid)
                return View(model);

            var getUserNameIsUniqueResponse = UserService.GetIsUsernameUnique(new UserService.GetIsUsernameUniqueRequest {
                Username = model.Username,
                UserId = userId
            });

            if(!getUserNameIsUniqueResponse.IsUnique)
                ModelState.AddModelError("username","The username is already in use.");

            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var setUserResponse = UserService.Set(new UserService.SetRequest
                {
                    UserId = userId,
                    User = new UserModel
                    {
                        Name = model.Name,
                        Surname = model.Surname,
                        MobilePhone = model.MobilePhone,
                        EmailAddress = model.EmailAddress,
                        Username = model.Username,
                        SystemRoleIds = model.SystemRoleIds
                       
                    }
                });

                ViewBag.Success = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to save a new user. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }



        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/user/create")]
        public ActionResult Create()
        {
            var model = new UserEditViewModel();
            

            return View(model);
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/user/create")]
        public ActionResult Create(UserEditViewModel model)
        {
            if(!ModelState.IsValid)
                return View(model);

            var getUserNameIsUniqueResponse = UserService.GetIsUsernameUnique(new UserService.GetIsUsernameUniqueRequest
            {
                Username = model.Username,
                UserId = Guid.Empty
            });

            if (!getUserNameIsUniqueResponse.IsUnique)
                ModelState.AddModelError("username", "The username is already in use.");

            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var setUserResponse = UserService.Set(new UserService.SetRequest
                {
                    UserId = Guid.Empty,
                    User = new UserModel
                    {
                        Name = model.Name,
                        Surname = model.Surname,
                        MobilePhone = model.MobilePhone,
                        EmailAddress = model.EmailAddress,
                        Username = model.Username,
                        SystemRoleIds = model.SystemRoleIds
                    }
                });

                ViewBag.Success = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to create a new user. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            if(ViewBag.Success == true)
                return Redirect(Request["returnurl"]);

            return View(model);
        }
    }
}