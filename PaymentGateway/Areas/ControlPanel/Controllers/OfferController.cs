﻿using NLog;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Core.Models;
using StreamAMG.OTTPlatform.Core.Services;
using System;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{
    [RouteArea("ControlPanel")]
    public class OfferController : Controller
    {
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/offer")]
        public ActionResult Index()
        {
            var searchRequest = new OfferService.GetOfferListRequest
            {
                PageIndex = 0,
                PageSize = 20
            };

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pageindex"]))
                searchRequest.PageIndex = Convert.ToInt32(Request.QueryString["pageindex"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pagesize"]))
                searchRequest.PageSize = Convert.ToInt32(Request.QueryString["pagesize"]);

            var searchResult = OfferService.GetOfferList(searchRequest);

            return View(searchResult);
        }


        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/offer/{offerId}")]
        public ActionResult View(Guid offerId)
        {
            var ExportToCsv = !string.IsNullOrWhiteSpace(Request.QueryString["export"]);
            var getOfferResult = OfferService.Get(new OfferService.GetRequest { OfferId = offerId });
            var model = new OfferViewModel
            {
                AppliesFor = getOfferResult.Offer.AppliesFor,
                Discount = getOfferResult.Offer.Discount,
                Name = getOfferResult.Offer.Name

            };

            if (ExportToCsv)
            {
                var getOfferListRequest = new OfferVoucherService.GetOfferVoucherListRequest { PageIndex = 0, PageSize = int.MaxValue, OfferId = offerId };

                if (!string.IsNullOrWhiteSpace(Request.QueryString["pageindex"]))
                {
                    getOfferListRequest.PageIndex = Convert.ToInt32(Request.QueryString["pageindex"]);
                }

                var getOfferListResult = OfferVoucherService.GetOfferVoucherList(getOfferListRequest);

                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=vouchers.csv");
                Response.ContentType = "text/csv";
                Response.Write(Utility.Helpers.CreateCSVFromGenericList(getOfferListResult.Items));
                Response.End();
            }

            return View(model);
        }



        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/offer/{offerId}/edit")]
        public ActionResult Edit(Guid offerId)
        {
            var getOfferResult = OfferService.Get(new OfferService.GetRequest { OfferId = offerId });
            var model = new OfferEditViewModel
            {
                AppliesFor = getOfferResult.Offer.AppliesFor,                
                AppliesToSubscriptionPlanId = getOfferResult.Offer.SubscriptionPlanId,
                Discount = getOfferResult.Offer.Discount,
                Name = getOfferResult.Offer.Name
            };
            return View(model);
        }

        [HttpPost ValidateInput(false)]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/offer/{offerId}/edit")]
        public ActionResult Edit(Guid offerId, OfferEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var setOfferResult = OfferService.Set(
                    new OfferService.SetRequest
                    {
                        OfferId = offerId,
                        Offer = new OfferModel
                        {
                            AppliesFor = model.AppliesFor,
                            Discount = model.Discount,
                            Name = model.Name,
                            SubscriptionPlanId = model.AppliesToSubscriptionPlanId
                        }
                    }
                );

                ViewBag.Success = true;

            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to save changes to the new offer '" + offerId + "'. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }


            return View(model);
        }


        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/offer/create")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost ValidateInput(false)]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/offer/create")]
        public ActionResult Create(OfferCreateViewModel model)
        {
            if(!ModelState.IsValid)
                return View(model);

            try
            {
                var offerSetResponse = OfferService.Set(new OfferService.SetRequest
                {
                    OfferId = Guid.Empty,
                    Offer = new OfferModel
                    {
                        AppliesFor = model.AppliesFor,
                        Discount = model.Discount,
                        Name = model.Name,
                        SubscriptionPlanId = model.AppliesToSubscriptionPlanId
                    }
                }
                );

                return RedirectToAction("View", new { offerId = offerSetResponse.Offer.Id });
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to create a new offer. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }
            return View(model);
        }

       
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/offer/{offerId}/issue")]
        public ActionResult Issue(Guid offerId)
        {
            return View();
        }

        [HttpPost ValidateInput(false)]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/offer/{offerId}/issue")]
        public ActionResult Issue(Guid offerId, OfferIssueViewModel model)
        {            
            if(!ModelState.IsValid)
                return View(model);


            try
            {
                OfferVoucherService.CreateOfferVoucher(
                    new OfferVoucherService.CreateOfferVoucherRequest
                    {
                        OfferId = offerId,
                        Campaign = model.Campaign,
                        QuantityToCreate = model.Quantity,
                        ValidFrom = DateTime.UtcNow,
                        ValidTo = DateTime.UtcNow.AddMonths(12)
                    }
                );
                return RedirectToAction("View", new { offerId = offerId });
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to create a new offer. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }

    }
}