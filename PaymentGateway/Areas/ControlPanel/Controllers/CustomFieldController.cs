﻿using NLog;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Areas.ControlPanel.Models;
using StreamAMG.OTTPlatform.Core.Services;
using System;
using System.Linq;
using System.Web.Mvc;
using StreamAMG.OTTPlatform.Core.Models;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{

    [RouteArea("ControlPanel")]
    public class CustomFieldController : Controller
    {

        #region Index

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customfield")]
        public ActionResult Index()
        {
            var searchRequest = new CustomFieldService.GetCustomFieldListRequest
            {
                PageIndex = 0,
                PageSize = 20
            };

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pageindex"]))
                searchRequest.PageIndex = Convert.ToInt32(Request.QueryString["pageindex"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pagesize"]))
                searchRequest.PageSize = Convert.ToInt32(Request.QueryString["pagesize"]);

            var searchResult = CustomFieldService.GetCustomFieldList(searchRequest);
            return View(searchResult);
        }

        #endregion

        #region View

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customfield/{customFieldId:guid}/view")]
        public ActionResult View(Guid customFieldId)
        {
            return View();
        }

        #endregion

        #region Edit

        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customfield/{customFieldId:guid}/edit")]
        public ActionResult Edit(Guid customFieldId)
        {
            var getCustomFieldResponse = CustomFieldService.GetCustomField(new CustomFieldService.GetCustomFieldRequest { CustomFieldId = customFieldId });

            var model = new CustomFieldEditViewModel
            {
                DisplayOrder = getCustomFieldResponse.CustomField.DisplayOrder,
                Label = getCustomFieldResponse.CustomField.Label,
                Required = getCustomFieldResponse.CustomField.Required,
                RequiredMessage = getCustomFieldResponse.CustomField.RequiredMessage
            };

            foreach (var option in getCustomFieldResponse.CustomField.Options)
            {
                model.Options.Add( new CustomFieldEditViewModel.Option
                {
                    Id = option.Id,
                    DisplayValue = option.DisplayValue,
                    ReportingValue = option.ReportingValue
                });
            }

            return View(model);
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customfield/{customFieldId:guid}/edit")]
        public ActionResult Edit(Guid customFieldId, CustomFieldEditViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var getCustomFieldResponse = CustomFieldService.GetCustomField(new CustomFieldService.GetCustomFieldRequest { CustomFieldId = customFieldId });

            getCustomFieldResponse.CustomField.DisplayOrder = model.DisplayOrder;
            getCustomFieldResponse.CustomField.Label = model.Label;
            getCustomFieldResponse.CustomField.Required = model.Required;
            getCustomFieldResponse.CustomField.RequiredMessage = model.RequiredMessage;

            getCustomFieldResponse.CustomField.Options.Clear();

            foreach (var option in model.Options)
            {
                getCustomFieldResponse.CustomField.Options.Add( new CustomFieldModel.Option
                {
                    Id = option.Id,
                    DisplayValue = option.DisplayValue,
                    ReportingValue = option.ReportingValue
                });
            }

            try
            {
                CustomFieldService.SetCustomField(new CustomFieldService.SetCustomFieldRequest
                {
                    CustomFieldId = customFieldId,
                    CustomField = getCustomFieldResponse.CustomField
                });
                ViewBag.Success = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to save Custom Field. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }

            return View(model);
        }

        [HttpPost]
        [Route("~/controlpanel/customfield/{customFieldId:guid}/edit/option")]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        public ActionResult EditAddOption()
        {
            return PartialView("EditOption", new CustomFieldEditViewModel.Option { Id = Guid.NewGuid() });
        }

        #endregion

        #region EditTransaltion


        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customfield/{customFieldId:guid}/translate/{language}")]
        public ActionResult EditTransaltion(Guid customFieldId, string language)
        {
            var getCustomFieldResponse = CustomFieldService.GetCustomField(
               new CustomFieldService.GetCustomFieldRequest
               {
                   CustomFieldId = customFieldId
               }
           );
            
            var getCustomFieldTransaltionResponse = CustomFieldService.GetCustomFieldTranslation(
                new CustomFieldService.GetCustomFieldTransaltionRequest
                {
                    CustomFieldId = customFieldId,
                    Language = language,
                    
                }
            );

            var model = new CustomFieldEditTranslationViewModel
            {
                DisplayOrderOriginal = getCustomFieldResponse.CustomField.DisplayOrder,
                Label = getCustomFieldTransaltionResponse.CustomFieldTransaltion.Label,
                LabelOriginal = getCustomFieldResponse.CustomField.Label,
                RequiredMessage = getCustomFieldTransaltionResponse.CustomFieldTransaltion.RequiredMessage,
                RequiredMessageOriginal = getCustomFieldResponse.CustomField.RequiredMessage,
                RequiredOriginal = getCustomFieldResponse.CustomField.Required
            };

            foreach (var option in getCustomFieldResponse.CustomField.Options)
            {
                var optionTransalted = getCustomFieldTransaltionResponse.CustomFieldTransaltion.Options.FirstOrDefault(x => x.Id == option.Id);

                model.Options.Add(new CustomFieldEditTranslationViewModel.Option
                {
                    Id = option.Id,
                    DisplayValueOriginal = option.DisplayValue,
                    DisplayValue = optionTransalted == null ? "" : optionTransalted.DisplayValue,
                    ReportingValueOriginal = option.ReportingValue
                });
            }

            return View(model);
        }

        [HttpPost]
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/customfield/{customFieldId:guid}/translate/{language}")]
        public ActionResult EditTransaltion(Guid customFieldId, string language, CustomFieldEditTranslationViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var getCustomFieldTranslationResponse = CustomFieldService.GetCustomFieldTranslation(new CustomFieldService.GetCustomFieldTransaltionRequest { CustomFieldId = customFieldId, Language = language });

            getCustomFieldTranslationResponse.CustomFieldTransaltion.Label = model.Label;
            getCustomFieldTranslationResponse.CustomFieldTransaltion.RequiredMessage = model.RequiredMessage;

            getCustomFieldTranslationResponse.CustomFieldTransaltion.Options.Clear();

            foreach (var option in model.Options)
            {
                getCustomFieldTranslationResponse.CustomFieldTransaltion.Options.Add(new CustomFieldTransaltionModel.Option
                {
                    Id = option.Id,
                    DisplayValue = option.DisplayValue,                    
                });
            }
            
            try
            {
                CustomFieldService.SetCustomFieldTranslation(new CustomFieldService.SetCustomFieldTransaltionRequest
                {
                    CustomFieldId = customFieldId,
                    Language = language,
                    CustomFieldTransaltion = getCustomFieldTranslationResponse.CustomFieldTransaltion
                });
                ViewBag.Success = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to save Custom Field Transaltion. Exception: {0}", ex.Message);
                ViewBag.Success = false;
            }
            
            return View(model);
        }

        #endregion
    }
}