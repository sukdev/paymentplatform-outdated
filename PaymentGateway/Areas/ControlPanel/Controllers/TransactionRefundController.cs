﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure;
using StreamAMG.OTTPlatform.Core.Services;
using System;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Controllers
{
    [RouteArea("ControlPanel")]
    public class TransactionRefundController : Controller
    {
        [ControlPanelRequireHttps]
        [ControlPanelAuthrorize("administrator")]
        [Route("~/controlpanel/transactionrefund")]
        public ActionResult Index()
        {
            var exportToCsv = !string.IsNullOrWhiteSpace(Request.QueryString["export"]);
            var searchRequest = new CustomerTransactionRefundService.TransationRefundSearchRequest
            {
                PageIndex = 0,
                PageSize = exportToCsv ? int.MaxValue : 20
            };

            searchRequest.DateTo = DateTime.UtcNow;
            searchRequest.DateFrom = DateTime.UtcNow.AddMonths(-1);

            if (!string.IsNullOrWhiteSpace(Request["dateto"]))
                searchRequest.DateTo = DateTime.ParseExact(Request["dateto"], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);

            if (!string.IsNullOrWhiteSpace(Request["datefrom"]))
                searchRequest.DateFrom = DateTime.ParseExact(Request["datefrom"], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["subscriptionplanid"]) && Regex.IsMatch(Request.QueryString["subscriptionplanid"], @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"))
                searchRequest.SubscriptionPlanId = new Guid(Request.QueryString["subscriptionplanid"]);

            if (!string.IsNullOrWhiteSpace(Request["status"]))
                searchRequest.TransactionStatus = (CustomerTransactionRefundService.TransationRefundSearchRequest.TransactionStatuses)Enum.Parse(typeof(CustomerTransactionRefundService.TransationRefundSearchRequest.TransactionStatuses), Request["status"], true);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pageindex"]))
                searchRequest.PageIndex = Convert.ToInt32(Request.QueryString["pageindex"]);

            if (!string.IsNullOrWhiteSpace(Request.QueryString["pagesize"]))
                searchRequest.PageSize = Convert.ToInt32(Request.QueryString["pagesize"]);

            var searchResult = CustomerTransactionRefundService.TransactionRefundSearch(searchRequest);

            if (!exportToCsv)
                return View(searchResult);

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=refunds.csv");
            Response.ContentType = "text/csv";
            Response.Write(Utility.Helpers.CreateCSVAndAppendCustomerFieldsFromGenericList(searchResult.Items, "CustomerId"));
            Response.End();
            return null;


        }
    }
}