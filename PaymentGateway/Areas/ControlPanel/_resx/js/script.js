﻿function validateInteger(e) {
    // Accepts 0-9, backspace, delete, tab, left & right arrow
    if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39)
        return true;
    else
        return false;
};

function validateDecimal(e) {
    // Accepts 0-9, backspace, delete, left & right arrow, tab, decimal point & period
    if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 110 || e.keyCode == 190)
        return true;    
    else
        return false;
};
