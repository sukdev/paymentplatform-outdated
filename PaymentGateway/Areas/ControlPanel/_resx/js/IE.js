
$(document).ready(function () {
    isIE();
    function isIE() {
        var myNav = navigator.userAgent.toLowerCase();
        return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
    }

    isSafariNotMac();
    function isSafariNotMac() {
        var myNav = navigator.userAgent.toLowerCase();
        var is_chrome = myNav.indexOf('chrome') > -1;
        var is_safari = myNav.indexOf("safari") > -1;
        var is_mac = myNav.indexOf("macintosh") > -1;
        var is_windows = myNav.indexOf("windows") > -1;
        if ((is_chrome) && (is_safari)) { is_safari = false; }
        if ((is_mac) && (is_safari)) { is_safari = false; }
        if ((is_windows) && (is_safari)) { is_safari = false; }
        return is_safari;
    }


    if (isIE() == 8 || isIE() == 7) {

        $("body").addClass("old-IE");
        //Select right menu
        $("#responsive-menu").remove();
        $("#ie-menu").show();

        //$("head").append("<style>" +
        //".container { width: 1170px; max-width: 1170px; min-width: 1170px; color: white; } .clearfix { width: 100%; }" +
        //".col-xs-1,.col-xs-2,.col-xs-3,.col-xs-4,.col-xs-5,.col-xs-6,.col-xs-7,.col-xs-8,.col-xs-9,.col-xs-10,.col-xs-11,.col-xs-12,.col-sm-1,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-10,.col-sm-11,.col-sm-12,.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12,.col-lg-1,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-10,.col-lg-11,.col-lg-12{float:left; padding-left: 0 !important; padding-right: 0 !important; margin-left: 0 !important; margin-right: 0 !important; border: 0px;}" +
        //".col-xs-1, .col-sm-1, .col-md-1, .col-lg-1 { width: 8%; }.col-xs-2, .col-sm-2, .col-md-2, .col-lg-2 { width: 16%; }.col-xs-3, .col-sm-3, .col-md-3, .col-lg-3 { width: 25%; }.col-xs-4, .col-sm-4, .col-md-4, .col-lg-4 { width: 33%; }.col-xs-5, .col-sm-5, .col-md-5, .col-lg-5 { width: 41%; }.col-xs-6, .col-sm-6, .col-md-6, .col-lg-6 { width: 49%;}.col-xs-7, .col-sm-7, .col-md-7, .col-lg-7 { width: 58%; }.col-xs-8, .col-sm-8, .col-md-8, .col-lg-8 { width: 66%; }.col-xs-9, .col-sm-9, .col-md-9, .col-lg-9 { width: 75%; }.col-xs-10, .col-sm-10, .col-md-10, .col-lg-10 { width: 83%; }.col-xs-11, .col-sm-11, .col-md-11, .col-lg-11 { width: 91%; }.col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 { width: 100%; }" +
        //".col-sm-2 { width: 140px; margin:0; padding:0; float: left; }.col-sm-10 { width: 670px; margin:0; padding:0; float: left; }.col-md-12 { width: 100%; margin:0; padding:0; float: left; }" +
        //".hidden-xs, .hidden-sm { display: block; } .hidden-md, .hidden-lg { display: none; }" +
		//".visible-xs, .visible-sm { display: none !important;}" +
		//".visible-md, .visible-lg { display:block !important;}" +
		//"body aside .col-md-6, body aside .col-sm-6{ height: auto !important; display: block !important; width:100%; }" +
		//"aside { margin-top: 70px ;}" +
		//"body #home article { height: 117px !important; } body #home article .right .col-sm-6 { width:48% !important; } " +
		//"body #home .season-pass { background: #4c4c4c !important; } .article-even { background: #191919 !important; } .article-odd { background: #333333 !important; } " +
		//".ribbon-wrapper-green{ top: auto; left: auto; height: auto; overflow: visible; } .ribbon-green { top:0; left:-38px; } " +
		//"#home article > .col-md-6.text-center { width: 55%; } #home article > .col-md-6.text-center.right { width:40%; }" +
		//"footer .footer .notes{ left:0; background: url(\"" + __cdnurl + "_resx/images/transparent-bg.png?cdnversion=" + __cdnversion + "\") repeat; } footer .footer .notes P { float: left; width:40%;} .footer .navigation-bottom { width: 40%; float: right; } .footer UL LI { float: left; }" +
        //"#kickoff .vertMiddle > div {    clear:both;    display: block;}#kickoff #sub1, #kickoff #sub2 {    width: 475px;    text-align: right;}" +
		//"#team1 {    margin-right: 10px !important; width:200px;}#team2 {    margin-left: 10px !important; width:200px;}" +
		//".field.f_100 { margin-right: 20px; }" +
		//"body .page { margin: 0 auto;  position: relative; }" +
		//".logo {      position: absolute;      z-index: 999; }" +
		//"body header #navigation {position: relative; margin-top: -10px; }body header nav {     position: absolute;     top: 5px;      right: 0; }" +
        //"body header.cookie #cookieMsg .cookie-text { display: block !important; }" +
        //".footer .navigation-bottom { width: 50%; }" +
		//"</style>");
        
        if ($("#home").html() != undefined) {
            $("article:odd").addClass("article-odd");
            $("article:even").addClass("article-even");
        }

        if ($("#settings").html() != undefined) {
            $(".field.f_100").css("width", "auto !important");
        }


        $("aside").attr("style", "margin-left: 50%; margin-top: 70px !important;");

        $(".footer ul li").first().css("border", "0");
        $(".hidden-xs").remove(); $(".hidden-sm").remove();

        $("#team1 img").removeClass("img-responsive");
        $("#team2 img").removeClass("img-responsive");

    }

});
