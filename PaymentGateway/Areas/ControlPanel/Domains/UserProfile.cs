﻿using System;
using System.Collections.Generic;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Domains
{
    public class UserProfile
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool Deleted { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public List<string> Languages { get; set; }
    }
}