﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class CustomerSubscriptionEditViewModel
    {
        public class SubscriptionPlanOption
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public string CurrentSubscriptionPlanName { get; set; }
        public Guid CurrentSubscriptionPlanId { get; set; }
        public DateTime? CurrentSubscriptionRenewalDate { get; set; }        

        public List<SelectListItem> SubscriptionPlanIds { get; set; }

        [Required]
        [DisplayName("Subscription Plan")]
        public Guid SubscriptionPlanId { get; set; }

        public CustomerSubscriptionEditViewModel()
        {
            SubscriptionPlanIds = new List<SelectListItem>();
        }
    }
}