﻿using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "You must enter your username")]
        [StringLength(100, ErrorMessage = "The username cannot be longer than 100 characters")]
        public string Username { get; set; }

        [Required(ErrorMessage = "You must enter your password")]
        [StringLength(20, ErrorMessage = "The password cannot be longer than 20 characters")]
        public string Password { get; set; }
    }
}