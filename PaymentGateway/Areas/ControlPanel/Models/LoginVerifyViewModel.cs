﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class LoginVerifyViewModel
    {
        [DisplayName("Verification Code")]
        [Required(ErrorMessage = "You must enter your six digit verification code")]
        [RegularExpression(@"^([0-9]{6})$", ErrorMessage = "You must enter a six digit verification code.")]
        public string VerificationCode { get; set; }
    }
}
