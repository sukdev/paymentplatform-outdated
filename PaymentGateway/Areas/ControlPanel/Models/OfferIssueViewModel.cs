﻿using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class OfferIssueViewModel
    {
        [Required(ErrorMessage = "You must enter a campaign name")]
        [StringLength(100, ErrorMessage = "The campaign name must be less than 100 characters long")]
        [Display(Name = "Campaign Name")]
        public string Campaign { get; set; }

        [Required(ErrorMessage = "You must enter the number of vouchers you wish to issue")]
        [Range(1, 10000, ErrorMessage = "You can issue between one and ten thousand vouchers at a time")]
        [Display(Name = "Number of vouchers to issue")]
        public int Quantity { get; set; }
    }
}