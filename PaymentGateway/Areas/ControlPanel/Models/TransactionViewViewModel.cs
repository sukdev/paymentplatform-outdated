﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class TransactionViewViewModel
    {

        [Required]
        public string SubscriptionPlanName { get; set; }

        [Required]
        public Guid SubscriptionPlanId { get; set; }

        [DisplayName("Created At")]
        public DateTime CreatedAt { get; set; }

        [DisplayName("Updated At ")]
        public DateTime UpdatedAt { get; set; }


        [DisplayName("Status")]
        public int Status { get; set; }

        [Required]
        [DisplayName("Amount")]
        public decimal Amount { get; set; }

        [DisplayName("Currency")]
        public string Currency { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Provider")]
        public string Provider { get; set; }

        [DisplayName("Reference")]
        public string ProviderId { get; set; }

        [DisplayName("Error")]
        public string ProviderMessage { get; set; }

        [DisplayName("Country")]
        public string Country { get; set; }

        [DisplayName("Payment Reference")]
        public string Reference { get; set; }

        [DisplayName("Renewal From")]
        public DateTime RenewalFrom { get; set; }

        [DisplayName("Renewal To")]
        public DateTime RenewalTo { get; set; }

        [DisplayName("Batch")]
        public bool Batch { get; set; }

        [DisplayName("Statement Description")]
        public string StatementDescription { get; set; }
    }
}