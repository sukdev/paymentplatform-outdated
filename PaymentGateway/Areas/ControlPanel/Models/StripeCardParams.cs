﻿using Newtonsoft.Json;
using System;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class StripeCardParams
    {
        [JsonPropertyAttribute("token")]
        public string Token { get; set; }
        [JsonPropertyAttribute("id")]
        public string Id { get; set; }
        [JsonPropertyAttribute("customerId")]
        public string CustomerId { get; set; }
        [JsonPropertyAttribute("expiry")]
        public DateTime Expiry { get; set; }
    }
}