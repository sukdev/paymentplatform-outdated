﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class SubscriptionPlanViewModel
    {
        [Required(ErrorMessage = "You must enter a name")]
        [StringLength(500, ErrorMessage = "The name must be less than 500 characters long")]
        [Display(Name = "Name")]        
        public string Name { get; set; }

        [Display(Name = "Hidden On Site")]
        public bool Deleted { get; set; }

        [Required(ErrorMessage = "Display order is missing")]
        [Display(Name = "Display Order")]
        public int DisplayOrder { get; set; }

        [Required(ErrorMessage = "Entitlements cannot be empty")]
        [StringLength(100, ErrorMessage = "The entitlements be less than 500 characters long")]
        [Display(Name = "Entitlements")]
        public string Entitlements { get; set; }

        [Required(ErrorMessage = "You must enter a statement description")]
        [StringLength(20, ErrorMessage = "The statement title be less than 20 characters long")]
        [Display(Name = "Statement Description")]
        public string StatementDescription { get; set; }

        [Required(ErrorMessage = "You must enter a listing title")]
        [StringLength(100, ErrorMessage = "The listing title be less than 100 characters long")]
        [Display(Name = "Listing Title")]
        public string ListingTitle { get; set; }

        [Required(ErrorMessage = "You must enter a listing body")]
        [StringLength(2000, ErrorMessage = "The listing body be less than 2000 characters long")]
        [Display(Name = "Listing Body Text")]
        public string ListingBody { get; set; }

        [Display(Name = "Listing Css Class")]
        [StringLength(100, ErrorMessage = "The listing css class must be less than 100 characters long")]
        public string ListingStyle { get; set; }

        [Required(ErrorMessage = "You must enter a checkout title")]
        [StringLength(100, ErrorMessage = "The checkout title must be less than 100 characters long")]
        [Display(Name = "Checkout Title")]
        public string CheckoutTitle { get; set; }

        [Required(ErrorMessage = "You must enter a checkout body text")]
        [StringLength(2000, ErrorMessage = "The checkout body text must be less than 2000 characters long")]
        [Display(Name = "Checkout Body Text")]
        public string CheckoutBody { get; set; }

        [Display(Name = "Checkout Css Class")]
        [StringLength(100, ErrorMessage = "The checkout css class must be less than 100 characters long")]
        public string CheckoutStyle { get; set; }

        [Required(ErrorMessage = "You must enter a listing title for free trials")]
        [StringLength(100, ErrorMessage = "The listing title for free trials be less than 100 characters long")]
        [Display(Name = "Listing Title With Free Trials")]
        public string ListingTitleWithFreeTrial { get; set; }

        [Required(ErrorMessage = "You must enter a listing body for free trials")]
        [StringLength(2000, ErrorMessage = "The listing body for free trials be less than 2000 characters long")]
        [Display(Name = "Listing Body Text With Free Trials")]
        public string ListingBodyWithFreeTrial { get; set; }

        [Display(Name = "Listing Css Class With Free Trials")]
        [StringLength(100, ErrorMessage = "The listing css class for free trials must be less than 100 characters long")]
        public string ListingStyleWithFreeTrial { get; set; }

        [Required(ErrorMessage = "You must enter a checkout title for free trials")]
        [StringLength(100, ErrorMessage = "The checkout title for free trials must be less than 100 characters long")]
        [Display(Name = "Checkout Title With Free Trials")]
        public string CheckoutTitleWithFreeTrial { get; set; }

        [Required(ErrorMessage = "You must enter a checkout body text for free trials")]
        [StringLength(2000, ErrorMessage = "The checkout body text for free trials must be less than 2000 characters long")]
        [Display(Name = "Checkout Body Text With Free Trials")]
        public string CheckoutBodyWithFreeTrial { get; set; }

        [Display(Name = "Checkout Css Class With Free Trials")]
        [StringLength(100, ErrorMessage = "The checkout css class for free trials must be less than 100 characters long")]
        public string CheckoutStyleWithFreeTrial { get; set; }

    }
}