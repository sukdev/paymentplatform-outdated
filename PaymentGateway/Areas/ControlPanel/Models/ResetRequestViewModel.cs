﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class ResetRequestViewModel
    {
        [DisplayName("Username")]
        [Required(ErrorMessage = "You must enter your username")]
        [StringLength(100, ErrorMessage = "The username cannot be longer than 100 characters")]
        public string Username { get; set; }
    }
}