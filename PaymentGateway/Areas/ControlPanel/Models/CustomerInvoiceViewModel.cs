﻿using System;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class CustomerInvoiceViewModel
    {
        public int Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountRefunded { get; set; }
        public string Currency { get; set; }
        public string Reference { get; set; }
        
        public string Country { get; set; }
        public string Provider { get; set; }
        public string ProviderId { get; set; }
        public string ProviderMessage { get; set; }
    }
}