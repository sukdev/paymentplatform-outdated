﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class MailTemplateEditViewModel
    {
        [Required(ErrorMessage = "You must enter a subject")]
        [StringLength(100, ErrorMessage = "The subject must be less than 100 characters long")]
        [Display(Name = "Subject")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "You must enter email body text")]
        [Display(Name = "Body")]
        public string Body { get; set; }
    }
}