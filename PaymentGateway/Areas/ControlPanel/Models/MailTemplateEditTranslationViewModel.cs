﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class MailTemplateEditTranslationViewModel
    {
        [StringLength(100, ErrorMessage = "The subject must be less than 100 characters long")]
        [Display(Name = "Subject")]
        public string Subject { get; set; }
        public string SubjectOriginal { get; set; }


        [Display(Name = "Body")]
        public string Body { get; set; }
        public string BodyOriginal { get; set; }
    }
}