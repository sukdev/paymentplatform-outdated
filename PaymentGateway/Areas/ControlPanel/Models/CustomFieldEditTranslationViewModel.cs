﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class CustomFieldEditTranslationViewModel
    {

        public class Option
        {

            [Display(Name = "Id")]
            public Guid Id { get; set; }

            [Display(Name = "Value")]
            public string ReportingValueOriginal { get; set; }

            [Display(Name = "Text")]
            public string DisplayValue { get; set; }
            public string DisplayValueOriginal { get; set; }
        }

        [Required(ErrorMessage = "You must enter a label")]
        [StringLength(100, ErrorMessage = "The label must be less than 100 characters long")]
        [Display(Name = "Label")]
        public string Label { get; set; }
        public string LabelOriginal { get; set; }
        
        [Display(Name = "Required")]
        public bool RequiredOriginal { get; set; }
        
        [StringLength(100, ErrorMessage = "The required validation message must be less than 100 characters long")]
        [Display(Name = "Message")]
        public string RequiredMessage { get; set; }
        public string RequiredMessageOriginal { get; set; }

        [Required(ErrorMessage = "You must enter a display order")]        
        [Display(Name = "Display Order")]
        public int DisplayOrderOriginal { get; set; }


        public List<Option> Options { get; set; }

        public CustomFieldEditTranslationViewModel()
        {
            Options = new List<Option>();
        }

    }
}