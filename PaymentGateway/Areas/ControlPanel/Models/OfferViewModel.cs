﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class OfferViewModel
    {
        
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Discount")]
        public Decimal Discount { get; set; }

        [Display(Name = "Applies for")]        
        public string AppliesFor { get; set; }
        
        [Display(Name = "Applies to")]
        public List<Guid> AppliesToSubscriptionPlanId { get; set; }
    }
}