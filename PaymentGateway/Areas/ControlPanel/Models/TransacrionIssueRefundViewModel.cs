﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class TransacrionIssueRefundViewModel
    {
        [Required(ErrorMessage = "Transaction currency is missing")]
        public string TransactionCurrency { get; set; }
        [Required(ErrorMessage = "Transaction amount is missing")]
        public Decimal TransactionAmount { get; set; }
        [Required(ErrorMessage = "Transaction amount refunded is missing")]
        public Decimal TransactionAmountRefunded { get; set; }
        [Required(ErrorMessage = "Transaction amount to refund max is missing")]
        public Decimal RefundAmountMax { get; set; }
        [Required(ErrorMessage = "You must enter a total amount to refund")]
        [Display(Name = "Amount to refund")]
        public Decimal RefundAmount { get; set; }
        [Display(Name = "Reason for refund")]
        [Required(ErrorMessage = "You must enter a reason for the refund")]
        [StringLength(100, ErrorMessage = "The reason must be less than 100 characters long")]
        public string RefundReason { get; set; }

    }
}
