﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class UserEditViewModel
    {
        [Required(ErrorMessage = "You must enter a name")]
        [StringLength(100, ErrorMessage = "Name cannot be longer than 100 characters.")]
        [DisplayName("Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "You must enter a surname")]
        [StringLength(100, ErrorMessage = "Surname cannot be longer than 100 characters.")]
        [DisplayName("Surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "You must enter an email address")]
        [StringLength(100, ErrorMessage = "Email address cannot be longer than 150 characters.")]
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "You must enter a username")]
        [StringLength(100, ErrorMessage = "Username cannot be longer than 150 characters.")]
        [DisplayName("Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "You must enter a mobile phone number")]
        [StringLength(100, ErrorMessage = "Mobile phone number cannot be longer than 50 characters.")]
        [DisplayName("Mobile Phone")]
        public string MobilePhone { get; set; }

        [DisplayName("User Access Rights")]
        public List<Guid> SystemRoleIds { get; set; }


        [Required(ErrorMessage = "You must slect if teh user is blocked or not")]
        [DisplayName("Blocked")]
        public bool Deleted { get; set; }

        public UserEditViewModel()
        {
            SystemRoleIds = new List<Guid>();
        }
    }
}