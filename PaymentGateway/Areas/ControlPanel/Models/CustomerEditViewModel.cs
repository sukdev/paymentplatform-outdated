﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class CustomerEditViewModel
    {
        [Required(ErrorMessage = "You must enter a first name")]
        [StringLength(100, ErrorMessage = "The first name must be less than 100 characters long")]
        [Display(Name ="First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "You must enter a last name")]
        [StringLength(100, ErrorMessage = "The last name must be less than 100 characters long")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "You must enter an email address")]
        [StringLength(100, ErrorMessage = "The email address must be less than 100 characters long")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [StringLength(20, MinimumLength = 6, ErrorMessage ="The password must be between 6 and 20 characters long")]
        [Display(Name = "New Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "You must choose if the account should be blocked or not")]
        [Display(Name = "Block Account")]
        public bool Blocked { get; set; }

        [Display(Name = "Free Access Until")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        [DataType(DataType.DateTime)]
        public DateTime? FullAccessUntil { get; set; }
        
        [StringLength(5, ErrorMessage = "The language must not be longer than 5 characters")]
        [Display(Name = "Langauge")]
        public string Language { get; set; }
    }
}