﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class ResetViewModel
    {
        [DisplayName("Username")]
        [Required(ErrorMessage = "You must enter your username")]
        [StringLength(100, ErrorMessage = "The username cannot be longer than 100 characters")]
        public string Username { get; set; }

        [DisplayName("New Password")]
        [Required(ErrorMessage = "You must enter a new password")]
        [StringLength(20, ErrorMessage = "Your new password cannot exceed 20 characters")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [DisplayName("Confirm New Password")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "The new password confirmation does not match the new password")]
        public string NewPasswordConfirmation { get; set; }
    }
}