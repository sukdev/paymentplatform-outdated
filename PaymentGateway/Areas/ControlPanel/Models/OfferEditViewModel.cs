﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class OfferEditViewModel
    {
        [Required(ErrorMessage = "You must enter a name")]
        [StringLength(100, ErrorMessage = "The name must be less than 100 characters long")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "You must enter a discount")]
        [Display(Name = "Discount (%)")]
        [Range(1, 100, ErrorMessage = "You must enter a discount between 1 and 100 percent")]
        public Decimal Discount { get; set; }

        [Required(ErrorMessage = "You must select the period for which the discount applies")]
        [Display(Name = "Applies for")]        
        public string AppliesFor { get; set; }

        [Required(ErrorMessage = "You must select the subscription plan or plans the discount applies to")]
        [Display(Name = "Applies to")]
        public List<Guid> AppliesToSubscriptionPlanId { get; set; }
    }
}