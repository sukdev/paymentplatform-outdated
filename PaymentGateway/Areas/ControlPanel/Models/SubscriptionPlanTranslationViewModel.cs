﻿
using System.ComponentModel.DataAnnotations;


namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class SubscriptionPlanTranslationViewModel
    {
        [StringLength(500, ErrorMessage = "The name must be less than 500 characters long")]
        [Display(Name = "Name")]        
        public string Name { get; set; }
        public string NameOriginal { get; set; }


        [Display(Name = "Hidden On Site")]
        public bool DeletedOriginal { get; set; }

        [Display(Name = "Display Order")]
        public int DisplayOrderOriginal { get; set; }

        [StringLength(100, ErrorMessage = "The entitlements be less than 500 characters long")]
        [Display(Name = "Entitlements")]
        public string EntitlementsOriginal { get; set; }


        [StringLength(20, ErrorMessage = "The statement title be less than 20 characters long")]
        [Display(Name = "Statement Description")]
        public string StatementDescription { get; set; }
        public string StatementDescriptionOriginal { get; set; }

        [StringLength(100, ErrorMessage = "The listing title be less than 100 characters long")]
        [Display(Name = "Listing Title")]
        public string ListingTitle { get; set; }
        public string ListingTitleOriginal { get; set; }

        [StringLength(2000, ErrorMessage = "The listing body be less than 2000 characters long")]
        [Display(Name = "Listing Body Text")]
        public string ListingBody { get; set; }
        public string ListingBodyOriginal { get; set; }

        [Display(Name = "Listing Css Class")]
        [StringLength(100, ErrorMessage = "The listing css class must be less than 100 characters long")]
        public string ListingStyleOriginal { get; set; }

        [StringLength(100, ErrorMessage = "The checkout title must be less than 100 characters long")]
        [Display(Name = "Checkout Title")]
        public string CheckoutTitle { get; set; }
        public string CheckoutTitleOriginal { get; set; }

        [StringLength(2000, ErrorMessage = "The checkout body text must be less than 2000 characters long")]
        [Display(Name = "Checkout Body Text")]
        public string CheckoutBody { get; set; }
        public string CheckoutBodyOriginal { get; set; }

        [Display(Name = "Checkout Css Class")]
        [StringLength(100, ErrorMessage = "The checkout css class must be less than 100 characters long")]
        public string CheckoutStyleOriginal { get; set; }
        
        
        [StringLength(100, ErrorMessage = "The listing title for free trials be less than 100 characters long")]
        [Display(Name = "Listing Title With Free Trials")]
        public string ListingTitleWithFreeTrial { get; set; }
        public string ListingTitleWithFreeTrialOriginal { get; set; }

        [StringLength(2000, ErrorMessage = "The listing body for free trials be less than 2000 characters long")]
        [Display(Name = "Listing Body Text With Free Trials")]
        public string ListingBodyWithFreeTrial { get; set; }
        public string ListingBodyWithFreeTrialOriginal { get; set; }

        
        [StringLength(100, ErrorMessage = "The listing css class for free trials must be less than 100 characters long")]
        public string ListingStyleWithFreeTrialOriginal { get; set; }

        
        [StringLength(100, ErrorMessage = "The checkout title for free trials must be less than 100 characters long")]
        [Display(Name = "Checkout Title With Free Trials")]
        public string CheckoutTitleWithFreeTrial { get; set; }
        public string CheckoutTitleWithFreeTrialOriginal { get; set; }

        
        [StringLength(2000, ErrorMessage = "The checkout body text for free trials must be less than 2000 characters long")]
        [Display(Name = "Checkout Body Text With Free Trials")]
        public string CheckoutBodyWithFreeTrial { get; set; }
        public string CheckoutBodyWithFreeTrialOriginal { get; set; }

        [Display(Name = "Checkout Css Class With Free Trials")]
        [StringLength(100, ErrorMessage = "The checkout css class for free trials must be less than 100 characters long")]        
        public string CheckoutStyleWithFreeTrialOriginal { get; set; }

    }
}