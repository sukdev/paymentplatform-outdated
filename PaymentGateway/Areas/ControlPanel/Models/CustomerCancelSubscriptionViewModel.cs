﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class CustomerCancelSubscriptionViewModel
    {
        public string SubscriptionPlanName { get; set; }
        public DateTime SubscriptionRenewalDate { get; set; }

        [Required(ErrorMessage = "You must enter a cancellation reason name")]
        [StringLength(100, ErrorMessage = "The cancellation reason be less than 100 characters long")]
        [Display(Name = "Reason For Cancellation")]
        public string CancellationReason { get; set; }
        
        [Display(Name = "Cancel immediately")]
        public bool CancellationImmediate { get; set; }
    }
}