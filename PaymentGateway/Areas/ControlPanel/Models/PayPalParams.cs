﻿using Newtonsoft.Json;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class PayPalParams
    {
        [JsonPropertyAttribute("expressCheckoutToken")]
        public string ExpressCheckoutToken { get; set; }
        [JsonPropertyAttribute("billingAgreementId")]
        public string BillingAgreementId { get; set; }
        [JsonPropertyAttribute("payerId")]
        public string PayerId { get; set; }
        [JsonPropertyAttribute("payerName")]
        public string PayerName { get; set; }
        [JsonPropertyAttribute("payer")]
        public string Payer { get; set; }
        [JsonPropertyAttribute("payerCountry")]
        public string PayerCountry { get; set; }
    }
}