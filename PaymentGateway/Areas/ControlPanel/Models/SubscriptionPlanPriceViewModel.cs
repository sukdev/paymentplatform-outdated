﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class SubscriptionPlanPriceViewModel
    {
        public Guid SubscriptionPlanId { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        [DataType(DataType.DateTime)]
        [DisplayName("Effective From")]
        public DateTime EffectiveFrom { get; set; }

        [DisplayName("Currency")]
        public string Currency { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [DisplayName("Amount")]
        public decimal Amount { get; set; }

        [Required]
        [DisplayName("Contract Length")]
        public string Interval { get; set; }        

        [DisplayName("Auto Renew")]
        public string Duration { get; set; }        

        [DisplayName("Trial Period")]
        public string TrialDuration { get; set; }        

       
    }
}