﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class CustomerReactivateSubscriptionViewModel
    {
        public string SubscriptionPlanName { get; set; }
        public DateTime SubscriptionRenewalDate { get; set; }       
    }
}