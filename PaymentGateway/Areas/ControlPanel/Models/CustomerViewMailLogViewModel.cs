﻿using System;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Models
{
    public class CustomerViewMailLogViewModel
    {
        public DateTime CreatedAt { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string MailTo { get; set; }
        public int Status { get; set; }
    }
}