﻿using StreamAMG.OTTPlatform.Areas.ControlPanel.Security;
using System.Web;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure
{
    public class ControlPanelAuthrorizeAttribute : AuthorizeAttribute
    {
        private readonly string[] allowedroles;

        public ControlPanelAuthrorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return ControlPanelAuthentication.IsAuthenticated && ControlPanelAuthentication.IsVerified && ControlPanelAuthentication.CurrentUserCanAccess(allowedroles);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var returnUrl = filterContext.HttpContext.Request.Url.PathAndQuery;

            filterContext.Result = new RedirectResult("/controlpanel/denied/?returnUrl=" + returnUrl);

            if (!ControlPanelAuthentication.IsVerified)
                filterContext.Result =
                    new RedirectResult("/controlpanel/verify/?returnUrl=" + returnUrl);

            if (!ControlPanelAuthentication.IsAuthenticated)
                filterContext.Result =
                    new RedirectResult("/controlpanel/login/?returnUrl=" + returnUrl);
        }
    }
}