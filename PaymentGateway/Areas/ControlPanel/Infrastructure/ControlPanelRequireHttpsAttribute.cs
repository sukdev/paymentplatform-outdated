﻿using System;
using System.Web;
using System.Web.Mvc;

namespace StreamAMG.OTTPlatform.Areas.ControlPanel.Infrastructure
{
    public class ControlPanelRequireHttpsAttribute : RequireHttpsAttribute
    {

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            // If we should be under https but arent then return false
            //if (filterContext != null && !string.Equals(HttpContext.Current.Request.Url.Host, "localhost", StringComparison.InvariantCultureIgnoreCase) && !string.Equals(HttpContext.Current.Request.Headers["X-Forwarded-Proto"], "https", StringComparison.InvariantCultureIgnoreCase))
                //HandleNonHttpsRequest(filterContext);

        }
    }
}