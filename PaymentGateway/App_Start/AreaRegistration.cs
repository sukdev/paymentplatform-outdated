﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace StreamAMG.OTTPlatform
{
    public class CMSAreaRegistration
    {
        public static void RegisterArea<T>(RouteCollection routes, object state) where T : AreaRegistration
        {
            var registration = (AreaRegistration)Activator.CreateInstance(typeof(T));
            var context = new AreaRegistrationContext(registration.AreaName, routes, state);
            var tNamespace = registration.GetType().Namespace;
            if (tNamespace != null)
            {
                context.Namespaces.Add(tNamespace + ".*");
            }
            registration.RegisterArea(context);
        }
    }
}