﻿using StreamAMG.OTTPlatform.Areas.ControlPanel;
using System.Web.Mvc;
using System.Web.Routing;

namespace StreamAMG.OTTPlatform
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapMvcAttributeRoutes();

            CMSAreaRegistration.RegisterArea<ControlPanelAreaRegistration>(routes, null);          
        }
    }
}
